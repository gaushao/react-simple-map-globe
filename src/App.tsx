/**
 * application root used to develop globe module
 * @category Dev
 * @module Dev App
 */

import styled from 'styled-components'
import Globe from './components/globe'
import { Menu } from './components/menu'
import { PROPS } from './constants'
import { useMarkers, useSettings } from './hooks'

window.document.body.style.margin = '0'
window.document.body.style.height = '100vh'
window.document.body.style.overflow = 'hidden'
window.document.body.style.fontFamily = 'system-ui'

const Page = styled.div`
  display: flex;
`

/**
 * setup development props into [[Globe]] and [[Menu]]
 * @category Component
 * @hooks [[useSettings]] [[useMarkers]]
 * @returns
 * >`div`
 * >>[[Menu]]\
 * >>[[Globe]]
 */
function App() {
  const settings = useSettings()
  const markers = useMarkers()
  return (
    <Page>
      <Menu settings={settings} markers={markers} />
      <Globe markers={markers[0]} settings={settings} {...PROPS} />
    </Page>
  )
}

export default App

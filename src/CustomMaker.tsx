/**
 * testing custom svg marker
 * @category Dev
 * @module Dev CustomMaker
 */
import { SVG } from './assets'

/**
 * @category Component
 * @returns [[SVG]]
 */
function CustomMaker(props: any) {
  return <SVG.Location />
}

export default CustomMaker

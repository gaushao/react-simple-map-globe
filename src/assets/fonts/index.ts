/**
 * @category Core
 * @module  Assets Fonts
 */
import CQ_MONO from './cq-mono.ttf'

export const FONTS = { CQ_MONO }

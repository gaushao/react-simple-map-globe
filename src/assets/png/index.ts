/**
 * @category Core
 * @module  Assets Images
 */

import CAT from './cat.png'

export const IMAGES = { CAT }

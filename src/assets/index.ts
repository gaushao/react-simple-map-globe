/**
 * @category Core
 * @module  Assets
 */

import { FONTS } from './fonts'
import { IMAGES } from './png'
import { SVG } from './svg'

export { FONTS, IMAGES, SVG }

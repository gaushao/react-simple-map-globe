/**
 * @category Core
 * @module  Assets SVGs
 */

import { ReactComponent as Globe } from './globe.svg'
import { ReactComponent as Settings } from './settings.svg'
import { ReactComponent as Gitlab } from './gitlab.svg'
import { ReactComponent as Book } from './book.svg'
import { ReactComponent as Location } from './location.svg'

/**
 * @category Assets
 */
export const SVG = { Globe, Settings, Gitlab, Book, Location }

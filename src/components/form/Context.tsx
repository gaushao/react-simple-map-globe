/**
 * provides state and dispatch features to form handling
 * @category Form
 * @module Dev Form Context
 */

import { get, noop } from 'lodash'
import {
  createContext,
  PropsWithChildren,
  useCallback,
  useMemo,
  useState,
} from 'react'
import { FormContextValue, FormDispatch, InitialProps } from './types'

const EMPTY_CONTEXT_VALUE: FormContextValue<any> = {
  data: {},
  dispatch: noop,
}

/**
 * @category Context
 *
 * @returns
 * [[FormContextValue]]
 */
export const FormContext =
  createContext<FormContextValue<any>>(EMPTY_CONTEXT_VALUE)

/**
 * @category Provider
 *
 * @typeParam T type of `initial`
 * @prop {InitialProps} [initial] form [[InitialProps.initial]] data
 * @param props
 *
 * @returns `wrap`\
 * [[FormContext]]
 */
export function FormProvider<T>({
  children,
  initial,
}: PropsWithChildren<InitialProps<T>>) {
  const [data, update] = useState<T>(initial)
  const dispatch: FormDispatch<T> = useCallback((path, value) => {
    const keys = path.split('.').reverse()
    update((curr) => {
      const changes = keys.reduce((acc, k, i, arr) => {
        const last = arr[i - 1]
        const rest = i === 0 ? {} : get(curr, [...arr].slice(i).reverse())
        return last ? { [k]: { ...rest, ...acc } } : { [k]: value }
      }, {})
      return { ...curr, ...changes }
    })
  }, [])
  const value = useMemo(() => ({ data, dispatch }), [data, dispatch])
  return <FormContext.Provider value={value}>{children}</FormContext.Provider>
}

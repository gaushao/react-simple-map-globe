/**
 * [[FormProvider]] wrapper
 * @category Form
 * @module Dev Form
 */

import { PropsWithChildren } from 'react'
import styled from 'styled-components'
import { FormProvider } from './Context'
import { InitialProps } from './types'

const StyledForm = styled.form``

/**
 * wrap form element into [[FormProvider]]
 * @category Component
 * @typeParam T type of [[InitialProps]]
 * @prop {InitialProps} [initial] form [[InitialProps.initial]] data
 * @param props
 * @returns `wrap`\
 * [[FormProvider]]
 */
export function Form<T>({
  initial,
  children,
  ...props
}: PropsWithChildren<InitialProps<T>>) {
  return (
    <FormProvider<T> initial={initial}>
      <StyledForm {...props}>{children}</StyledForm>
    </FormProvider>
  )
}

export default Form

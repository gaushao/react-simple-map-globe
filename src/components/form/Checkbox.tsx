/**
 * type Checkbox input element wrapper
 * @category Form
 * @module Dev Form Checkbox
 */

import { ChangeEventHandler, PropsWithChildren, useCallback } from 'react'

export interface CheckboxProps {
  value: boolean
  label?: string
  onChange: (v: boolean) => void
}

/**
 * Checkbox field with label
 * @category Component
 * @param value Checkbox input and label children
 * @param onChange value callback
 * @param checked empty shape for initial form value
 */
export function Checkbox({
  value,
  label,
  onChange,
  children,
}: PropsWithChildren<CheckboxProps>) {
  const onChangeCheck = useCallback<ChangeEventHandler<HTMLInputElement>>(
    ({ target: { checked } }) => {
      onChange(checked)
    },
    [onChange]
  )
  return (
    <div>
      <input type='checkbox' checked={value} onChange={onChangeCheck} />
      {label && <label>{label}</label>}
      {children}
    </div>
  )
}

export default Checkbox

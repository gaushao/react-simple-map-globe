/**
 * @category Form
 * @module Dev Form Hooks
 */

import { get } from 'lodash'
import { useCallback, useContext, useEffect, useState } from 'react'
import { DispatchState } from '../globe/types'
import { FormContext } from './Context'
import { FormContextValue } from './types'

/**
 * @category Context
 */
export function useFormContext<T>() {
  return useContext<FormContextValue<T>>(FormContext)
}
/**
 * access form value by path
 * use dots to select inner properties
 * ```typescript
 * const value = useFormValue('path.to.value')
 * ```
 * @category Selector
 */
export function useFormValue<T>(path: string): T {
  return get(useFormContext<T>().data, path)
}
/**
 * call form dispatch on path with value
 * ```typescript
 * const dispatch = useFormDispatch('path.to.value')
 * dispatch(value)
 * ```
 * @category Dispatch
 */
export function useFormDispatch<T>(path: string): DispatchState<T> {
  const { dispatch } = useFormContext()
  return useCallback((value) => dispatch(path, value), [dispatch, path])
}
/**
 * `listen` to value by returned `listen callback`
 * and dispatches it into the form path on change\
 * `listen callback` will only fire on value change effect
 *
 * @returns `listen callback`
 *
 * @category Listener
 */
export function useFormListener<T>(path: string): DispatchState<T> {
  const [heard, listen] = useState<React.SetStateAction<T>>()
  const { dispatch } = useFormContext()
  const current = useFormValue(path)
  useEffect(() => {
    if (heard !== current) dispatch(path, heard)
  }, [current, dispatch, heard, path])
  return useCallback((value) => heard !== value && listen(value), [heard])
}

/**
 * form input element wrapper
 * @category Form
 * @module Dev Form Input
 */

import { InputHTMLAttributes, useCallback } from 'react'
import styled from 'styled-components'

const StyledInput = styled.input``

/**
 * value proxy
 * @category Callback
 */
export interface InputOnChangeValue<T> {
  (v: T): void
}

/**
 * callsback value of `React.ChangeEventHandler`
 * [Crash onDrag](https://gitlab.com/gaushao/react-simple-map-globe/-/issues/3)
 * @category Change Event
 *
 * @param callback - [[InputOnChangeValue<T>]]
 * @returns [[InputOnChangeValue<T> | undefined]]
 */
export function useOnInputChange<T>(callback?: InputOnChangeValue<T>) {
  const onChange = useCallback(
    ({ target: { value } }) => (!!callback ? callback(value) : null),
    [callback]
  )
  return !!callback ? onChange : undefined
}
/**
 * ensure [[useOnInputChange]] to callback `number` value
 * {@link Crash onDrag | https://gitlab.com/gaushao/react-simple-map-globe/-/issues/3}
 * @category Change Event
 *
 * @param callback - [[InputOnChangeValue<number>]]
 * @returns [[InputOnChangeValue<number> | undefined]]
 */
export function useOnNumberChange(callback?: InputOnChangeValue<number>) {
  const onNumberChange = useCallback(
    (value) => {
      if (!(!!callback && !!value)) return
      if (typeof value === 'number') return callback(value)
      try {
        const num = parseFloat(value)
        if (typeof num === 'number') return callback(num)
      } catch (e) {}
    },
    [callback]
  )
  const onChange = useOnInputChange(onNumberChange)
  return !!callback ? onChange : undefined
}
/**
 * form input component defines onChangeCallback
 *
 * ```typescript
 * onInputChange = useOnInputChange<T>(onChangeValue)
 * onNumberChange = useOnNumberChange(onChangeNumber)
 * onChangeCallback = onInputChange || onNumberChange || onChange
 * ```
 *
 * @param onChangeValue passed into [[useOnInputChange]]
 * @param onChangeNumber passed into [[useOnNumberChange]]
 * @param onChange passed into `input`
 *
 * @category Component
 */
function Input<T = any>({
  onChangeValue,
  onChangeNumber,
  onChange,
  ...props
}: InputHTMLAttributes<Element> & {
  onChangeValue?: InputOnChangeValue<T>
  onChangeNumber?: InputOnChangeValue<number>
}) {
  const onInputChange = useOnInputChange<T>(onChangeValue)
  const onNumberChange = useOnNumberChange(onChangeNumber)
  const change = onInputChange || onNumberChange || onChange
  return <StyledInput {...props} onChange={change} />
}

export default Input

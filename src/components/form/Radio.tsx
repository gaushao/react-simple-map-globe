/**
 * type radio input element wrapper
 * @category Form
 * @module Dev Form Radio
 */

import { PropsWithChildren, useCallback } from "react";
import Input from "./Input";

export interface RadioOption {
  name: string | null;
  value: string | null;
}

export interface RadioProps {
  option: RadioOption;
  checked: boolean;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
}

/**
 * radio field with label
 * @category Component
 * @param value radio input and label children
 * @param onChange value callback
 * @param checked empty shape for initial form value
 */
export function Radio({ option, onChange, checked }: RadioProps) {
  const { name, value } = option;
  return value ? (
    <div>
      <input type="radio" value={value} onChange={onChange} checked={checked} />
      <label>{name}</label>
    </div>
  ) : null;
}
/**
 * radio field with input as label
 * [[RadioGroup]] render it if [[RadioOption.name]] `null`
 * allows user to customize radio input value
 * changes `value` only if radio `checked`
 * @category Component
 * @param value radio input and label children
 * @param onChange value callback
 * @param checked empty shape for initial form value
 */
export function RadioInput({ option, onChange, checked }: RadioProps) {
  const { value } = option;
  const current = (checked && value) || "";
  return (
    <div>
      <input
        type="radio"
        value={current}
        onChange={onChange}
        checked={checked}
      />
      <label>
        <Input value={current} onChange={onChange} />
      </label>
    </div>
  );
}

export interface RadioGroupProps {
  selected: RadioOption["value"];
  options: RadioOption[];
  onSelect: (value: RadioOption["value"]) => void;
}

/**
 * radio fields handler: will render one [[Radio]]
 * to each one of the `options`. allows user to
 * customize radio `value` with [[RadioInput]]
 * @category Component
 *
 * @param selected selected [[RadioOption.value]]
 * @param options list of [[RadioOption]]
 * @param onSelect callsback [[RadioOption.value]]
 */
export function RadioGroup({
  selected,
  options,
  onSelect,
  children = null,
}: PropsWithChildren<RadioGroupProps>) {
  const onChange = useCallback(
    (o: RadioOption): React.ChangeEventHandler<HTMLInputElement> =>
      (e) => {
        const { value } = e.target;
        switch (true) {
          case !(selected === value):
            onSelect(value);
            break;
          case o === null:
            onSelect("");
            break;
        }
      },
    [onSelect, selected]
  );
  return (
    <div>
      {options.map((o: RadioOption) =>
        o.name ? (
          <Radio
            key={o.name}
            option={o}
            checked={o.value === selected}
            onChange={onChange(o)}
          />
        ) : (
          <RadioInput
            key="RadioInput"
            option={{ name: null, value: selected }}
            onChange={onChange(o)}
            checked={!options.map((p) => p.value).includes(selected)}
          />
        )
      )}
      {children}
    </div>
  );
}

export default Radio;

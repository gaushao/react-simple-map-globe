/**
 * @category Form
 * @module Dev Form Types
 */

/**
 * dispatches `value` into [[FormContextValue.data]] `path`
 */
export interface FormDispatch<T = any> {
  (path: string, value: T): void
}
/**
 * props forwarded into form provider as [[FormContextValue.data]]
 * @typeParam Initial type of `initial`
 * @property {Initial} [initial] form data initial state
 */
export interface InitialProps<Initial = any> {
  initial: Initial
}
/**
 * @property {T} [data] form data
 * @property {FormDispatch} [dispatch] dispatch callback
 */
export interface FormContextValue<T> {
  data: T
  dispatch: FormDispatch<T>
}

/**
 * @category Globe
 * @module Globe Cartography Classes
 */

/**
 * @category Data
 */
export class CartographyData {
  constructor(public urls?: string[], public jsons?: JSON[]) {}
}

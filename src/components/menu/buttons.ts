/**
 * @category Menu
 * @module Menu Buttons
 */
import styled from 'styled-components'
import { SVG } from '../../assets'

const DOCS_URL = 'https://gaushao.gitlab.io/react-simple-map-globe/docs'
const GITLAB_URL = 'https://gitlab.com/gaushao/react-simple-map-globe'
/**
 * move elsewhere
 * @category Callback
 */
export function onClickWindowOpen(url: string) {
  return () => {
    window.open(url, '_blank')
  }
}
/**
 * pass `onClick` into `defaultProps`
 * @category Constructor
 * @returns
 * `Button.defaultProps`
 */
export function onClickWindowOpenProp(url: string) {
  return {
    onClick: onClickWindowOpen(url),
  }
}

const style = () => `
  height: 24px;
`

export const DocsButton = styled(SVG.Book)(style)

DocsButton.defaultProps = onClickWindowOpenProp(DOCS_URL)

export const GitlabButton = styled(SVG.Gitlab)(style)

GitlabButton.defaultProps = onClickWindowOpenProp(GITLAB_URL)

export const SettingsButton = styled(SVG.Settings)(style)

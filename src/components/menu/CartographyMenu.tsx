/**
 * [[CartographySettings]] UI configuration
 * @category Menu
 * @module Menu Cartography
 */
import { keys, remove, uniqueId } from 'lodash'
import { memo, useCallback, useState } from 'react'
import Checkbox from '../form/Checkbox'
import Input from '../form/Input'
import { CartographyUrl } from '../globe/cartography/constants'
import Section from './Section'
import { MenuProps } from './types'

/**
 * @category Component
 */
export function CartographyMenu({ settings }: MenuProps) {
  const controller = settings?.cartography?.[1]
  const urls = settings?.cartography?.[0]?.urls
  const [options, setOptions] = useState<Record<string, string>>(CartographyUrl)
  const addOption = useCallback(() => {
    setOptions((curr) => ({ ...curr, [`${uniqueId()}-${uniqueId()}`]: '' }))
  }, [])
  const setOption = useCallback((value: string, i: string) => {
    setOptions((curr) => {
      return { ...curr, [i]: value }
    })
  }, [])
  const addUrl = useCallback(
    (url?: string) => {
      if (!controller || !url) return
      controller((curr) =>
        curr.urls && !curr.urls.includes(url)
          ? { ...curr, urls: [...curr.urls, url] }
          : curr
      )
    },
    [controller]
  )
  const delUrl = useCallback(
    (url?: string) => {
      if (!controller || !url) return
      controller((curr) =>
        curr.urls
          ? {
              ...curr,
              urls: remove(curr.urls, (v) => v !== url),
            }
          : curr
      )
    },
    [controller]
  )
  const includes = useCallback((url: string) => !!urls?.includes(url), [urls])
  return (
    <Section title='CARTOGRAPHY'>
      {keys(options).map((key, i) =>
        keys(CartographyUrl).includes(key) ? (
          <Checkbox
            key={key}
            label={key}
            value={includes(CartographyUrl[key as keyof typeof CartographyUrl])}
            onChange={(checked) =>
              checked ? addUrl(options[key]) : delUrl(options[key])
            }
          />
        ) : (
          <Checkbox
            key={`${key}-checkbox`}
            value={includes(options[key])}
            onChange={(checked) =>
              checked ? addUrl(options[key]) : delUrl(options[key])
            }
          >
            <Input
              key={`${key}-input`}
              value={options[key]}
              onChangeValue={(v) => setOption(v, key)}
            />
          </Checkbox>
        )
      )}
      <p
        onClick={addOption}
        style={{
          textAlign: 'center',
          textDecoration: 'underline',
          cursor: 'pointer',
        }}
      >
        add custom url
      </p>
    </Section>
  )
}

export default memo(CartographyMenu)

/**
 * [[ViewSettings]] UI configuration
 * @category Menu
 * @module Menu View
 */
import { useCallback } from 'react'
import Checkbox from '../form/Checkbox'
import Input from '../form/Input'
import { Point } from '../globe/classes'
import { EMPTY_COORD } from '../globe/constants'
import { INITIAL_DIMENSIONS } from '../globe/view/constants'
import Section from './Section'
import { MenuProps } from './types'

const [INITIAL_WIDTH, INITIAL_HEIGHT] = INITIAL_DIMENSIONS

/**
 * @category Component
 */
export function ViewMenu({ settings }: MenuProps) {
  const controller = settings?.view?.[1]
  const data = settings?.view?.[0]
  const [w, h] = data?.dimensions || EMPTY_COORD
  const setW = useCallback(
    (v) => {
      if (!controller) return
      controller((current) => ({
        ...current,
        fetchToTarget: false,
        dimensions: new Point(v, current?.dimensions?.[1] || INITIAL_HEIGHT),
      }))
    },
    [controller]
  )
  const setH = useCallback(
    (v) => {
      if (!controller) return
      controller((current) => ({
        ...current,
        fetchToTarget: false,
        dimensions: new Point(current?.dimensions?.[0] || INITIAL_WIDTH, v),
      }))
    },
    [controller]
  )
  const setFetchToBody = useCallback(
    (v) => {
      if (!controller) return
      controller((current) => {
        return {
          ...current,
          fetchToTarget: v,
        }
      })
    },
    [controller]
  )
  return (
    <Section title='VIEW'>
      <span>width: </span>
      <Input type='number' value={w} onChangeNumber={setW} />
      <br />
      <span>height: </span>
      <Input type='number' value={h} onChangeNumber={setH} />
      <br />
      <br />
      {!!data?.target && (
        <Checkbox
          value={!!data?.fetchToTarget}
          label='fetch dimensions to body'
          onChange={setFetchToBody}
        />
      )}
    </Section>
  )
}

export default ViewMenu

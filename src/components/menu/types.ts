/**
 * @category Globe
 * @module Globe Menu Types
 */
import { MarkerData } from '../globe/markers/types'
import { GlobeSettings, MutableState } from '../globe/types'

export interface MenuProps {
  settings?: GlobeSettings
  markers?: MutableState<MarkerData[]>
}

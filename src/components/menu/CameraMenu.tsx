/**
 * [[CameraSettings]] UI configuration
 * @category Menu
 * @module Menu Camera
 */
import { useCallback } from 'react'
import Input from '../form/Input'
import { Coord } from '../globe/classes'
import { EMPTY_COORD } from '../globe/constants'
import Section from './Section'
import { MenuProps } from './types'

/**
 * @category Component
 */
export function CameraMenu({ settings }: MenuProps) {
  const controller = settings?.camera?.[1]
  const [x, y] = settings?.camera?.[0]?.rotation || EMPTY_COORD
  const setX = useCallback(
    (v: number) => {
      if (!controller) return
      controller((current) => ({
        ...current,
        rotation: new Coord(v, current.rotation.y),
      }))
    },
    [controller]
  )
  const setY = useCallback(
    (v: number) => {
      if (!controller) return
      controller((current) => ({
        ...current,
        rotation: new Coord(current.rotation.x, v),
      }))
    },
    [controller]
  )
  return (
    <Section title='CAMERA'>
      <h6>ROTATION</h6>
      <span>x: </span>
      <Input type='number' value={x} onChangeNumber={setX} />
      <div />
      <span>y: </span>
      <Input type='number' value={y} onChangeNumber={setY} />
    </Section>
  )
}

export default CameraMenu

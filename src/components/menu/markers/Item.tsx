/**
 * @category Globe
 * @module Globe Menu Markers Item
 */
import { useCallback } from 'react'
import styled from 'styled-components'
import Input from '../../form/Input'
import { Coord } from '../../globe/classes'
import { MarkerData } from '../../globe/markers/types'
import { DispatchState } from '../../globe/types'

const Container = styled.div`
  border: 1px solid white;
  padding: 8px 8px;
`
/**
 * @category Props
 */
interface ItemProps {
  marker: MarkerData
  controller: DispatchState<MarkerData[]>
}

/**
 * each marker renders an item
 * @category Component
 */
function Item({ marker, controller }: ItemProps) {
  const setX = useCallback(
    (x: number) => {
      controller((markers) => {
        const result = [...markers]
        const i = markers.findIndex((f) => f.id === marker.id)
        result[i] = {
          ...marker,
          coordinates: new Coord(x, marker.coordinates.longitude),
        }
        return result
      })
    },
    [marker, controller]
  )
  const setY = useCallback(
    (y: number) => {
      controller((markers) => {
        const result = [...markers]
        const i = markers.findIndex((f) => f.id === marker.id)
        result[i] = {
          ...marker,
          coordinates: new Coord(marker.coordinates.latitude, y),
        }
        return result
      })
    },
    [marker, controller]
  )
  return (
    <Container>
      <span>id: {marker.id}</span>
      <br />
      <span>x: </span>
      <Input
        type='number'
        value={marker.coordinates[0]}
        onChangeNumber={setX}
      />
      <div />
      <span>y: </span>
      <Input
        type='number'
        value={marker.coordinates[1]}
        onChangeNumber={setY}
      />
    </Container>
  )
}

export default Item

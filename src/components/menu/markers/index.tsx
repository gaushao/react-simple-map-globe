/**
 * [[Markers]] UI configuration
 * @category Menu
 * @module Menu Camera
 */
import Section from '../Section'
import { MenuProps } from './../types'
import Item from './Item'

/**
 * @category Component
 */
export function MarkersMenu({ markers }: MenuProps) {
  if (!markers) return null
  const [m, sM] = markers
  return (
    <Section title='MARKERS'>
      {m.map((marker) => (
        <Item key={marker.id} marker={marker} controller={sM} />
      ))}
    </Section>
  )
}

export default MarkersMenu

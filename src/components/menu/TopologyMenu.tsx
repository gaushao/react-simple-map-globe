/**
 * [[TopologySettings]] UI configuration
 * @category Menu
 * @module Menu Topology
 */
// import styled from 'styled-components'
import { MenuProps } from './types'

// const Container = styled.div``

/**
 * WIP handle Topology settings
 * @category Component
 * @returns `wrap`\
 * [[RadioGroup]]
 * - [[RadioGroupProps.options]]: [[TopologyData.keys]]
 * - [[RadioGroupProps.onSelect]]: [[TopologyController.setSelected]]>
 */
export function TopologyMenu({ settings }: MenuProps) {
  // const controller = settings?.cartography?.[1]
  // const keys = []
  // const options = useMemo(
  //   () => (keys || []).map((k) => ({ name: k, value: k })),
  //   [keys]
  // )
  // return (
  //   <Container>
  //     <h4>Topology</h4>
  //     <RadioGroup
  //       selected={selected}
  //       options={options}
  //       onSelect={setSelected as () => void}
  //     />
  //   </Container>
  // )
  return null
}
export default TopologyMenu

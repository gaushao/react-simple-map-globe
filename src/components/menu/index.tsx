/**
 * module debugger and controller
 * @category Menu
 * @module Menu
 */

import { PropsWithChildren } from 'react'
import styled from 'styled-components'
import { useBooleanState } from '../globe/hooks'
import { DocsButton, GitlabButton, SettingsButton } from './buttons'
import { MenuProps } from './types'
import CameraMenu from './CameraMenu'
import CartographyMenu from './CartographyMenu'
import MarkersMenu from './markers'
import TopologyMenu from './TopologyMenu'
import ViewMenu from './ViewMenu'

const Container = styled.div`
  position: fixed;
  z-index: 1;
  margin: 16px 32px;
  padding: 16px 32px;
  background-color: lightgray;
  overflow: scroll;
  max-height: 85vh;
  -ms-overflow-style: none; /* Internet Explorer 10+ */
  scrollbar-width: none; /* Firefox */
  ::-webkit-scrollbar {
    display: none; /* Safari and Chrome */
  }
`

const Buttons = styled.div`
  display: flex;
  gap: 0 16px;
`
/**
 * Links to:\
 * [Gitlab](https://gitlab.com/gaushao/react-simple-map-globe)\
 * [Docs](https://gaushao.gitlab.io/react-simple-map-globe/docs)\
 * open UI:\
 * [[CameraSettings]]\
 * [[CartographySettings]]
 *
 * @category Component
 * @returns `wrap`\
 * [[CartographyMenu]]\
 * [[CameraMenu]]
 */
export function Menu({
  settings,
  markers,
}: PropsWithChildren<MenuProps>): JSX.Element {
  const { bool: visible, negate: toggle } = useBooleanState(false)
  return (
    <Container>
      <Buttons>
        <SettingsButton onClick={toggle} />
        <GitlabButton />
        <DocsButton />
      </Buttons>
      {visible && (
        <>
          <MarkersMenu markers={markers} />
          <ViewMenu settings={settings} />
          <CartographyMenu settings={settings} />
          <CameraMenu settings={settings} />
          <TopologyMenu settings={settings} />
        </>
      )}
      <div id='GLOBE_DEBUGGER' />
    </Container>
  )
}

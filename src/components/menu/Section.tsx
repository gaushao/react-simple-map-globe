/**
 * UI menu container
 * @category Menu
 * @module Menu Section
 */
import { PropsWithChildren } from "react";
import styled from "styled-components";
import { useBooleanState } from "../globe/hooks";

const Container = styled.div``;

/**
 * @category Props
 */
interface SectionProps {
  title: string;
}

/**
 * toggles visibility of children by clicking title
 * @category Component
 */
export function Section({ title, children }: PropsWithChildren<SectionProps>) {
  const { bool: visible, negate: toggle } = useBooleanState(false);
  return (
    <Container>
      {visible ? (
        <h4 onClick={toggle}>+ {title}</h4>
      ) : (
        <h4 onClick={toggle}>- {title}</h4>
      )}
      {visible && children}
    </Container>
  );
}

export default Section;

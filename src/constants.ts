/**
 * used to test globe module features
 * @category Dev
 * @module Dev Constants
 */

import { Coord } from './components/globe/classes'
import { MarkerData } from './components/globe/markers/types'
import { GlobeProps } from './components/globe/types'
import { ViewData } from './components/globe/view/classes'

export const DEV_VIEW_DATA = new ViewData(
  undefined,
  undefined,
  document.body,
  true
)

/**
 * dummy locations to [[Marker]] testing
 * @category Constant
 */
export const LOCATIONS: Record<string, MarkerData> = {
  NYC: {
    id: 'nyc',
    coordinates: new Coord(-74.006111, 40.712778),
    // svg: CustomMaker,
    pin: {
      fill: 'blue',
    },
    label: {
      text: 'New York',
      style: {
        stroke: 'darkblue',
      },
    },
  },
  BRAZIL: {
    // props: {
    //   onClick: (e) => console.log(e),
    //   hover: {
    //     enter: {
    //       callback: (e: any) => console.log(e),
    //       props: {},
    //     },
    //   },
    // },
    id: 'br',
    coordinates: new Coord(-47.8825, -15.7942),
    pin: {
      fill: 'green',
    },
    label: {
      text: 'Brazil',
      style: {
        stroke: 'green',
      },
    },
  },
}

/**
 * dummy props to [[Globe]] testing
 * @category Constant
 */
export const PROPS: GlobeProps = {
  camera: {
    // onMouseDown: (e: any) => console.log(e),
    // onWheel: (e: any) => console.log(e),
    // onTouchMove: (e: any) => console.log(e.geoCameraData),
  },
  marker: {
    label: {
      text: '',
      textAnchor: 'middle',
      y: 24,
      style: {
        fill: 'white',
        fontFamily: 'system-ui',
      },
    },
    pin: {
      shape: 'circle',
      fill: 'white',
      stroke: 'black',
      strokeWidth: 2,
      r: 8,
    },
  },
  geo: {
    // onClick: (e: any) => console.log('onClick', e.globePathData),
    // onMouseEnter: (e: any) => console.log('onMouseEnter', e.globePathData),
    // onMouseDown: (e: any) => console.log('onMouseDown', e.globePathData),
    hover: {
      enter: {
        // callback: console.log,
        props: {
          style: {
            fill: 'red',
            outline: 'none',
          },
        },
      },
    },
  },
}

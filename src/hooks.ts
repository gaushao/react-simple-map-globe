/**
 * application root
 * easly setting of globe states
 * @category Dev
 * @module  Dev Hooks
 */

import { useCallback, useState } from 'react'
import { EMPTY_CAMERA_DATA } from './components/globe/camera/constants'
import { EMPTY_CARTOGRAPHY_DATA } from './components/globe/cartography/constants'
import { MarkerData } from './components/globe/markers/types'
import { GlobeSettings, MutableState } from './components/globe/types'
import { DEV_VIEW_DATA, LOCATIONS } from './constants'

/**
 * initialize settings states
 * @category State
 */
export function useSettings(): GlobeSettings {
  const view = useState(DEV_VIEW_DATA)
  const camera = useState(EMPTY_CAMERA_DATA)
  const cartography = useState(EMPTY_CARTOGRAPHY_DATA)
  return { view, camera, cartography }
}

/**
 * setup state for markers
 * @category State
 */
export function useMarkers(): MutableState<MarkerData[]> {
  const markers = useState([LOCATIONS.BRAZIL, LOCATIONS.NYC])
  return markers
}

/**
 * set innerText to debug div
 * @category Tool
 */
export function useDebugger() {
  const element = window.document.getElementById('GLOBE_DEBUGGER')
  return useCallback(
    (text: string) => {
      if (!element) return
      element.innerText = text
    },
    [element]
  )
}

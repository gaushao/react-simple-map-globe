[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Types](../modules/Globe_Types.md) / GlobeProps

# Interface: GlobeProps

[Globe Types](../modules/Globe_Types.md).GlobeProps

root component props

## Table of contents

### Properties

- [camera](Globe_Types.GlobeProps.md#camera)
- [geo](Globe_Types.GlobeProps.md#geo)
- [marker](Globe_Types.GlobeProps.md#marker)
- [markers](Globe_Types.GlobeProps.md#markers)
- [settings](Globe_Types.GlobeProps.md#settings)

## Properties

### camera

• `Optional` **camera**: [`CameraProps`](../classes/Globe_Camera_Classes.CameraProps.md)

#### Defined in

[src/components/globe/types.ts:68](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L68)

___

### geo

• `Optional` **geo**: `Partial`<[`PathProps`](Globe_Cartography_Types.PathProps.md)\>

#### Defined in

[src/components/globe/types.ts:66](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L66)

___

### marker

• `Optional` **marker**: [`MarkerProps`](Globe_Markers_Types.MarkerProps.md)

#### Defined in

[src/components/globe/types.ts:67](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L67)

___

### markers

• `Optional` **markers**: [`MarkerData`](Globe_Markers_Types.MarkerData.md)[]

#### Defined in

[src/components/globe/types.ts:64](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L64)

___

### settings

• `Optional` **settings**: [`GlobeSettings`](Globe_Types.GlobeSettings.md)

#### Defined in

[src/components/globe/types.ts:65](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L65)

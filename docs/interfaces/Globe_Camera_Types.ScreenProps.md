[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Types](../modules/Globe_Camera_Types.md) / ScreenProps

# Interface: ScreenProps

[Globe Camera Types](../modules/Globe_Camera_Types.md).ScreenProps

## Hierarchy

- `PropsWithChildren`<`React.DetailedHTMLProps`<`React.HTMLAttributes`<`HTMLDivElement`\>, `HTMLDivElement`\>\>

  ↳ **`ScreenProps`**

## Table of contents

### Properties

- [about](Globe_Camera_Types.ScreenProps.md#about)
- [accessKey](Globe_Camera_Types.ScreenProps.md#accesskey)
- [aria-activedescendant](Globe_Camera_Types.ScreenProps.md#aria-activedescendant)
- [aria-atomic](Globe_Camera_Types.ScreenProps.md#aria-atomic)
- [aria-autocomplete](Globe_Camera_Types.ScreenProps.md#aria-autocomplete)
- [aria-busy](Globe_Camera_Types.ScreenProps.md#aria-busy)
- [aria-checked](Globe_Camera_Types.ScreenProps.md#aria-checked)
- [aria-colcount](Globe_Camera_Types.ScreenProps.md#aria-colcount)
- [aria-colindex](Globe_Camera_Types.ScreenProps.md#aria-colindex)
- [aria-colspan](Globe_Camera_Types.ScreenProps.md#aria-colspan)
- [aria-controls](Globe_Camera_Types.ScreenProps.md#aria-controls)
- [aria-current](Globe_Camera_Types.ScreenProps.md#aria-current)
- [aria-describedby](Globe_Camera_Types.ScreenProps.md#aria-describedby)
- [aria-details](Globe_Camera_Types.ScreenProps.md#aria-details)
- [aria-disabled](Globe_Camera_Types.ScreenProps.md#aria-disabled)
- [aria-dropeffect](Globe_Camera_Types.ScreenProps.md#aria-dropeffect)
- [aria-errormessage](Globe_Camera_Types.ScreenProps.md#aria-errormessage)
- [aria-expanded](Globe_Camera_Types.ScreenProps.md#aria-expanded)
- [aria-flowto](Globe_Camera_Types.ScreenProps.md#aria-flowto)
- [aria-grabbed](Globe_Camera_Types.ScreenProps.md#aria-grabbed)
- [aria-haspopup](Globe_Camera_Types.ScreenProps.md#aria-haspopup)
- [aria-hidden](Globe_Camera_Types.ScreenProps.md#aria-hidden)
- [aria-invalid](Globe_Camera_Types.ScreenProps.md#aria-invalid)
- [aria-keyshortcuts](Globe_Camera_Types.ScreenProps.md#aria-keyshortcuts)
- [aria-label](Globe_Camera_Types.ScreenProps.md#aria-label)
- [aria-labelledby](Globe_Camera_Types.ScreenProps.md#aria-labelledby)
- [aria-level](Globe_Camera_Types.ScreenProps.md#aria-level)
- [aria-live](Globe_Camera_Types.ScreenProps.md#aria-live)
- [aria-modal](Globe_Camera_Types.ScreenProps.md#aria-modal)
- [aria-multiline](Globe_Camera_Types.ScreenProps.md#aria-multiline)
- [aria-multiselectable](Globe_Camera_Types.ScreenProps.md#aria-multiselectable)
- [aria-orientation](Globe_Camera_Types.ScreenProps.md#aria-orientation)
- [aria-owns](Globe_Camera_Types.ScreenProps.md#aria-owns)
- [aria-placeholder](Globe_Camera_Types.ScreenProps.md#aria-placeholder)
- [aria-posinset](Globe_Camera_Types.ScreenProps.md#aria-posinset)
- [aria-pressed](Globe_Camera_Types.ScreenProps.md#aria-pressed)
- [aria-readonly](Globe_Camera_Types.ScreenProps.md#aria-readonly)
- [aria-relevant](Globe_Camera_Types.ScreenProps.md#aria-relevant)
- [aria-required](Globe_Camera_Types.ScreenProps.md#aria-required)
- [aria-roledescription](Globe_Camera_Types.ScreenProps.md#aria-roledescription)
- [aria-rowcount](Globe_Camera_Types.ScreenProps.md#aria-rowcount)
- [aria-rowindex](Globe_Camera_Types.ScreenProps.md#aria-rowindex)
- [aria-rowspan](Globe_Camera_Types.ScreenProps.md#aria-rowspan)
- [aria-selected](Globe_Camera_Types.ScreenProps.md#aria-selected)
- [aria-setsize](Globe_Camera_Types.ScreenProps.md#aria-setsize)
- [aria-sort](Globe_Camera_Types.ScreenProps.md#aria-sort)
- [aria-valuemax](Globe_Camera_Types.ScreenProps.md#aria-valuemax)
- [aria-valuemin](Globe_Camera_Types.ScreenProps.md#aria-valuemin)
- [aria-valuenow](Globe_Camera_Types.ScreenProps.md#aria-valuenow)
- [aria-valuetext](Globe_Camera_Types.ScreenProps.md#aria-valuetext)
- [autoCapitalize](Globe_Camera_Types.ScreenProps.md#autocapitalize)
- [autoCorrect](Globe_Camera_Types.ScreenProps.md#autocorrect)
- [autoSave](Globe_Camera_Types.ScreenProps.md#autosave)
- [children](Globe_Camera_Types.ScreenProps.md#children)
- [className](Globe_Camera_Types.ScreenProps.md#classname)
- [color](Globe_Camera_Types.ScreenProps.md#color)
- [contentEditable](Globe_Camera_Types.ScreenProps.md#contenteditable)
- [contextMenu](Globe_Camera_Types.ScreenProps.md#contextmenu)
- [dangerouslySetInnerHTML](Globe_Camera_Types.ScreenProps.md#dangerouslysetinnerhtml)
- [datatype](Globe_Camera_Types.ScreenProps.md#datatype)
- [defaultChecked](Globe_Camera_Types.ScreenProps.md#defaultchecked)
- [defaultValue](Globe_Camera_Types.ScreenProps.md#defaultvalue)
- [dir](Globe_Camera_Types.ScreenProps.md#dir)
- [draggable](Globe_Camera_Types.ScreenProps.md#draggable)
- [hidden](Globe_Camera_Types.ScreenProps.md#hidden)
- [id](Globe_Camera_Types.ScreenProps.md#id)
- [inlist](Globe_Camera_Types.ScreenProps.md#inlist)
- [inputMode](Globe_Camera_Types.ScreenProps.md#inputmode)
- [is](Globe_Camera_Types.ScreenProps.md#is)
- [itemID](Globe_Camera_Types.ScreenProps.md#itemid)
- [itemProp](Globe_Camera_Types.ScreenProps.md#itemprop)
- [itemRef](Globe_Camera_Types.ScreenProps.md#itemref)
- [itemScope](Globe_Camera_Types.ScreenProps.md#itemscope)
- [itemType](Globe_Camera_Types.ScreenProps.md#itemtype)
- [key](Globe_Camera_Types.ScreenProps.md#key)
- [lang](Globe_Camera_Types.ScreenProps.md#lang)
- [onAbort](Globe_Camera_Types.ScreenProps.md#onabort)
- [onAbortCapture](Globe_Camera_Types.ScreenProps.md#onabortcapture)
- [onAnimationEnd](Globe_Camera_Types.ScreenProps.md#onanimationend)
- [onAnimationEndCapture](Globe_Camera_Types.ScreenProps.md#onanimationendcapture)
- [onAnimationIteration](Globe_Camera_Types.ScreenProps.md#onanimationiteration)
- [onAnimationIterationCapture](Globe_Camera_Types.ScreenProps.md#onanimationiterationcapture)
- [onAnimationStart](Globe_Camera_Types.ScreenProps.md#onanimationstart)
- [onAnimationStartCapture](Globe_Camera_Types.ScreenProps.md#onanimationstartcapture)
- [onAuxClick](Globe_Camera_Types.ScreenProps.md#onauxclick)
- [onAuxClickCapture](Globe_Camera_Types.ScreenProps.md#onauxclickcapture)
- [onBeforeInput](Globe_Camera_Types.ScreenProps.md#onbeforeinput)
- [onBeforeInputCapture](Globe_Camera_Types.ScreenProps.md#onbeforeinputcapture)
- [onBlur](Globe_Camera_Types.ScreenProps.md#onblur)
- [onBlurCapture](Globe_Camera_Types.ScreenProps.md#onblurcapture)
- [onCanPlay](Globe_Camera_Types.ScreenProps.md#oncanplay)
- [onCanPlayCapture](Globe_Camera_Types.ScreenProps.md#oncanplaycapture)
- [onCanPlayThrough](Globe_Camera_Types.ScreenProps.md#oncanplaythrough)
- [onCanPlayThroughCapture](Globe_Camera_Types.ScreenProps.md#oncanplaythroughcapture)
- [onChange](Globe_Camera_Types.ScreenProps.md#onchange)
- [onChangeCapture](Globe_Camera_Types.ScreenProps.md#onchangecapture)
- [onClick](Globe_Camera_Types.ScreenProps.md#onclick)
- [onClickCapture](Globe_Camera_Types.ScreenProps.md#onclickcapture)
- [onCompositionEnd](Globe_Camera_Types.ScreenProps.md#oncompositionend)
- [onCompositionEndCapture](Globe_Camera_Types.ScreenProps.md#oncompositionendcapture)
- [onCompositionStart](Globe_Camera_Types.ScreenProps.md#oncompositionstart)
- [onCompositionStartCapture](Globe_Camera_Types.ScreenProps.md#oncompositionstartcapture)
- [onCompositionUpdate](Globe_Camera_Types.ScreenProps.md#oncompositionupdate)
- [onCompositionUpdateCapture](Globe_Camera_Types.ScreenProps.md#oncompositionupdatecapture)
- [onContextMenu](Globe_Camera_Types.ScreenProps.md#oncontextmenu)
- [onContextMenuCapture](Globe_Camera_Types.ScreenProps.md#oncontextmenucapture)
- [onCopy](Globe_Camera_Types.ScreenProps.md#oncopy)
- [onCopyCapture](Globe_Camera_Types.ScreenProps.md#oncopycapture)
- [onCut](Globe_Camera_Types.ScreenProps.md#oncut)
- [onCutCapture](Globe_Camera_Types.ScreenProps.md#oncutcapture)
- [onDoubleClick](Globe_Camera_Types.ScreenProps.md#ondoubleclick)
- [onDoubleClickCapture](Globe_Camera_Types.ScreenProps.md#ondoubleclickcapture)
- [onDrag](Globe_Camera_Types.ScreenProps.md#ondrag)
- [onDragCapture](Globe_Camera_Types.ScreenProps.md#ondragcapture)
- [onDragEnd](Globe_Camera_Types.ScreenProps.md#ondragend)
- [onDragEndCapture](Globe_Camera_Types.ScreenProps.md#ondragendcapture)
- [onDragEnter](Globe_Camera_Types.ScreenProps.md#ondragenter)
- [onDragEnterCapture](Globe_Camera_Types.ScreenProps.md#ondragentercapture)
- [onDragExit](Globe_Camera_Types.ScreenProps.md#ondragexit)
- [onDragExitCapture](Globe_Camera_Types.ScreenProps.md#ondragexitcapture)
- [onDragLeave](Globe_Camera_Types.ScreenProps.md#ondragleave)
- [onDragLeaveCapture](Globe_Camera_Types.ScreenProps.md#ondragleavecapture)
- [onDragOver](Globe_Camera_Types.ScreenProps.md#ondragover)
- [onDragOverCapture](Globe_Camera_Types.ScreenProps.md#ondragovercapture)
- [onDragStart](Globe_Camera_Types.ScreenProps.md#ondragstart)
- [onDragStartCapture](Globe_Camera_Types.ScreenProps.md#ondragstartcapture)
- [onDrop](Globe_Camera_Types.ScreenProps.md#ondrop)
- [onDropCapture](Globe_Camera_Types.ScreenProps.md#ondropcapture)
- [onDurationChange](Globe_Camera_Types.ScreenProps.md#ondurationchange)
- [onDurationChangeCapture](Globe_Camera_Types.ScreenProps.md#ondurationchangecapture)
- [onEmptied](Globe_Camera_Types.ScreenProps.md#onemptied)
- [onEmptiedCapture](Globe_Camera_Types.ScreenProps.md#onemptiedcapture)
- [onEncrypted](Globe_Camera_Types.ScreenProps.md#onencrypted)
- [onEncryptedCapture](Globe_Camera_Types.ScreenProps.md#onencryptedcapture)
- [onEnded](Globe_Camera_Types.ScreenProps.md#onended)
- [onEndedCapture](Globe_Camera_Types.ScreenProps.md#onendedcapture)
- [onError](Globe_Camera_Types.ScreenProps.md#onerror)
- [onErrorCapture](Globe_Camera_Types.ScreenProps.md#onerrorcapture)
- [onFocus](Globe_Camera_Types.ScreenProps.md#onfocus)
- [onFocusCapture](Globe_Camera_Types.ScreenProps.md#onfocuscapture)
- [onGotPointerCapture](Globe_Camera_Types.ScreenProps.md#ongotpointercapture)
- [onGotPointerCaptureCapture](Globe_Camera_Types.ScreenProps.md#ongotpointercapturecapture)
- [onInput](Globe_Camera_Types.ScreenProps.md#oninput)
- [onInputCapture](Globe_Camera_Types.ScreenProps.md#oninputcapture)
- [onInvalid](Globe_Camera_Types.ScreenProps.md#oninvalid)
- [onInvalidCapture](Globe_Camera_Types.ScreenProps.md#oninvalidcapture)
- [onKeyDown](Globe_Camera_Types.ScreenProps.md#onkeydown)
- [onKeyDownCapture](Globe_Camera_Types.ScreenProps.md#onkeydowncapture)
- [onKeyPress](Globe_Camera_Types.ScreenProps.md#onkeypress)
- [onKeyPressCapture](Globe_Camera_Types.ScreenProps.md#onkeypresscapture)
- [onKeyUp](Globe_Camera_Types.ScreenProps.md#onkeyup)
- [onKeyUpCapture](Globe_Camera_Types.ScreenProps.md#onkeyupcapture)
- [onLoad](Globe_Camera_Types.ScreenProps.md#onload)
- [onLoadCapture](Globe_Camera_Types.ScreenProps.md#onloadcapture)
- [onLoadStart](Globe_Camera_Types.ScreenProps.md#onloadstart)
- [onLoadStartCapture](Globe_Camera_Types.ScreenProps.md#onloadstartcapture)
- [onLoadedData](Globe_Camera_Types.ScreenProps.md#onloadeddata)
- [onLoadedDataCapture](Globe_Camera_Types.ScreenProps.md#onloadeddatacapture)
- [onLoadedMetadata](Globe_Camera_Types.ScreenProps.md#onloadedmetadata)
- [onLoadedMetadataCapture](Globe_Camera_Types.ScreenProps.md#onloadedmetadatacapture)
- [onLostPointerCapture](Globe_Camera_Types.ScreenProps.md#onlostpointercapture)
- [onLostPointerCaptureCapture](Globe_Camera_Types.ScreenProps.md#onlostpointercapturecapture)
- [onMouseDown](Globe_Camera_Types.ScreenProps.md#onmousedown)
- [onMouseDownCapture](Globe_Camera_Types.ScreenProps.md#onmousedowncapture)
- [onMouseEnter](Globe_Camera_Types.ScreenProps.md#onmouseenter)
- [onMouseLeave](Globe_Camera_Types.ScreenProps.md#onmouseleave)
- [onMouseMove](Globe_Camera_Types.ScreenProps.md#onmousemove)
- [onMouseMoveCapture](Globe_Camera_Types.ScreenProps.md#onmousemovecapture)
- [onMouseOut](Globe_Camera_Types.ScreenProps.md#onmouseout)
- [onMouseOutCapture](Globe_Camera_Types.ScreenProps.md#onmouseoutcapture)
- [onMouseOver](Globe_Camera_Types.ScreenProps.md#onmouseover)
- [onMouseOverCapture](Globe_Camera_Types.ScreenProps.md#onmouseovercapture)
- [onMouseUp](Globe_Camera_Types.ScreenProps.md#onmouseup)
- [onMouseUpCapture](Globe_Camera_Types.ScreenProps.md#onmouseupcapture)
- [onPaste](Globe_Camera_Types.ScreenProps.md#onpaste)
- [onPasteCapture](Globe_Camera_Types.ScreenProps.md#onpastecapture)
- [onPause](Globe_Camera_Types.ScreenProps.md#onpause)
- [onPauseCapture](Globe_Camera_Types.ScreenProps.md#onpausecapture)
- [onPlay](Globe_Camera_Types.ScreenProps.md#onplay)
- [onPlayCapture](Globe_Camera_Types.ScreenProps.md#onplaycapture)
- [onPlaying](Globe_Camera_Types.ScreenProps.md#onplaying)
- [onPlayingCapture](Globe_Camera_Types.ScreenProps.md#onplayingcapture)
- [onPointerCancel](Globe_Camera_Types.ScreenProps.md#onpointercancel)
- [onPointerCancelCapture](Globe_Camera_Types.ScreenProps.md#onpointercancelcapture)
- [onPointerDown](Globe_Camera_Types.ScreenProps.md#onpointerdown)
- [onPointerDownCapture](Globe_Camera_Types.ScreenProps.md#onpointerdowncapture)
- [onPointerEnter](Globe_Camera_Types.ScreenProps.md#onpointerenter)
- [onPointerEnterCapture](Globe_Camera_Types.ScreenProps.md#onpointerentercapture)
- [onPointerLeave](Globe_Camera_Types.ScreenProps.md#onpointerleave)
- [onPointerLeaveCapture](Globe_Camera_Types.ScreenProps.md#onpointerleavecapture)
- [onPointerMove](Globe_Camera_Types.ScreenProps.md#onpointermove)
- [onPointerMoveCapture](Globe_Camera_Types.ScreenProps.md#onpointermovecapture)
- [onPointerOut](Globe_Camera_Types.ScreenProps.md#onpointerout)
- [onPointerOutCapture](Globe_Camera_Types.ScreenProps.md#onpointeroutcapture)
- [onPointerOver](Globe_Camera_Types.ScreenProps.md#onpointerover)
- [onPointerOverCapture](Globe_Camera_Types.ScreenProps.md#onpointerovercapture)
- [onPointerUp](Globe_Camera_Types.ScreenProps.md#onpointerup)
- [onPointerUpCapture](Globe_Camera_Types.ScreenProps.md#onpointerupcapture)
- [onProgress](Globe_Camera_Types.ScreenProps.md#onprogress)
- [onProgressCapture](Globe_Camera_Types.ScreenProps.md#onprogresscapture)
- [onRateChange](Globe_Camera_Types.ScreenProps.md#onratechange)
- [onRateChangeCapture](Globe_Camera_Types.ScreenProps.md#onratechangecapture)
- [onReset](Globe_Camera_Types.ScreenProps.md#onreset)
- [onResetCapture](Globe_Camera_Types.ScreenProps.md#onresetcapture)
- [onScroll](Globe_Camera_Types.ScreenProps.md#onscroll)
- [onScrollCapture](Globe_Camera_Types.ScreenProps.md#onscrollcapture)
- [onSeeked](Globe_Camera_Types.ScreenProps.md#onseeked)
- [onSeekedCapture](Globe_Camera_Types.ScreenProps.md#onseekedcapture)
- [onSeeking](Globe_Camera_Types.ScreenProps.md#onseeking)
- [onSeekingCapture](Globe_Camera_Types.ScreenProps.md#onseekingcapture)
- [onSelect](Globe_Camera_Types.ScreenProps.md#onselect)
- [onSelectCapture](Globe_Camera_Types.ScreenProps.md#onselectcapture)
- [onStalled](Globe_Camera_Types.ScreenProps.md#onstalled)
- [onStalledCapture](Globe_Camera_Types.ScreenProps.md#onstalledcapture)
- [onSubmit](Globe_Camera_Types.ScreenProps.md#onsubmit)
- [onSubmitCapture](Globe_Camera_Types.ScreenProps.md#onsubmitcapture)
- [onSuspend](Globe_Camera_Types.ScreenProps.md#onsuspend)
- [onSuspendCapture](Globe_Camera_Types.ScreenProps.md#onsuspendcapture)
- [onTimeUpdate](Globe_Camera_Types.ScreenProps.md#ontimeupdate)
- [onTimeUpdateCapture](Globe_Camera_Types.ScreenProps.md#ontimeupdatecapture)
- [onTouchCancel](Globe_Camera_Types.ScreenProps.md#ontouchcancel)
- [onTouchCancelCapture](Globe_Camera_Types.ScreenProps.md#ontouchcancelcapture)
- [onTouchEnd](Globe_Camera_Types.ScreenProps.md#ontouchend)
- [onTouchEndCapture](Globe_Camera_Types.ScreenProps.md#ontouchendcapture)
- [onTouchMove](Globe_Camera_Types.ScreenProps.md#ontouchmove)
- [onTouchMoveCapture](Globe_Camera_Types.ScreenProps.md#ontouchmovecapture)
- [onTouchStart](Globe_Camera_Types.ScreenProps.md#ontouchstart)
- [onTouchStartCapture](Globe_Camera_Types.ScreenProps.md#ontouchstartcapture)
- [onTransitionEnd](Globe_Camera_Types.ScreenProps.md#ontransitionend)
- [onTransitionEndCapture](Globe_Camera_Types.ScreenProps.md#ontransitionendcapture)
- [onVolumeChange](Globe_Camera_Types.ScreenProps.md#onvolumechange)
- [onVolumeChangeCapture](Globe_Camera_Types.ScreenProps.md#onvolumechangecapture)
- [onWaiting](Globe_Camera_Types.ScreenProps.md#onwaiting)
- [onWaitingCapture](Globe_Camera_Types.ScreenProps.md#onwaitingcapture)
- [onWheel](Globe_Camera_Types.ScreenProps.md#onwheel)
- [onWheelCapture](Globe_Camera_Types.ScreenProps.md#onwheelcapture)
- [placeholder](Globe_Camera_Types.ScreenProps.md#placeholder)
- [prefix](Globe_Camera_Types.ScreenProps.md#prefix)
- [property](Globe_Camera_Types.ScreenProps.md#property)
- [radioGroup](Globe_Camera_Types.ScreenProps.md#radiogroup)
- [ref](Globe_Camera_Types.ScreenProps.md#ref)
- [resource](Globe_Camera_Types.ScreenProps.md#resource)
- [results](Globe_Camera_Types.ScreenProps.md#results)
- [role](Globe_Camera_Types.ScreenProps.md#role)
- [security](Globe_Camera_Types.ScreenProps.md#security)
- [slot](Globe_Camera_Types.ScreenProps.md#slot)
- [spellCheck](Globe_Camera_Types.ScreenProps.md#spellcheck)
- [style](Globe_Camera_Types.ScreenProps.md#style)
- [suppressContentEditableWarning](Globe_Camera_Types.ScreenProps.md#suppresscontenteditablewarning)
- [suppressHydrationWarning](Globe_Camera_Types.ScreenProps.md#suppresshydrationwarning)
- [tabIndex](Globe_Camera_Types.ScreenProps.md#tabindex)
- [title](Globe_Camera_Types.ScreenProps.md#title)
- [translate](Globe_Camera_Types.ScreenProps.md#translate)
- [typeof](Globe_Camera_Types.ScreenProps.md#typeof)
- [unselectable](Globe_Camera_Types.ScreenProps.md#unselectable)
- [vocab](Globe_Camera_Types.ScreenProps.md#vocab)

## Properties

### about

• `Optional` **about**: `string`

#### Inherited from

React.PropsWithChildren.about

#### Defined in

node_modules/@types/react/index.d.ts:1859

___

### accessKey

• `Optional` **accessKey**: `string`

#### Inherited from

React.PropsWithChildren.accessKey

#### Defined in

node_modules/@types/react/index.d.ts:1835

___

### aria-activedescendant

• `Optional` **aria-activedescendant**: `string`

Identifies the currently active element when DOM focus is on a composite widget, textbox, group, or application.

#### Inherited from

React.PropsWithChildren.aria-activedescendant

#### Defined in

node_modules/@types/react/index.d.ts:1569

___

### aria-atomic

• `Optional` **aria-atomic**: `Booleanish`

Indicates whether assistive technologies will present all, or only parts of, the changed region based on the change notifications defined by the aria-relevant attribute.

#### Inherited from

React.PropsWithChildren.aria-atomic

#### Defined in

node_modules/@types/react/index.d.ts:1571

___

### aria-autocomplete

• `Optional` **aria-autocomplete**: ``"none"`` \| ``"list"`` \| ``"inline"`` \| ``"both"``

Indicates whether inputting text could trigger display of one or more predictions of the user's intended value for an input and specifies how predictions would be
presented if they are made.

#### Inherited from

React.PropsWithChildren.aria-autocomplete

#### Defined in

node_modules/@types/react/index.d.ts:1576

___

### aria-busy

• `Optional` **aria-busy**: `Booleanish`

Indicates an element is being modified and that assistive technologies MAY want to wait until the modifications are complete before exposing them to the user.

#### Inherited from

React.PropsWithChildren.aria-busy

#### Defined in

node_modules/@types/react/index.d.ts:1578

___

### aria-checked

• `Optional` **aria-checked**: `boolean` \| ``"false"`` \| ``"true"`` \| ``"mixed"``

Indicates the current "checked" state of checkboxes, radio buttons, and other widgets.

**`See`**

 - aria-pressed
 - aria-selected.

#### Inherited from

React.PropsWithChildren.aria-checked

#### Defined in

node_modules/@types/react/index.d.ts:1583

___

### aria-colcount

• `Optional` **aria-colcount**: `number`

Defines the total number of columns in a table, grid, or treegrid.

**`See`**

aria-colindex.

#### Inherited from

React.PropsWithChildren.aria-colcount

#### Defined in

node_modules/@types/react/index.d.ts:1588

___

### aria-colindex

• `Optional` **aria-colindex**: `number`

Defines an element's column index or position with respect to the total number of columns within a table, grid, or treegrid.

**`See`**

 - aria-colcount
 - aria-colspan.

#### Inherited from

React.PropsWithChildren.aria-colindex

#### Defined in

node_modules/@types/react/index.d.ts:1593

___

### aria-colspan

• `Optional` **aria-colspan**: `number`

Defines the number of columns spanned by a cell or gridcell within a table, grid, or treegrid.

**`See`**

 - aria-colindex
 - aria-rowspan.

#### Inherited from

React.PropsWithChildren.aria-colspan

#### Defined in

node_modules/@types/react/index.d.ts:1598

___

### aria-controls

• `Optional` **aria-controls**: `string`

Identifies the element (or elements) whose contents or presence are controlled by the current element.

**`See`**

aria-owns.

#### Inherited from

React.PropsWithChildren.aria-controls

#### Defined in

node_modules/@types/react/index.d.ts:1603

___

### aria-current

• `Optional` **aria-current**: `boolean` \| ``"time"`` \| ``"false"`` \| ``"true"`` \| ``"page"`` \| ``"step"`` \| ``"location"`` \| ``"date"``

Indicates the element that represents the current item within a container or set of related elements.

#### Inherited from

React.PropsWithChildren.aria-current

#### Defined in

node_modules/@types/react/index.d.ts:1605

___

### aria-describedby

• `Optional` **aria-describedby**: `string`

Identifies the element (or elements) that describes the object.

**`See`**

aria-labelledby

#### Inherited from

React.PropsWithChildren.aria-describedby

#### Defined in

node_modules/@types/react/index.d.ts:1610

___

### aria-details

• `Optional` **aria-details**: `string`

Identifies the element that provides a detailed, extended description for the object.

**`See`**

aria-describedby.

#### Inherited from

React.PropsWithChildren.aria-details

#### Defined in

node_modules/@types/react/index.d.ts:1615

___

### aria-disabled

• `Optional` **aria-disabled**: `Booleanish`

Indicates that the element is perceivable but disabled, so it is not editable or otherwise operable.

**`See`**

 - aria-hidden
 - aria-readonly.

#### Inherited from

React.PropsWithChildren.aria-disabled

#### Defined in

node_modules/@types/react/index.d.ts:1620

___

### aria-dropeffect

• `Optional` **aria-dropeffect**: ``"link"`` \| ``"move"`` \| ``"none"`` \| ``"copy"`` \| ``"execute"`` \| ``"popup"``

Indicates what functions can be performed when a dragged object is released on the drop target.

**`Deprecated`**

in ARIA 1.1

#### Inherited from

React.PropsWithChildren.aria-dropeffect

#### Defined in

node_modules/@types/react/index.d.ts:1625

___

### aria-errormessage

• `Optional` **aria-errormessage**: `string`

Identifies the element that provides an error message for the object.

**`See`**

 - aria-invalid
 - aria-describedby.

#### Inherited from

React.PropsWithChildren.aria-errormessage

#### Defined in

node_modules/@types/react/index.d.ts:1630

___

### aria-expanded

• `Optional` **aria-expanded**: `Booleanish`

Indicates whether the element, or another grouping element it controls, is currently expanded or collapsed.

#### Inherited from

React.PropsWithChildren.aria-expanded

#### Defined in

node_modules/@types/react/index.d.ts:1632

___

### aria-flowto

• `Optional` **aria-flowto**: `string`

Identifies the next element (or elements) in an alternate reading order of content which, at the user's discretion,
allows assistive technology to override the general default of reading in document source order.

#### Inherited from

React.PropsWithChildren.aria-flowto

#### Defined in

node_modules/@types/react/index.d.ts:1637

___

### aria-grabbed

• `Optional` **aria-grabbed**: `Booleanish`

Indicates an element's "grabbed" state in a drag-and-drop operation.

**`Deprecated`**

in ARIA 1.1

#### Inherited from

React.PropsWithChildren.aria-grabbed

#### Defined in

node_modules/@types/react/index.d.ts:1642

___

### aria-haspopup

• `Optional` **aria-haspopup**: `boolean` \| ``"dialog"`` \| ``"menu"`` \| ``"false"`` \| ``"true"`` \| ``"grid"`` \| ``"listbox"`` \| ``"tree"``

Indicates the availability and type of interactive popup element, such as menu or dialog, that can be triggered by an element.

#### Inherited from

React.PropsWithChildren.aria-haspopup

#### Defined in

node_modules/@types/react/index.d.ts:1644

___

### aria-hidden

• `Optional` **aria-hidden**: `Booleanish`

Indicates whether the element is exposed to an accessibility API.

**`See`**

aria-disabled.

#### Inherited from

React.PropsWithChildren.aria-hidden

#### Defined in

node_modules/@types/react/index.d.ts:1649

___

### aria-invalid

• `Optional` **aria-invalid**: `boolean` \| ``"false"`` \| ``"true"`` \| ``"grammar"`` \| ``"spelling"``

Indicates the entered value does not conform to the format expected by the application.

**`See`**

aria-errormessage.

#### Inherited from

React.PropsWithChildren.aria-invalid

#### Defined in

node_modules/@types/react/index.d.ts:1654

___

### aria-keyshortcuts

• `Optional` **aria-keyshortcuts**: `string`

Indicates keyboard shortcuts that an author has implemented to activate or give focus to an element.

#### Inherited from

React.PropsWithChildren.aria-keyshortcuts

#### Defined in

node_modules/@types/react/index.d.ts:1656

___

### aria-label

• `Optional` **aria-label**: `string`

Defines a string value that labels the current element.

**`See`**

aria-labelledby.

#### Inherited from

React.PropsWithChildren.aria-label

#### Defined in

node_modules/@types/react/index.d.ts:1661

___

### aria-labelledby

• `Optional` **aria-labelledby**: `string`

Identifies the element (or elements) that labels the current element.

**`See`**

aria-describedby.

#### Inherited from

React.PropsWithChildren.aria-labelledby

#### Defined in

node_modules/@types/react/index.d.ts:1666

___

### aria-level

• `Optional` **aria-level**: `number`

Defines the hierarchical level of an element within a structure.

#### Inherited from

React.PropsWithChildren.aria-level

#### Defined in

node_modules/@types/react/index.d.ts:1668

___

### aria-live

• `Optional` **aria-live**: ``"off"`` \| ``"assertive"`` \| ``"polite"``

Indicates that an element will be updated, and describes the types of updates the user agents, assistive technologies, and user can expect from the live region.

#### Inherited from

React.PropsWithChildren.aria-live

#### Defined in

node_modules/@types/react/index.d.ts:1670

___

### aria-modal

• `Optional` **aria-modal**: `Booleanish`

Indicates whether an element is modal when displayed.

#### Inherited from

React.PropsWithChildren.aria-modal

#### Defined in

node_modules/@types/react/index.d.ts:1672

___

### aria-multiline

• `Optional` **aria-multiline**: `Booleanish`

Indicates whether a text box accepts multiple lines of input or only a single line.

#### Inherited from

React.PropsWithChildren.aria-multiline

#### Defined in

node_modules/@types/react/index.d.ts:1674

___

### aria-multiselectable

• `Optional` **aria-multiselectable**: `Booleanish`

Indicates that the user may select more than one item from the current selectable descendants.

#### Inherited from

React.PropsWithChildren.aria-multiselectable

#### Defined in

node_modules/@types/react/index.d.ts:1676

___

### aria-orientation

• `Optional` **aria-orientation**: ``"horizontal"`` \| ``"vertical"``

Indicates whether the element's orientation is horizontal, vertical, or unknown/ambiguous.

#### Inherited from

React.PropsWithChildren.aria-orientation

#### Defined in

node_modules/@types/react/index.d.ts:1678

___

### aria-owns

• `Optional` **aria-owns**: `string`

Identifies an element (or elements) in order to define a visual, functional, or contextual parent/child relationship
between DOM elements where the DOM hierarchy cannot be used to represent the relationship.

**`See`**

aria-controls.

#### Inherited from

React.PropsWithChildren.aria-owns

#### Defined in

node_modules/@types/react/index.d.ts:1684

___

### aria-placeholder

• `Optional` **aria-placeholder**: `string`

Defines a short hint (a word or short phrase) intended to aid the user with data entry when the control has no value.
A hint could be a sample value or a brief description of the expected format.

#### Inherited from

React.PropsWithChildren.aria-placeholder

#### Defined in

node_modules/@types/react/index.d.ts:1689

___

### aria-posinset

• `Optional` **aria-posinset**: `number`

Defines an element's number or position in the current set of listitems or treeitems. Not required if all elements in the set are present in the DOM.

**`See`**

aria-setsize.

#### Inherited from

React.PropsWithChildren.aria-posinset

#### Defined in

node_modules/@types/react/index.d.ts:1694

___

### aria-pressed

• `Optional` **aria-pressed**: `boolean` \| ``"false"`` \| ``"true"`` \| ``"mixed"``

Indicates the current "pressed" state of toggle buttons.

**`See`**

 - aria-checked
 - aria-selected.

#### Inherited from

React.PropsWithChildren.aria-pressed

#### Defined in

node_modules/@types/react/index.d.ts:1699

___

### aria-readonly

• `Optional` **aria-readonly**: `Booleanish`

Indicates that the element is not editable, but is otherwise operable.

**`See`**

aria-disabled.

#### Inherited from

React.PropsWithChildren.aria-readonly

#### Defined in

node_modules/@types/react/index.d.ts:1704

___

### aria-relevant

• `Optional` **aria-relevant**: ``"text"`` \| ``"all"`` \| ``"additions"`` \| ``"additions removals"`` \| ``"additions text"`` \| ``"removals"`` \| ``"removals additions"`` \| ``"removals text"`` \| ``"text additions"`` \| ``"text removals"``

Indicates what notifications the user agent will trigger when the accessibility tree within a live region is modified.

**`See`**

aria-atomic.

#### Inherited from

React.PropsWithChildren.aria-relevant

#### Defined in

node_modules/@types/react/index.d.ts:1709

___

### aria-required

• `Optional` **aria-required**: `Booleanish`

Indicates that user input is required on the element before a form may be submitted.

#### Inherited from

React.PropsWithChildren.aria-required

#### Defined in

node_modules/@types/react/index.d.ts:1711

___

### aria-roledescription

• `Optional` **aria-roledescription**: `string`

Defines a human-readable, author-localized description for the role of an element.

#### Inherited from

React.PropsWithChildren.aria-roledescription

#### Defined in

node_modules/@types/react/index.d.ts:1713

___

### aria-rowcount

• `Optional` **aria-rowcount**: `number`

Defines the total number of rows in a table, grid, or treegrid.

**`See`**

aria-rowindex.

#### Inherited from

React.PropsWithChildren.aria-rowcount

#### Defined in

node_modules/@types/react/index.d.ts:1718

___

### aria-rowindex

• `Optional` **aria-rowindex**: `number`

Defines an element's row index or position with respect to the total number of rows within a table, grid, or treegrid.

**`See`**

 - aria-rowcount
 - aria-rowspan.

#### Inherited from

React.PropsWithChildren.aria-rowindex

#### Defined in

node_modules/@types/react/index.d.ts:1723

___

### aria-rowspan

• `Optional` **aria-rowspan**: `number`

Defines the number of rows spanned by a cell or gridcell within a table, grid, or treegrid.

**`See`**

 - aria-rowindex
 - aria-colspan.

#### Inherited from

React.PropsWithChildren.aria-rowspan

#### Defined in

node_modules/@types/react/index.d.ts:1728

___

### aria-selected

• `Optional` **aria-selected**: `Booleanish`

Indicates the current "selected" state of various widgets.

**`See`**

 - aria-checked
 - aria-pressed.

#### Inherited from

React.PropsWithChildren.aria-selected

#### Defined in

node_modules/@types/react/index.d.ts:1733

___

### aria-setsize

• `Optional` **aria-setsize**: `number`

Defines the number of items in the current set of listitems or treeitems. Not required if all elements in the set are present in the DOM.

**`See`**

aria-posinset.

#### Inherited from

React.PropsWithChildren.aria-setsize

#### Defined in

node_modules/@types/react/index.d.ts:1738

___

### aria-sort

• `Optional` **aria-sort**: ``"none"`` \| ``"ascending"`` \| ``"descending"`` \| ``"other"``

Indicates if items in a table or grid are sorted in ascending or descending order.

#### Inherited from

React.PropsWithChildren.aria-sort

#### Defined in

node_modules/@types/react/index.d.ts:1740

___

### aria-valuemax

• `Optional` **aria-valuemax**: `number`

Defines the maximum allowed value for a range widget.

#### Inherited from

React.PropsWithChildren.aria-valuemax

#### Defined in

node_modules/@types/react/index.d.ts:1742

___

### aria-valuemin

• `Optional` **aria-valuemin**: `number`

Defines the minimum allowed value for a range widget.

#### Inherited from

React.PropsWithChildren.aria-valuemin

#### Defined in

node_modules/@types/react/index.d.ts:1744

___

### aria-valuenow

• `Optional` **aria-valuenow**: `number`

Defines the current value for a range widget.

**`See`**

aria-valuetext.

#### Inherited from

React.PropsWithChildren.aria-valuenow

#### Defined in

node_modules/@types/react/index.d.ts:1749

___

### aria-valuetext

• `Optional` **aria-valuetext**: `string`

Defines the human readable text alternative of aria-valuenow for a range widget.

#### Inherited from

React.PropsWithChildren.aria-valuetext

#### Defined in

node_modules/@types/react/index.d.ts:1751

___

### autoCapitalize

• `Optional` **autoCapitalize**: `string`

#### Inherited from

React.PropsWithChildren.autoCapitalize

#### Defined in

node_modules/@types/react/index.d.ts:1869

___

### autoCorrect

• `Optional` **autoCorrect**: `string`

#### Inherited from

React.PropsWithChildren.autoCorrect

#### Defined in

node_modules/@types/react/index.d.ts:1870

___

### autoSave

• `Optional` **autoSave**: `string`

#### Inherited from

React.PropsWithChildren.autoSave

#### Defined in

node_modules/@types/react/index.d.ts:1871

___

### children

• `Optional` **children**: `ReactNode`

#### Inherited from

React.PropsWithChildren.children

#### Defined in

node_modules/@types/react/index.d.ts:1359

node_modules/@types/react/index.d.ts:829

___

### className

• `Optional` **className**: `string`

#### Inherited from

React.PropsWithChildren.className

#### Defined in

node_modules/@types/react/index.d.ts:1836

___

### color

• `Optional` **color**: `string`

#### Inherited from

React.PropsWithChildren.color

#### Defined in

node_modules/@types/react/index.d.ts:1872

___

### contentEditable

• `Optional` **contentEditable**: ``"inherit"`` \| `Booleanish`

#### Inherited from

React.PropsWithChildren.contentEditable

#### Defined in

node_modules/@types/react/index.d.ts:1837

___

### contextMenu

• `Optional` **contextMenu**: `string`

#### Inherited from

React.PropsWithChildren.contextMenu

#### Defined in

node_modules/@types/react/index.d.ts:1838

___

### dangerouslySetInnerHTML

• `Optional` **dangerouslySetInnerHTML**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `__html` | `string` |

#### Inherited from

React.PropsWithChildren.dangerouslySetInnerHTML

#### Defined in

node_modules/@types/react/index.d.ts:1360

___

### datatype

• `Optional` **datatype**: `string`

#### Inherited from

React.PropsWithChildren.datatype

#### Defined in

node_modules/@types/react/index.d.ts:1860

___

### defaultChecked

• `Optional` **defaultChecked**: `boolean`

#### Inherited from

React.PropsWithChildren.defaultChecked

#### Defined in

node_modules/@types/react/index.d.ts:1829

___

### defaultValue

• `Optional` **defaultValue**: `string` \| `number` \| readonly `string`[]

#### Inherited from

React.PropsWithChildren.defaultValue

#### Defined in

node_modules/@types/react/index.d.ts:1830

___

### dir

• `Optional` **dir**: `string`

#### Inherited from

React.PropsWithChildren.dir

#### Defined in

node_modules/@types/react/index.d.ts:1839

___

### draggable

• `Optional` **draggable**: `Booleanish`

#### Inherited from

React.PropsWithChildren.draggable

#### Defined in

node_modules/@types/react/index.d.ts:1840

___

### hidden

• `Optional` **hidden**: `boolean`

#### Inherited from

React.PropsWithChildren.hidden

#### Defined in

node_modules/@types/react/index.d.ts:1841

___

### id

• `Optional` **id**: `string`

#### Inherited from

React.PropsWithChildren.id

#### Defined in

node_modules/@types/react/index.d.ts:1842

___

### inlist

• `Optional` **inlist**: `any`

#### Inherited from

React.PropsWithChildren.inlist

#### Defined in

node_modules/@types/react/index.d.ts:1861

___

### inputMode

• `Optional` **inputMode**: ``"search"`` \| ``"text"`` \| ``"none"`` \| ``"email"`` \| ``"tel"`` \| ``"url"`` \| ``"numeric"`` \| ``"decimal"``

Hints at the type of data that might be entered by the user while editing the element or its contents

**`See`**

https://html.spec.whatwg.org/multipage/interaction.html#input-modalities:-the-inputmode-attribute

#### Inherited from

React.PropsWithChildren.inputMode

#### Defined in

node_modules/@types/react/index.d.ts:1887

___

### is

• `Optional` **is**: `string`

Specify that a standard HTML element should behave like a defined custom built-in element

**`See`**

https://html.spec.whatwg.org/multipage/custom-elements.html#attr-is

#### Inherited from

React.PropsWithChildren.is

#### Defined in

node_modules/@types/react/index.d.ts:1892

___

### itemID

• `Optional` **itemID**: `string`

#### Inherited from

React.PropsWithChildren.itemID

#### Defined in

node_modules/@types/react/index.d.ts:1876

___

### itemProp

• `Optional` **itemProp**: `string`

#### Inherited from

React.PropsWithChildren.itemProp

#### Defined in

node_modules/@types/react/index.d.ts:1873

___

### itemRef

• `Optional` **itemRef**: `string`

#### Inherited from

React.PropsWithChildren.itemRef

#### Defined in

node_modules/@types/react/index.d.ts:1877

___

### itemScope

• `Optional` **itemScope**: `boolean`

#### Inherited from

React.PropsWithChildren.itemScope

#### Defined in

node_modules/@types/react/index.d.ts:1874

___

### itemType

• `Optional` **itemType**: `string`

#### Inherited from

React.PropsWithChildren.itemType

#### Defined in

node_modules/@types/react/index.d.ts:1875

___

### key

• `Optional` **key**: ``null`` \| `Key`

#### Inherited from

React.PropsWithChildren.key

#### Defined in

node_modules/@types/react/index.d.ts:137

___

### lang

• `Optional` **lang**: `string`

#### Inherited from

React.PropsWithChildren.lang

#### Defined in

node_modules/@types/react/index.d.ts:1843

___

### onAbort

• `Optional` **onAbort**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAbort

#### Defined in

node_modules/@types/react/index.d.ts:1415

___

### onAbortCapture

• `Optional` **onAbortCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAbortCapture

#### Defined in

node_modules/@types/react/index.d.ts:1416

___

### onAnimationEnd

• `Optional` **onAnimationEnd**: `AnimationEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAnimationEnd

#### Defined in

node_modules/@types/react/index.d.ts:1545

___

### onAnimationEndCapture

• `Optional` **onAnimationEndCapture**: `AnimationEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAnimationEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1546

___

### onAnimationIteration

• `Optional` **onAnimationIteration**: `AnimationEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAnimationIteration

#### Defined in

node_modules/@types/react/index.d.ts:1547

___

### onAnimationIterationCapture

• `Optional` **onAnimationIterationCapture**: `AnimationEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAnimationIterationCapture

#### Defined in

node_modules/@types/react/index.d.ts:1548

___

### onAnimationStart

• `Optional` **onAnimationStart**: `AnimationEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAnimationStart

#### Defined in

node_modules/@types/react/index.d.ts:1543

___

### onAnimationStartCapture

• `Optional` **onAnimationStartCapture**: `AnimationEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAnimationStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1544

___

### onAuxClick

• `Optional` **onAuxClick**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAuxClick

#### Defined in

node_modules/@types/react/index.d.ts:1461

___

### onAuxClickCapture

• `Optional` **onAuxClickCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onAuxClickCapture

#### Defined in

node_modules/@types/react/index.d.ts:1462

___

### onBeforeInput

• `Optional` **onBeforeInput**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onBeforeInput

#### Defined in

node_modules/@types/react/index.d.ts:1389

___

### onBeforeInputCapture

• `Optional` **onBeforeInputCapture**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onBeforeInputCapture

#### Defined in

node_modules/@types/react/index.d.ts:1390

___

### onBlur

• `Optional` **onBlur**: `FocusEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onBlur

#### Defined in

node_modules/@types/react/index.d.ts:1383

___

### onBlurCapture

• `Optional` **onBlurCapture**: `FocusEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onBlurCapture

#### Defined in

node_modules/@types/react/index.d.ts:1384

___

### onCanPlay

• `Optional` **onCanPlay**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCanPlay

#### Defined in

node_modules/@types/react/index.d.ts:1417

___

### onCanPlayCapture

• `Optional` **onCanPlayCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCanPlayCapture

#### Defined in

node_modules/@types/react/index.d.ts:1418

___

### onCanPlayThrough

• `Optional` **onCanPlayThrough**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCanPlayThrough

#### Defined in

node_modules/@types/react/index.d.ts:1419

___

### onCanPlayThroughCapture

• `Optional` **onCanPlayThroughCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCanPlayThroughCapture

#### Defined in

node_modules/@types/react/index.d.ts:1420

___

### onChange

• `Optional` **onChange**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onChange

#### Defined in

node_modules/@types/react/index.d.ts:1387

___

### onChangeCapture

• `Optional` **onChangeCapture**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onChangeCapture

#### Defined in

node_modules/@types/react/index.d.ts:1388

___

### onClick

• `Optional` **onClick**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onClick

#### Defined in

node_modules/@types/react/index.d.ts:1463

___

### onClickCapture

• `Optional` **onClickCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onClickCapture

#### Defined in

node_modules/@types/react/index.d.ts:1464

___

### onCompositionEnd

• `Optional` **onCompositionEnd**: `CompositionEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCompositionEnd

#### Defined in

node_modules/@types/react/index.d.ts:1373

___

### onCompositionEndCapture

• `Optional` **onCompositionEndCapture**: `CompositionEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCompositionEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1374

___

### onCompositionStart

• `Optional` **onCompositionStart**: `CompositionEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCompositionStart

#### Defined in

node_modules/@types/react/index.d.ts:1375

___

### onCompositionStartCapture

• `Optional` **onCompositionStartCapture**: `CompositionEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCompositionStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1376

___

### onCompositionUpdate

• `Optional` **onCompositionUpdate**: `CompositionEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCompositionUpdate

#### Defined in

node_modules/@types/react/index.d.ts:1377

___

### onCompositionUpdateCapture

• `Optional` **onCompositionUpdateCapture**: `CompositionEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCompositionUpdateCapture

#### Defined in

node_modules/@types/react/index.d.ts:1378

___

### onContextMenu

• `Optional` **onContextMenu**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onContextMenu

#### Defined in

node_modules/@types/react/index.d.ts:1465

___

### onContextMenuCapture

• `Optional` **onContextMenuCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onContextMenuCapture

#### Defined in

node_modules/@types/react/index.d.ts:1466

___

### onCopy

• `Optional` **onCopy**: `ClipboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCopy

#### Defined in

node_modules/@types/react/index.d.ts:1365

___

### onCopyCapture

• `Optional` **onCopyCapture**: `ClipboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCopyCapture

#### Defined in

node_modules/@types/react/index.d.ts:1366

___

### onCut

• `Optional` **onCut**: `ClipboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCut

#### Defined in

node_modules/@types/react/index.d.ts:1367

___

### onCutCapture

• `Optional` **onCutCapture**: `ClipboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onCutCapture

#### Defined in

node_modules/@types/react/index.d.ts:1368

___

### onDoubleClick

• `Optional` **onDoubleClick**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDoubleClick

#### Defined in

node_modules/@types/react/index.d.ts:1467

___

### onDoubleClickCapture

• `Optional` **onDoubleClickCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDoubleClickCapture

#### Defined in

node_modules/@types/react/index.d.ts:1468

___

### onDrag

• `Optional` **onDrag**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDrag

#### Defined in

node_modules/@types/react/index.d.ts:1469

___

### onDragCapture

• `Optional` **onDragCapture**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragCapture

#### Defined in

node_modules/@types/react/index.d.ts:1470

___

### onDragEnd

• `Optional` **onDragEnd**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragEnd

#### Defined in

node_modules/@types/react/index.d.ts:1471

___

### onDragEndCapture

• `Optional` **onDragEndCapture**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1472

___

### onDragEnter

• `Optional` **onDragEnter**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragEnter

#### Defined in

node_modules/@types/react/index.d.ts:1473

___

### onDragEnterCapture

• `Optional` **onDragEnterCapture**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragEnterCapture

#### Defined in

node_modules/@types/react/index.d.ts:1474

___

### onDragExit

• `Optional` **onDragExit**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragExit

#### Defined in

node_modules/@types/react/index.d.ts:1475

___

### onDragExitCapture

• `Optional` **onDragExitCapture**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragExitCapture

#### Defined in

node_modules/@types/react/index.d.ts:1476

___

### onDragLeave

• `Optional` **onDragLeave**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragLeave

#### Defined in

node_modules/@types/react/index.d.ts:1477

___

### onDragLeaveCapture

• `Optional` **onDragLeaveCapture**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragLeaveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1478

___

### onDragOver

• `Optional` **onDragOver**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragOver

#### Defined in

node_modules/@types/react/index.d.ts:1479

___

### onDragOverCapture

• `Optional` **onDragOverCapture**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragOverCapture

#### Defined in

node_modules/@types/react/index.d.ts:1480

___

### onDragStart

• `Optional` **onDragStart**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragStart

#### Defined in

node_modules/@types/react/index.d.ts:1481

___

### onDragStartCapture

• `Optional` **onDragStartCapture**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDragStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1482

___

### onDrop

• `Optional` **onDrop**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDrop

#### Defined in

node_modules/@types/react/index.d.ts:1483

___

### onDropCapture

• `Optional` **onDropCapture**: `DragEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDropCapture

#### Defined in

node_modules/@types/react/index.d.ts:1484

___

### onDurationChange

• `Optional` **onDurationChange**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDurationChange

#### Defined in

node_modules/@types/react/index.d.ts:1421

___

### onDurationChangeCapture

• `Optional` **onDurationChangeCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onDurationChangeCapture

#### Defined in

node_modules/@types/react/index.d.ts:1422

___

### onEmptied

• `Optional` **onEmptied**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onEmptied

#### Defined in

node_modules/@types/react/index.d.ts:1423

___

### onEmptiedCapture

• `Optional` **onEmptiedCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onEmptiedCapture

#### Defined in

node_modules/@types/react/index.d.ts:1424

___

### onEncrypted

• `Optional` **onEncrypted**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onEncrypted

#### Defined in

node_modules/@types/react/index.d.ts:1425

___

### onEncryptedCapture

• `Optional` **onEncryptedCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onEncryptedCapture

#### Defined in

node_modules/@types/react/index.d.ts:1426

___

### onEnded

• `Optional` **onEnded**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onEnded

#### Defined in

node_modules/@types/react/index.d.ts:1427

___

### onEndedCapture

• `Optional` **onEndedCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onEndedCapture

#### Defined in

node_modules/@types/react/index.d.ts:1428

___

### onError

• `Optional` **onError**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onError

#### Defined in

node_modules/@types/react/index.d.ts:1403

___

### onErrorCapture

• `Optional` **onErrorCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onErrorCapture

#### Defined in

node_modules/@types/react/index.d.ts:1404

___

### onFocus

• `Optional` **onFocus**: `FocusEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onFocus

#### Defined in

node_modules/@types/react/index.d.ts:1381

___

### onFocusCapture

• `Optional` **onFocusCapture**: `FocusEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onFocusCapture

#### Defined in

node_modules/@types/react/index.d.ts:1382

___

### onGotPointerCapture

• `Optional` **onGotPointerCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onGotPointerCapture

#### Defined in

node_modules/@types/react/index.d.ts:1529

___

### onGotPointerCaptureCapture

• `Optional` **onGotPointerCaptureCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onGotPointerCaptureCapture

#### Defined in

node_modules/@types/react/index.d.ts:1530

___

### onInput

• `Optional` **onInput**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onInput

#### Defined in

node_modules/@types/react/index.d.ts:1391

___

### onInputCapture

• `Optional` **onInputCapture**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onInputCapture

#### Defined in

node_modules/@types/react/index.d.ts:1392

___

### onInvalid

• `Optional` **onInvalid**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onInvalid

#### Defined in

node_modules/@types/react/index.d.ts:1397

___

### onInvalidCapture

• `Optional` **onInvalidCapture**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onInvalidCapture

#### Defined in

node_modules/@types/react/index.d.ts:1398

___

### onKeyDown

• `Optional` **onKeyDown**: `KeyboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onKeyDown

#### Defined in

node_modules/@types/react/index.d.ts:1407

___

### onKeyDownCapture

• `Optional` **onKeyDownCapture**: `KeyboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onKeyDownCapture

#### Defined in

node_modules/@types/react/index.d.ts:1408

___

### onKeyPress

• `Optional` **onKeyPress**: `KeyboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onKeyPress

#### Defined in

node_modules/@types/react/index.d.ts:1409

___

### onKeyPressCapture

• `Optional` **onKeyPressCapture**: `KeyboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onKeyPressCapture

#### Defined in

node_modules/@types/react/index.d.ts:1410

___

### onKeyUp

• `Optional` **onKeyUp**: `KeyboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onKeyUp

#### Defined in

node_modules/@types/react/index.d.ts:1411

___

### onKeyUpCapture

• `Optional` **onKeyUpCapture**: `KeyboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onKeyUpCapture

#### Defined in

node_modules/@types/react/index.d.ts:1412

___

### onLoad

• `Optional` **onLoad**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLoad

#### Defined in

node_modules/@types/react/index.d.ts:1401

___

### onLoadCapture

• `Optional` **onLoadCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLoadCapture

#### Defined in

node_modules/@types/react/index.d.ts:1402

___

### onLoadStart

• `Optional` **onLoadStart**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLoadStart

#### Defined in

node_modules/@types/react/index.d.ts:1433

___

### onLoadStartCapture

• `Optional` **onLoadStartCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLoadStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1434

___

### onLoadedData

• `Optional` **onLoadedData**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLoadedData

#### Defined in

node_modules/@types/react/index.d.ts:1429

___

### onLoadedDataCapture

• `Optional` **onLoadedDataCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLoadedDataCapture

#### Defined in

node_modules/@types/react/index.d.ts:1430

___

### onLoadedMetadata

• `Optional` **onLoadedMetadata**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLoadedMetadata

#### Defined in

node_modules/@types/react/index.d.ts:1431

___

### onLoadedMetadataCapture

• `Optional` **onLoadedMetadataCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLoadedMetadataCapture

#### Defined in

node_modules/@types/react/index.d.ts:1432

___

### onLostPointerCapture

• `Optional` **onLostPointerCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLostPointerCapture

#### Defined in

node_modules/@types/react/index.d.ts:1531

___

### onLostPointerCaptureCapture

• `Optional` **onLostPointerCaptureCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onLostPointerCaptureCapture

#### Defined in

node_modules/@types/react/index.d.ts:1532

___

### onMouseDown

• `Optional` **onMouseDown**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseDown

#### Defined in

node_modules/@types/react/index.d.ts:1485

___

### onMouseDownCapture

• `Optional` **onMouseDownCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseDownCapture

#### Defined in

node_modules/@types/react/index.d.ts:1486

___

### onMouseEnter

• `Optional` **onMouseEnter**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseEnter

#### Defined in

node_modules/@types/react/index.d.ts:1487

___

### onMouseLeave

• `Optional` **onMouseLeave**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseLeave

#### Defined in

node_modules/@types/react/index.d.ts:1488

___

### onMouseMove

• `Optional` **onMouseMove**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseMove

#### Defined in

node_modules/@types/react/index.d.ts:1489

___

### onMouseMoveCapture

• `Optional` **onMouseMoveCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseMoveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1490

___

### onMouseOut

• `Optional` **onMouseOut**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseOut

#### Defined in

node_modules/@types/react/index.d.ts:1491

___

### onMouseOutCapture

• `Optional` **onMouseOutCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseOutCapture

#### Defined in

node_modules/@types/react/index.d.ts:1492

___

### onMouseOver

• `Optional` **onMouseOver**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseOver

#### Defined in

node_modules/@types/react/index.d.ts:1493

___

### onMouseOverCapture

• `Optional` **onMouseOverCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseOverCapture

#### Defined in

node_modules/@types/react/index.d.ts:1494

___

### onMouseUp

• `Optional` **onMouseUp**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseUp

#### Defined in

node_modules/@types/react/index.d.ts:1495

___

### onMouseUpCapture

• `Optional` **onMouseUpCapture**: `MouseEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onMouseUpCapture

#### Defined in

node_modules/@types/react/index.d.ts:1496

___

### onPaste

• `Optional` **onPaste**: `ClipboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPaste

#### Defined in

node_modules/@types/react/index.d.ts:1369

___

### onPasteCapture

• `Optional` **onPasteCapture**: `ClipboardEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPasteCapture

#### Defined in

node_modules/@types/react/index.d.ts:1370

___

### onPause

• `Optional` **onPause**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPause

#### Defined in

node_modules/@types/react/index.d.ts:1435

___

### onPauseCapture

• `Optional` **onPauseCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPauseCapture

#### Defined in

node_modules/@types/react/index.d.ts:1436

___

### onPlay

• `Optional` **onPlay**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPlay

#### Defined in

node_modules/@types/react/index.d.ts:1437

___

### onPlayCapture

• `Optional` **onPlayCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPlayCapture

#### Defined in

node_modules/@types/react/index.d.ts:1438

___

### onPlaying

• `Optional` **onPlaying**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPlaying

#### Defined in

node_modules/@types/react/index.d.ts:1439

___

### onPlayingCapture

• `Optional` **onPlayingCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPlayingCapture

#### Defined in

node_modules/@types/react/index.d.ts:1440

___

### onPointerCancel

• `Optional` **onPointerCancel**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerCancel

#### Defined in

node_modules/@types/react/index.d.ts:1519

___

### onPointerCancelCapture

• `Optional` **onPointerCancelCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerCancelCapture

#### Defined in

node_modules/@types/react/index.d.ts:1520

___

### onPointerDown

• `Optional` **onPointerDown**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerDown

#### Defined in

node_modules/@types/react/index.d.ts:1513

___

### onPointerDownCapture

• `Optional` **onPointerDownCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerDownCapture

#### Defined in

node_modules/@types/react/index.d.ts:1514

___

### onPointerEnter

• `Optional` **onPointerEnter**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerEnter

#### Defined in

node_modules/@types/react/index.d.ts:1521

___

### onPointerEnterCapture

• `Optional` **onPointerEnterCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerEnterCapture

#### Defined in

node_modules/@types/react/index.d.ts:1522

___

### onPointerLeave

• `Optional` **onPointerLeave**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerLeave

#### Defined in

node_modules/@types/react/index.d.ts:1523

___

### onPointerLeaveCapture

• `Optional` **onPointerLeaveCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerLeaveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1524

___

### onPointerMove

• `Optional` **onPointerMove**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerMove

#### Defined in

node_modules/@types/react/index.d.ts:1515

___

### onPointerMoveCapture

• `Optional` **onPointerMoveCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerMoveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1516

___

### onPointerOut

• `Optional` **onPointerOut**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerOut

#### Defined in

node_modules/@types/react/index.d.ts:1527

___

### onPointerOutCapture

• `Optional` **onPointerOutCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerOutCapture

#### Defined in

node_modules/@types/react/index.d.ts:1528

___

### onPointerOver

• `Optional` **onPointerOver**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerOver

#### Defined in

node_modules/@types/react/index.d.ts:1525

___

### onPointerOverCapture

• `Optional` **onPointerOverCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerOverCapture

#### Defined in

node_modules/@types/react/index.d.ts:1526

___

### onPointerUp

• `Optional` **onPointerUp**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerUp

#### Defined in

node_modules/@types/react/index.d.ts:1517

___

### onPointerUpCapture

• `Optional` **onPointerUpCapture**: `PointerEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onPointerUpCapture

#### Defined in

node_modules/@types/react/index.d.ts:1518

___

### onProgress

• `Optional` **onProgress**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onProgress

#### Defined in

node_modules/@types/react/index.d.ts:1441

___

### onProgressCapture

• `Optional` **onProgressCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onProgressCapture

#### Defined in

node_modules/@types/react/index.d.ts:1442

___

### onRateChange

• `Optional` **onRateChange**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onRateChange

#### Defined in

node_modules/@types/react/index.d.ts:1443

___

### onRateChangeCapture

• `Optional` **onRateChangeCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onRateChangeCapture

#### Defined in

node_modules/@types/react/index.d.ts:1444

___

### onReset

• `Optional` **onReset**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onReset

#### Defined in

node_modules/@types/react/index.d.ts:1393

___

### onResetCapture

• `Optional` **onResetCapture**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onResetCapture

#### Defined in

node_modules/@types/react/index.d.ts:1394

___

### onScroll

• `Optional` **onScroll**: `UIEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onScroll

#### Defined in

node_modules/@types/react/index.d.ts:1535

___

### onScrollCapture

• `Optional` **onScrollCapture**: `UIEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onScrollCapture

#### Defined in

node_modules/@types/react/index.d.ts:1536

___

### onSeeked

• `Optional` **onSeeked**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSeeked

#### Defined in

node_modules/@types/react/index.d.ts:1445

___

### onSeekedCapture

• `Optional` **onSeekedCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSeekedCapture

#### Defined in

node_modules/@types/react/index.d.ts:1446

___

### onSeeking

• `Optional` **onSeeking**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSeeking

#### Defined in

node_modules/@types/react/index.d.ts:1447

___

### onSeekingCapture

• `Optional` **onSeekingCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSeekingCapture

#### Defined in

node_modules/@types/react/index.d.ts:1448

___

### onSelect

• `Optional` **onSelect**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSelect

#### Defined in

node_modules/@types/react/index.d.ts:1499

___

### onSelectCapture

• `Optional` **onSelectCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSelectCapture

#### Defined in

node_modules/@types/react/index.d.ts:1500

___

### onStalled

• `Optional` **onStalled**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onStalled

#### Defined in

node_modules/@types/react/index.d.ts:1449

___

### onStalledCapture

• `Optional` **onStalledCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onStalledCapture

#### Defined in

node_modules/@types/react/index.d.ts:1450

___

### onSubmit

• `Optional` **onSubmit**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSubmit

#### Defined in

node_modules/@types/react/index.d.ts:1395

___

### onSubmitCapture

• `Optional` **onSubmitCapture**: `FormEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSubmitCapture

#### Defined in

node_modules/@types/react/index.d.ts:1396

___

### onSuspend

• `Optional` **onSuspend**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSuspend

#### Defined in

node_modules/@types/react/index.d.ts:1451

___

### onSuspendCapture

• `Optional` **onSuspendCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onSuspendCapture

#### Defined in

node_modules/@types/react/index.d.ts:1452

___

### onTimeUpdate

• `Optional` **onTimeUpdate**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTimeUpdate

#### Defined in

node_modules/@types/react/index.d.ts:1453

___

### onTimeUpdateCapture

• `Optional` **onTimeUpdateCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTimeUpdateCapture

#### Defined in

node_modules/@types/react/index.d.ts:1454

___

### onTouchCancel

• `Optional` **onTouchCancel**: `TouchEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTouchCancel

#### Defined in

node_modules/@types/react/index.d.ts:1503

___

### onTouchCancelCapture

• `Optional` **onTouchCancelCapture**: `TouchEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTouchCancelCapture

#### Defined in

node_modules/@types/react/index.d.ts:1504

___

### onTouchEnd

• `Optional` **onTouchEnd**: `TouchEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTouchEnd

#### Defined in

node_modules/@types/react/index.d.ts:1505

___

### onTouchEndCapture

• `Optional` **onTouchEndCapture**: `TouchEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTouchEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1506

___

### onTouchMove

• `Optional` **onTouchMove**: `TouchEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTouchMove

#### Defined in

node_modules/@types/react/index.d.ts:1507

___

### onTouchMoveCapture

• `Optional` **onTouchMoveCapture**: `TouchEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTouchMoveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1508

___

### onTouchStart

• `Optional` **onTouchStart**: `TouchEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTouchStart

#### Defined in

node_modules/@types/react/index.d.ts:1509

___

### onTouchStartCapture

• `Optional` **onTouchStartCapture**: `TouchEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTouchStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1510

___

### onTransitionEnd

• `Optional` **onTransitionEnd**: `TransitionEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTransitionEnd

#### Defined in

node_modules/@types/react/index.d.ts:1551

___

### onTransitionEndCapture

• `Optional` **onTransitionEndCapture**: `TransitionEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onTransitionEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1552

___

### onVolumeChange

• `Optional` **onVolumeChange**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onVolumeChange

#### Defined in

node_modules/@types/react/index.d.ts:1455

___

### onVolumeChangeCapture

• `Optional` **onVolumeChangeCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onVolumeChangeCapture

#### Defined in

node_modules/@types/react/index.d.ts:1456

___

### onWaiting

• `Optional` **onWaiting**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onWaiting

#### Defined in

node_modules/@types/react/index.d.ts:1457

___

### onWaitingCapture

• `Optional` **onWaitingCapture**: `ReactEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onWaitingCapture

#### Defined in

node_modules/@types/react/index.d.ts:1458

___

### onWheel

• `Optional` **onWheel**: `WheelEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onWheel

#### Defined in

node_modules/@types/react/index.d.ts:1539

___

### onWheelCapture

• `Optional` **onWheelCapture**: `WheelEventHandler`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.onWheelCapture

#### Defined in

node_modules/@types/react/index.d.ts:1540

___

### placeholder

• `Optional` **placeholder**: `string`

#### Inherited from

React.PropsWithChildren.placeholder

#### Defined in

node_modules/@types/react/index.d.ts:1844

___

### prefix

• `Optional` **prefix**: `string`

#### Inherited from

React.PropsWithChildren.prefix

#### Defined in

node_modules/@types/react/index.d.ts:1862

___

### property

• `Optional` **property**: `string`

#### Inherited from

React.PropsWithChildren.property

#### Defined in

node_modules/@types/react/index.d.ts:1863

___

### radioGroup

• `Optional` **radioGroup**: `string`

#### Inherited from

React.PropsWithChildren.radioGroup

#### Defined in

node_modules/@types/react/index.d.ts:1853

___

### ref

• `Optional` **ref**: `LegacyRef`<`HTMLDivElement`\>

#### Inherited from

React.PropsWithChildren.ref

#### Defined in

node_modules/@types/react/index.d.ts:143

___

### resource

• `Optional` **resource**: `string`

#### Inherited from

React.PropsWithChildren.resource

#### Defined in

node_modules/@types/react/index.d.ts:1864

___

### results

• `Optional` **results**: `number`

#### Inherited from

React.PropsWithChildren.results

#### Defined in

node_modules/@types/react/index.d.ts:1878

___

### role

• `Optional` **role**: `AriaRole`

#### Inherited from

React.PropsWithChildren.role

#### Defined in

node_modules/@types/react/index.d.ts:1856

___

### security

• `Optional` **security**: `string`

#### Inherited from

React.PropsWithChildren.security

#### Defined in

node_modules/@types/react/index.d.ts:1879

___

### slot

• `Optional` **slot**: `string`

#### Inherited from

React.PropsWithChildren.slot

#### Defined in

node_modules/@types/react/index.d.ts:1845

___

### spellCheck

• `Optional` **spellCheck**: `Booleanish`

#### Inherited from

React.PropsWithChildren.spellCheck

#### Defined in

node_modules/@types/react/index.d.ts:1846

___

### style

• `Optional` **style**: `CSSProperties`

#### Inherited from

React.PropsWithChildren.style

#### Defined in

node_modules/@types/react/index.d.ts:1847

___

### suppressContentEditableWarning

• `Optional` **suppressContentEditableWarning**: `boolean`

#### Inherited from

React.PropsWithChildren.suppressContentEditableWarning

#### Defined in

node_modules/@types/react/index.d.ts:1831

___

### suppressHydrationWarning

• `Optional` **suppressHydrationWarning**: `boolean`

#### Inherited from

React.PropsWithChildren.suppressHydrationWarning

#### Defined in

node_modules/@types/react/index.d.ts:1832

___

### tabIndex

• `Optional` **tabIndex**: `number`

#### Inherited from

React.PropsWithChildren.tabIndex

#### Defined in

node_modules/@types/react/index.d.ts:1848

___

### title

• `Optional` **title**: `string`

#### Inherited from

React.PropsWithChildren.title

#### Defined in

node_modules/@types/react/index.d.ts:1849

___

### translate

• `Optional` **translate**: ``"no"`` \| ``"yes"``

#### Inherited from

React.PropsWithChildren.translate

#### Defined in

node_modules/@types/react/index.d.ts:1850

___

### typeof

• `Optional` **typeof**: `string`

#### Inherited from

React.PropsWithChildren.typeof

#### Defined in

node_modules/@types/react/index.d.ts:1865

___

### unselectable

• `Optional` **unselectable**: ``"off"`` \| ``"on"``

#### Inherited from

React.PropsWithChildren.unselectable

#### Defined in

node_modules/@types/react/index.d.ts:1880

___

### vocab

• `Optional` **vocab**: `string`

#### Inherited from

React.PropsWithChildren.vocab

#### Defined in

node_modules/@types/react/index.d.ts:1866

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Markers Types](../modules/Globe_Markers_Types.md) / MarkerForwardedData

# Interface: MarkerForwardedData

[Globe Markers Types](../modules/Globe_Markers_Types.md).MarkerForwardedData

data will be forwarded to `g` svg element of Marker component

**`Globe Marker Data`**

forwarded data

## Table of contents

### Properties

- [globeMarkerData](Globe_Markers_Types.MarkerForwardedData.md#globemarkerdata)

## Properties

### globeMarkerData

• **globeMarkerData**: [`MarkerData`](Globe_Markers_Types.MarkerData.md)

#### Defined in

[src/components/globe/markers/types.ts:77](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/markers/types.ts#L77)

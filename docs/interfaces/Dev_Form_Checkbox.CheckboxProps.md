[React Simple Globe](../README.md) / [Exports](../modules.md) / [Dev Form Checkbox](../modules/Dev_Form_Checkbox.md) / CheckboxProps

# Interface: CheckboxProps

[Dev Form Checkbox](../modules/Dev_Form_Checkbox.md).CheckboxProps

## Table of contents

### Properties

- [label](Dev_Form_Checkbox.CheckboxProps.md#label)
- [onChange](Dev_Form_Checkbox.CheckboxProps.md#onchange)
- [value](Dev_Form_Checkbox.CheckboxProps.md#value)

## Properties

### label

• `Optional` **label**: `string`

#### Defined in

[src/components/form/Checkbox.tsx:11](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Checkbox.tsx#L11)

___

### onChange

• **onChange**: (`v`: `boolean`) => `void`

#### Type declaration

▸ (`v`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `v` | `boolean` |

##### Returns

`void`

#### Defined in

[src/components/form/Checkbox.tsx:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Checkbox.tsx#L12)

___

### value

• **value**: `boolean`

#### Defined in

[src/components/form/Checkbox.tsx:10](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Checkbox.tsx#L10)

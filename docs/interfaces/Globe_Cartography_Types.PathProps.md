[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Types](../modules/Globe_Cartography_Types.md) / PathProps

# Interface: PathProps

[Globe Cartography Types](../modules/Globe_Cartography_Types.md).PathProps

## Hierarchy

- `Partial`<[`HandleEventProps`](Globe_Events_Types.HandleEventProps.md)\>

- `SVGProps`<`SVGPathElement`\>

  ↳ **`PathProps`**

## Table of contents

### Properties

- [accentHeight](Globe_Cartography_Types.PathProps.md#accentheight)
- [accumulate](Globe_Cartography_Types.PathProps.md#accumulate)
- [additive](Globe_Cartography_Types.PathProps.md#additive)
- [alignmentBaseline](Globe_Cartography_Types.PathProps.md#alignmentbaseline)
- [allowReorder](Globe_Cartography_Types.PathProps.md#allowreorder)
- [alphabetic](Globe_Cartography_Types.PathProps.md#alphabetic)
- [amplitude](Globe_Cartography_Types.PathProps.md#amplitude)
- [arabicForm](Globe_Cartography_Types.PathProps.md#arabicform)
- [aria-activedescendant](Globe_Cartography_Types.PathProps.md#aria-activedescendant)
- [aria-atomic](Globe_Cartography_Types.PathProps.md#aria-atomic)
- [aria-autocomplete](Globe_Cartography_Types.PathProps.md#aria-autocomplete)
- [aria-busy](Globe_Cartography_Types.PathProps.md#aria-busy)
- [aria-checked](Globe_Cartography_Types.PathProps.md#aria-checked)
- [aria-colcount](Globe_Cartography_Types.PathProps.md#aria-colcount)
- [aria-colindex](Globe_Cartography_Types.PathProps.md#aria-colindex)
- [aria-colspan](Globe_Cartography_Types.PathProps.md#aria-colspan)
- [aria-controls](Globe_Cartography_Types.PathProps.md#aria-controls)
- [aria-current](Globe_Cartography_Types.PathProps.md#aria-current)
- [aria-describedby](Globe_Cartography_Types.PathProps.md#aria-describedby)
- [aria-details](Globe_Cartography_Types.PathProps.md#aria-details)
- [aria-disabled](Globe_Cartography_Types.PathProps.md#aria-disabled)
- [aria-dropeffect](Globe_Cartography_Types.PathProps.md#aria-dropeffect)
- [aria-errormessage](Globe_Cartography_Types.PathProps.md#aria-errormessage)
- [aria-expanded](Globe_Cartography_Types.PathProps.md#aria-expanded)
- [aria-flowto](Globe_Cartography_Types.PathProps.md#aria-flowto)
- [aria-grabbed](Globe_Cartography_Types.PathProps.md#aria-grabbed)
- [aria-haspopup](Globe_Cartography_Types.PathProps.md#aria-haspopup)
- [aria-hidden](Globe_Cartography_Types.PathProps.md#aria-hidden)
- [aria-invalid](Globe_Cartography_Types.PathProps.md#aria-invalid)
- [aria-keyshortcuts](Globe_Cartography_Types.PathProps.md#aria-keyshortcuts)
- [aria-label](Globe_Cartography_Types.PathProps.md#aria-label)
- [aria-labelledby](Globe_Cartography_Types.PathProps.md#aria-labelledby)
- [aria-level](Globe_Cartography_Types.PathProps.md#aria-level)
- [aria-live](Globe_Cartography_Types.PathProps.md#aria-live)
- [aria-modal](Globe_Cartography_Types.PathProps.md#aria-modal)
- [aria-multiline](Globe_Cartography_Types.PathProps.md#aria-multiline)
- [aria-multiselectable](Globe_Cartography_Types.PathProps.md#aria-multiselectable)
- [aria-orientation](Globe_Cartography_Types.PathProps.md#aria-orientation)
- [aria-owns](Globe_Cartography_Types.PathProps.md#aria-owns)
- [aria-placeholder](Globe_Cartography_Types.PathProps.md#aria-placeholder)
- [aria-posinset](Globe_Cartography_Types.PathProps.md#aria-posinset)
- [aria-pressed](Globe_Cartography_Types.PathProps.md#aria-pressed)
- [aria-readonly](Globe_Cartography_Types.PathProps.md#aria-readonly)
- [aria-relevant](Globe_Cartography_Types.PathProps.md#aria-relevant)
- [aria-required](Globe_Cartography_Types.PathProps.md#aria-required)
- [aria-roledescription](Globe_Cartography_Types.PathProps.md#aria-roledescription)
- [aria-rowcount](Globe_Cartography_Types.PathProps.md#aria-rowcount)
- [aria-rowindex](Globe_Cartography_Types.PathProps.md#aria-rowindex)
- [aria-rowspan](Globe_Cartography_Types.PathProps.md#aria-rowspan)
- [aria-selected](Globe_Cartography_Types.PathProps.md#aria-selected)
- [aria-setsize](Globe_Cartography_Types.PathProps.md#aria-setsize)
- [aria-sort](Globe_Cartography_Types.PathProps.md#aria-sort)
- [aria-valuemax](Globe_Cartography_Types.PathProps.md#aria-valuemax)
- [aria-valuemin](Globe_Cartography_Types.PathProps.md#aria-valuemin)
- [aria-valuenow](Globe_Cartography_Types.PathProps.md#aria-valuenow)
- [aria-valuetext](Globe_Cartography_Types.PathProps.md#aria-valuetext)
- [ascent](Globe_Cartography_Types.PathProps.md#ascent)
- [attributeName](Globe_Cartography_Types.PathProps.md#attributename)
- [attributeType](Globe_Cartography_Types.PathProps.md#attributetype)
- [autoReverse](Globe_Cartography_Types.PathProps.md#autoreverse)
- [azimuth](Globe_Cartography_Types.PathProps.md#azimuth)
- [baseFrequency](Globe_Cartography_Types.PathProps.md#basefrequency)
- [baseProfile](Globe_Cartography_Types.PathProps.md#baseprofile)
- [baselineShift](Globe_Cartography_Types.PathProps.md#baselineshift)
- [bbox](Globe_Cartography_Types.PathProps.md#bbox)
- [begin](Globe_Cartography_Types.PathProps.md#begin)
- [bias](Globe_Cartography_Types.PathProps.md#bias)
- [by](Globe_Cartography_Types.PathProps.md#by)
- [calcMode](Globe_Cartography_Types.PathProps.md#calcmode)
- [capHeight](Globe_Cartography_Types.PathProps.md#capheight)
- [children](Globe_Cartography_Types.PathProps.md#children)
- [className](Globe_Cartography_Types.PathProps.md#classname)
- [clip](Globe_Cartography_Types.PathProps.md#clip)
- [clipPath](Globe_Cartography_Types.PathProps.md#clippath)
- [clipPathUnits](Globe_Cartography_Types.PathProps.md#clippathunits)
- [clipRule](Globe_Cartography_Types.PathProps.md#cliprule)
- [color](Globe_Cartography_Types.PathProps.md#color)
- [colorInterpolation](Globe_Cartography_Types.PathProps.md#colorinterpolation)
- [colorInterpolationFilters](Globe_Cartography_Types.PathProps.md#colorinterpolationfilters)
- [colorProfile](Globe_Cartography_Types.PathProps.md#colorprofile)
- [colorRendering](Globe_Cartography_Types.PathProps.md#colorrendering)
- [contentScriptType](Globe_Cartography_Types.PathProps.md#contentscripttype)
- [contentStyleType](Globe_Cartography_Types.PathProps.md#contentstyletype)
- [crossOrigin](Globe_Cartography_Types.PathProps.md#crossorigin)
- [cursor](Globe_Cartography_Types.PathProps.md#cursor)
- [cx](Globe_Cartography_Types.PathProps.md#cx)
- [cy](Globe_Cartography_Types.PathProps.md#cy)
- [d](Globe_Cartography_Types.PathProps.md#d)
- [dangerouslySetInnerHTML](Globe_Cartography_Types.PathProps.md#dangerouslysetinnerhtml)
- [decelerate](Globe_Cartography_Types.PathProps.md#decelerate)
- [descent](Globe_Cartography_Types.PathProps.md#descent)
- [diffuseConstant](Globe_Cartography_Types.PathProps.md#diffuseconstant)
- [direction](Globe_Cartography_Types.PathProps.md#direction)
- [display](Globe_Cartography_Types.PathProps.md#display)
- [divisor](Globe_Cartography_Types.PathProps.md#divisor)
- [dominantBaseline](Globe_Cartography_Types.PathProps.md#dominantbaseline)
- [dur](Globe_Cartography_Types.PathProps.md#dur)
- [dx](Globe_Cartography_Types.PathProps.md#dx)
- [dy](Globe_Cartography_Types.PathProps.md#dy)
- [edgeMode](Globe_Cartography_Types.PathProps.md#edgemode)
- [elevation](Globe_Cartography_Types.PathProps.md#elevation)
- [enableBackground](Globe_Cartography_Types.PathProps.md#enablebackground)
- [end](Globe_Cartography_Types.PathProps.md#end)
- [exponent](Globe_Cartography_Types.PathProps.md#exponent)
- [externalResourcesRequired](Globe_Cartography_Types.PathProps.md#externalresourcesrequired)
- [fill](Globe_Cartography_Types.PathProps.md#fill)
- [fillOpacity](Globe_Cartography_Types.PathProps.md#fillopacity)
- [fillRule](Globe_Cartography_Types.PathProps.md#fillrule)
- [filter](Globe_Cartography_Types.PathProps.md#filter)
- [filterRes](Globe_Cartography_Types.PathProps.md#filterres)
- [filterUnits](Globe_Cartography_Types.PathProps.md#filterunits)
- [floodColor](Globe_Cartography_Types.PathProps.md#floodcolor)
- [floodOpacity](Globe_Cartography_Types.PathProps.md#floodopacity)
- [focusable](Globe_Cartography_Types.PathProps.md#focusable)
- [fontFamily](Globe_Cartography_Types.PathProps.md#fontfamily)
- [fontSize](Globe_Cartography_Types.PathProps.md#fontsize)
- [fontSizeAdjust](Globe_Cartography_Types.PathProps.md#fontsizeadjust)
- [fontStretch](Globe_Cartography_Types.PathProps.md#fontstretch)
- [fontStyle](Globe_Cartography_Types.PathProps.md#fontstyle)
- [fontVariant](Globe_Cartography_Types.PathProps.md#fontvariant)
- [fontWeight](Globe_Cartography_Types.PathProps.md#fontweight)
- [format](Globe_Cartography_Types.PathProps.md#format)
- [fr](Globe_Cartography_Types.PathProps.md#fr)
- [from](Globe_Cartography_Types.PathProps.md#from)
- [fx](Globe_Cartography_Types.PathProps.md#fx)
- [fy](Globe_Cartography_Types.PathProps.md#fy)
- [g1](Globe_Cartography_Types.PathProps.md#g1)
- [g2](Globe_Cartography_Types.PathProps.md#g2)
- [geo](Globe_Cartography_Types.PathProps.md#geo)
- [glyphName](Globe_Cartography_Types.PathProps.md#glyphname)
- [glyphOrientationHorizontal](Globe_Cartography_Types.PathProps.md#glyphorientationhorizontal)
- [glyphOrientationVertical](Globe_Cartography_Types.PathProps.md#glyphorientationvertical)
- [glyphRef](Globe_Cartography_Types.PathProps.md#glyphref)
- [gradientTransform](Globe_Cartography_Types.PathProps.md#gradienttransform)
- [gradientUnits](Globe_Cartography_Types.PathProps.md#gradientunits)
- [hanging](Globe_Cartography_Types.PathProps.md#hanging)
- [height](Globe_Cartography_Types.PathProps.md#height)
- [horizAdvX](Globe_Cartography_Types.PathProps.md#horizadvx)
- [horizOriginX](Globe_Cartography_Types.PathProps.md#horizoriginx)
- [hover](Globe_Cartography_Types.PathProps.md#hover)
- [href](Globe_Cartography_Types.PathProps.md#href)
- [id](Globe_Cartography_Types.PathProps.md#id)
- [ideographic](Globe_Cartography_Types.PathProps.md#ideographic)
- [imageRendering](Globe_Cartography_Types.PathProps.md#imagerendering)
- [in](Globe_Cartography_Types.PathProps.md#in)
- [in2](Globe_Cartography_Types.PathProps.md#in2)
- [intercept](Globe_Cartography_Types.PathProps.md#intercept)
- [k](Globe_Cartography_Types.PathProps.md#k)
- [k1](Globe_Cartography_Types.PathProps.md#k1)
- [k2](Globe_Cartography_Types.PathProps.md#k2)
- [k3](Globe_Cartography_Types.PathProps.md#k3)
- [k4](Globe_Cartography_Types.PathProps.md#k4)
- [kernelMatrix](Globe_Cartography_Types.PathProps.md#kernelmatrix)
- [kernelUnitLength](Globe_Cartography_Types.PathProps.md#kernelunitlength)
- [kerning](Globe_Cartography_Types.PathProps.md#kerning)
- [key](Globe_Cartography_Types.PathProps.md#key)
- [keyPoints](Globe_Cartography_Types.PathProps.md#keypoints)
- [keySplines](Globe_Cartography_Types.PathProps.md#keysplines)
- [keyTimes](Globe_Cartography_Types.PathProps.md#keytimes)
- [lang](Globe_Cartography_Types.PathProps.md#lang)
- [lengthAdjust](Globe_Cartography_Types.PathProps.md#lengthadjust)
- [letterSpacing](Globe_Cartography_Types.PathProps.md#letterspacing)
- [lightingColor](Globe_Cartography_Types.PathProps.md#lightingcolor)
- [limitingConeAngle](Globe_Cartography_Types.PathProps.md#limitingconeangle)
- [local](Globe_Cartography_Types.PathProps.md#local)
- [markerEnd](Globe_Cartography_Types.PathProps.md#markerend)
- [markerHeight](Globe_Cartography_Types.PathProps.md#markerheight)
- [markerMid](Globe_Cartography_Types.PathProps.md#markermid)
- [markerStart](Globe_Cartography_Types.PathProps.md#markerstart)
- [markerUnits](Globe_Cartography_Types.PathProps.md#markerunits)
- [markerWidth](Globe_Cartography_Types.PathProps.md#markerwidth)
- [mask](Globe_Cartography_Types.PathProps.md#mask)
- [maskContentUnits](Globe_Cartography_Types.PathProps.md#maskcontentunits)
- [maskUnits](Globe_Cartography_Types.PathProps.md#maskunits)
- [mathematical](Globe_Cartography_Types.PathProps.md#mathematical)
- [max](Globe_Cartography_Types.PathProps.md#max)
- [media](Globe_Cartography_Types.PathProps.md#media)
- [method](Globe_Cartography_Types.PathProps.md#method)
- [min](Globe_Cartography_Types.PathProps.md#min)
- [mode](Globe_Cartography_Types.PathProps.md#mode)
- [name](Globe_Cartography_Types.PathProps.md#name)
- [numOctaves](Globe_Cartography_Types.PathProps.md#numoctaves)
- [offset](Globe_Cartography_Types.PathProps.md#offset)
- [onAbort](Globe_Cartography_Types.PathProps.md#onabort)
- [onAbortCapture](Globe_Cartography_Types.PathProps.md#onabortcapture)
- [onAnimationEnd](Globe_Cartography_Types.PathProps.md#onanimationend)
- [onAnimationEndCapture](Globe_Cartography_Types.PathProps.md#onanimationendcapture)
- [onAnimationIteration](Globe_Cartography_Types.PathProps.md#onanimationiteration)
- [onAnimationIterationCapture](Globe_Cartography_Types.PathProps.md#onanimationiterationcapture)
- [onAnimationStart](Globe_Cartography_Types.PathProps.md#onanimationstart)
- [onAnimationStartCapture](Globe_Cartography_Types.PathProps.md#onanimationstartcapture)
- [onAuxClick](Globe_Cartography_Types.PathProps.md#onauxclick)
- [onAuxClickCapture](Globe_Cartography_Types.PathProps.md#onauxclickcapture)
- [onBeforeInput](Globe_Cartography_Types.PathProps.md#onbeforeinput)
- [onBeforeInputCapture](Globe_Cartography_Types.PathProps.md#onbeforeinputcapture)
- [onBlur](Globe_Cartography_Types.PathProps.md#onblur)
- [onBlurCapture](Globe_Cartography_Types.PathProps.md#onblurcapture)
- [onCanPlay](Globe_Cartography_Types.PathProps.md#oncanplay)
- [onCanPlayCapture](Globe_Cartography_Types.PathProps.md#oncanplaycapture)
- [onCanPlayThrough](Globe_Cartography_Types.PathProps.md#oncanplaythrough)
- [onCanPlayThroughCapture](Globe_Cartography_Types.PathProps.md#oncanplaythroughcapture)
- [onChange](Globe_Cartography_Types.PathProps.md#onchange)
- [onChangeCapture](Globe_Cartography_Types.PathProps.md#onchangecapture)
- [onClick](Globe_Cartography_Types.PathProps.md#onclick)
- [onClickCapture](Globe_Cartography_Types.PathProps.md#onclickcapture)
- [onCompositionEnd](Globe_Cartography_Types.PathProps.md#oncompositionend)
- [onCompositionEndCapture](Globe_Cartography_Types.PathProps.md#oncompositionendcapture)
- [onCompositionStart](Globe_Cartography_Types.PathProps.md#oncompositionstart)
- [onCompositionStartCapture](Globe_Cartography_Types.PathProps.md#oncompositionstartcapture)
- [onCompositionUpdate](Globe_Cartography_Types.PathProps.md#oncompositionupdate)
- [onCompositionUpdateCapture](Globe_Cartography_Types.PathProps.md#oncompositionupdatecapture)
- [onContextMenu](Globe_Cartography_Types.PathProps.md#oncontextmenu)
- [onContextMenuCapture](Globe_Cartography_Types.PathProps.md#oncontextmenucapture)
- [onCopy](Globe_Cartography_Types.PathProps.md#oncopy)
- [onCopyCapture](Globe_Cartography_Types.PathProps.md#oncopycapture)
- [onCut](Globe_Cartography_Types.PathProps.md#oncut)
- [onCutCapture](Globe_Cartography_Types.PathProps.md#oncutcapture)
- [onDoubleClick](Globe_Cartography_Types.PathProps.md#ondoubleclick)
- [onDoubleClickCapture](Globe_Cartography_Types.PathProps.md#ondoubleclickcapture)
- [onDrag](Globe_Cartography_Types.PathProps.md#ondrag)
- [onDragCapture](Globe_Cartography_Types.PathProps.md#ondragcapture)
- [onDragEnd](Globe_Cartography_Types.PathProps.md#ondragend)
- [onDragEndCapture](Globe_Cartography_Types.PathProps.md#ondragendcapture)
- [onDragEnter](Globe_Cartography_Types.PathProps.md#ondragenter)
- [onDragEnterCapture](Globe_Cartography_Types.PathProps.md#ondragentercapture)
- [onDragExit](Globe_Cartography_Types.PathProps.md#ondragexit)
- [onDragExitCapture](Globe_Cartography_Types.PathProps.md#ondragexitcapture)
- [onDragLeave](Globe_Cartography_Types.PathProps.md#ondragleave)
- [onDragLeaveCapture](Globe_Cartography_Types.PathProps.md#ondragleavecapture)
- [onDragOver](Globe_Cartography_Types.PathProps.md#ondragover)
- [onDragOverCapture](Globe_Cartography_Types.PathProps.md#ondragovercapture)
- [onDragStart](Globe_Cartography_Types.PathProps.md#ondragstart)
- [onDragStartCapture](Globe_Cartography_Types.PathProps.md#ondragstartcapture)
- [onDrop](Globe_Cartography_Types.PathProps.md#ondrop)
- [onDropCapture](Globe_Cartography_Types.PathProps.md#ondropcapture)
- [onDurationChange](Globe_Cartography_Types.PathProps.md#ondurationchange)
- [onDurationChangeCapture](Globe_Cartography_Types.PathProps.md#ondurationchangecapture)
- [onEmptied](Globe_Cartography_Types.PathProps.md#onemptied)
- [onEmptiedCapture](Globe_Cartography_Types.PathProps.md#onemptiedcapture)
- [onEncrypted](Globe_Cartography_Types.PathProps.md#onencrypted)
- [onEncryptedCapture](Globe_Cartography_Types.PathProps.md#onencryptedcapture)
- [onEnded](Globe_Cartography_Types.PathProps.md#onended)
- [onEndedCapture](Globe_Cartography_Types.PathProps.md#onendedcapture)
- [onError](Globe_Cartography_Types.PathProps.md#onerror)
- [onErrorCapture](Globe_Cartography_Types.PathProps.md#onerrorcapture)
- [onFocus](Globe_Cartography_Types.PathProps.md#onfocus)
- [onFocusCapture](Globe_Cartography_Types.PathProps.md#onfocuscapture)
- [onGotPointerCapture](Globe_Cartography_Types.PathProps.md#ongotpointercapture)
- [onGotPointerCaptureCapture](Globe_Cartography_Types.PathProps.md#ongotpointercapturecapture)
- [onInput](Globe_Cartography_Types.PathProps.md#oninput)
- [onInputCapture](Globe_Cartography_Types.PathProps.md#oninputcapture)
- [onInvalid](Globe_Cartography_Types.PathProps.md#oninvalid)
- [onInvalidCapture](Globe_Cartography_Types.PathProps.md#oninvalidcapture)
- [onKeyDown](Globe_Cartography_Types.PathProps.md#onkeydown)
- [onKeyDownCapture](Globe_Cartography_Types.PathProps.md#onkeydowncapture)
- [onKeyPress](Globe_Cartography_Types.PathProps.md#onkeypress)
- [onKeyPressCapture](Globe_Cartography_Types.PathProps.md#onkeypresscapture)
- [onKeyUp](Globe_Cartography_Types.PathProps.md#onkeyup)
- [onKeyUpCapture](Globe_Cartography_Types.PathProps.md#onkeyupcapture)
- [onLoad](Globe_Cartography_Types.PathProps.md#onload)
- [onLoadCapture](Globe_Cartography_Types.PathProps.md#onloadcapture)
- [onLoadStart](Globe_Cartography_Types.PathProps.md#onloadstart)
- [onLoadStartCapture](Globe_Cartography_Types.PathProps.md#onloadstartcapture)
- [onLoadedData](Globe_Cartography_Types.PathProps.md#onloadeddata)
- [onLoadedDataCapture](Globe_Cartography_Types.PathProps.md#onloadeddatacapture)
- [onLoadedMetadata](Globe_Cartography_Types.PathProps.md#onloadedmetadata)
- [onLoadedMetadataCapture](Globe_Cartography_Types.PathProps.md#onloadedmetadatacapture)
- [onLostPointerCapture](Globe_Cartography_Types.PathProps.md#onlostpointercapture)
- [onLostPointerCaptureCapture](Globe_Cartography_Types.PathProps.md#onlostpointercapturecapture)
- [onMouseDown](Globe_Cartography_Types.PathProps.md#onmousedown)
- [onMouseDownCapture](Globe_Cartography_Types.PathProps.md#onmousedowncapture)
- [onMouseEnter](Globe_Cartography_Types.PathProps.md#onmouseenter)
- [onMouseLeave](Globe_Cartography_Types.PathProps.md#onmouseleave)
- [onMouseMove](Globe_Cartography_Types.PathProps.md#onmousemove)
- [onMouseMoveCapture](Globe_Cartography_Types.PathProps.md#onmousemovecapture)
- [onMouseOut](Globe_Cartography_Types.PathProps.md#onmouseout)
- [onMouseOutCapture](Globe_Cartography_Types.PathProps.md#onmouseoutcapture)
- [onMouseOver](Globe_Cartography_Types.PathProps.md#onmouseover)
- [onMouseOverCapture](Globe_Cartography_Types.PathProps.md#onmouseovercapture)
- [onMouseUp](Globe_Cartography_Types.PathProps.md#onmouseup)
- [onMouseUpCapture](Globe_Cartography_Types.PathProps.md#onmouseupcapture)
- [onPaste](Globe_Cartography_Types.PathProps.md#onpaste)
- [onPasteCapture](Globe_Cartography_Types.PathProps.md#onpastecapture)
- [onPause](Globe_Cartography_Types.PathProps.md#onpause)
- [onPauseCapture](Globe_Cartography_Types.PathProps.md#onpausecapture)
- [onPlay](Globe_Cartography_Types.PathProps.md#onplay)
- [onPlayCapture](Globe_Cartography_Types.PathProps.md#onplaycapture)
- [onPlaying](Globe_Cartography_Types.PathProps.md#onplaying)
- [onPlayingCapture](Globe_Cartography_Types.PathProps.md#onplayingcapture)
- [onPointerCancel](Globe_Cartography_Types.PathProps.md#onpointercancel)
- [onPointerCancelCapture](Globe_Cartography_Types.PathProps.md#onpointercancelcapture)
- [onPointerDown](Globe_Cartography_Types.PathProps.md#onpointerdown)
- [onPointerDownCapture](Globe_Cartography_Types.PathProps.md#onpointerdowncapture)
- [onPointerEnter](Globe_Cartography_Types.PathProps.md#onpointerenter)
- [onPointerEnterCapture](Globe_Cartography_Types.PathProps.md#onpointerentercapture)
- [onPointerLeave](Globe_Cartography_Types.PathProps.md#onpointerleave)
- [onPointerLeaveCapture](Globe_Cartography_Types.PathProps.md#onpointerleavecapture)
- [onPointerMove](Globe_Cartography_Types.PathProps.md#onpointermove)
- [onPointerMoveCapture](Globe_Cartography_Types.PathProps.md#onpointermovecapture)
- [onPointerOut](Globe_Cartography_Types.PathProps.md#onpointerout)
- [onPointerOutCapture](Globe_Cartography_Types.PathProps.md#onpointeroutcapture)
- [onPointerOver](Globe_Cartography_Types.PathProps.md#onpointerover)
- [onPointerOverCapture](Globe_Cartography_Types.PathProps.md#onpointerovercapture)
- [onPointerUp](Globe_Cartography_Types.PathProps.md#onpointerup)
- [onPointerUpCapture](Globe_Cartography_Types.PathProps.md#onpointerupcapture)
- [onProgress](Globe_Cartography_Types.PathProps.md#onprogress)
- [onProgressCapture](Globe_Cartography_Types.PathProps.md#onprogresscapture)
- [onRateChange](Globe_Cartography_Types.PathProps.md#onratechange)
- [onRateChangeCapture](Globe_Cartography_Types.PathProps.md#onratechangecapture)
- [onReset](Globe_Cartography_Types.PathProps.md#onreset)
- [onResetCapture](Globe_Cartography_Types.PathProps.md#onresetcapture)
- [onScroll](Globe_Cartography_Types.PathProps.md#onscroll)
- [onScrollCapture](Globe_Cartography_Types.PathProps.md#onscrollcapture)
- [onSeeked](Globe_Cartography_Types.PathProps.md#onseeked)
- [onSeekedCapture](Globe_Cartography_Types.PathProps.md#onseekedcapture)
- [onSeeking](Globe_Cartography_Types.PathProps.md#onseeking)
- [onSeekingCapture](Globe_Cartography_Types.PathProps.md#onseekingcapture)
- [onSelect](Globe_Cartography_Types.PathProps.md#onselect)
- [onSelectCapture](Globe_Cartography_Types.PathProps.md#onselectcapture)
- [onStalled](Globe_Cartography_Types.PathProps.md#onstalled)
- [onStalledCapture](Globe_Cartography_Types.PathProps.md#onstalledcapture)
- [onSubmit](Globe_Cartography_Types.PathProps.md#onsubmit)
- [onSubmitCapture](Globe_Cartography_Types.PathProps.md#onsubmitcapture)
- [onSuspend](Globe_Cartography_Types.PathProps.md#onsuspend)
- [onSuspendCapture](Globe_Cartography_Types.PathProps.md#onsuspendcapture)
- [onTimeUpdate](Globe_Cartography_Types.PathProps.md#ontimeupdate)
- [onTimeUpdateCapture](Globe_Cartography_Types.PathProps.md#ontimeupdatecapture)
- [onTouchCancel](Globe_Cartography_Types.PathProps.md#ontouchcancel)
- [onTouchCancelCapture](Globe_Cartography_Types.PathProps.md#ontouchcancelcapture)
- [onTouchEnd](Globe_Cartography_Types.PathProps.md#ontouchend)
- [onTouchEndCapture](Globe_Cartography_Types.PathProps.md#ontouchendcapture)
- [onTouchMove](Globe_Cartography_Types.PathProps.md#ontouchmove)
- [onTouchMoveCapture](Globe_Cartography_Types.PathProps.md#ontouchmovecapture)
- [onTouchStart](Globe_Cartography_Types.PathProps.md#ontouchstart)
- [onTouchStartCapture](Globe_Cartography_Types.PathProps.md#ontouchstartcapture)
- [onTransitionEnd](Globe_Cartography_Types.PathProps.md#ontransitionend)
- [onTransitionEndCapture](Globe_Cartography_Types.PathProps.md#ontransitionendcapture)
- [onVolumeChange](Globe_Cartography_Types.PathProps.md#onvolumechange)
- [onVolumeChangeCapture](Globe_Cartography_Types.PathProps.md#onvolumechangecapture)
- [onWaiting](Globe_Cartography_Types.PathProps.md#onwaiting)
- [onWaitingCapture](Globe_Cartography_Types.PathProps.md#onwaitingcapture)
- [onWheel](Globe_Cartography_Types.PathProps.md#onwheel)
- [onWheelCapture](Globe_Cartography_Types.PathProps.md#onwheelcapture)
- [opacity](Globe_Cartography_Types.PathProps.md#opacity)
- [operator](Globe_Cartography_Types.PathProps.md#operator)
- [order](Globe_Cartography_Types.PathProps.md#order)
- [orient](Globe_Cartography_Types.PathProps.md#orient)
- [orientation](Globe_Cartography_Types.PathProps.md#orientation)
- [origin](Globe_Cartography_Types.PathProps.md#origin)
- [overflow](Globe_Cartography_Types.PathProps.md#overflow)
- [overlinePosition](Globe_Cartography_Types.PathProps.md#overlineposition)
- [overlineThickness](Globe_Cartography_Types.PathProps.md#overlinethickness)
- [paintOrder](Globe_Cartography_Types.PathProps.md#paintorder)
- [panose1](Globe_Cartography_Types.PathProps.md#panose1)
- [path](Globe_Cartography_Types.PathProps.md#path)
- [pathLength](Globe_Cartography_Types.PathProps.md#pathlength)
- [patternContentUnits](Globe_Cartography_Types.PathProps.md#patterncontentunits)
- [patternTransform](Globe_Cartography_Types.PathProps.md#patterntransform)
- [patternUnits](Globe_Cartography_Types.PathProps.md#patternunits)
- [pointerEvents](Globe_Cartography_Types.PathProps.md#pointerevents)
- [points](Globe_Cartography_Types.PathProps.md#points)
- [pointsAtX](Globe_Cartography_Types.PathProps.md#pointsatx)
- [pointsAtY](Globe_Cartography_Types.PathProps.md#pointsaty)
- [pointsAtZ](Globe_Cartography_Types.PathProps.md#pointsatz)
- [preserveAlpha](Globe_Cartography_Types.PathProps.md#preservealpha)
- [preserveAspectRatio](Globe_Cartography_Types.PathProps.md#preserveaspectratio)
- [primitiveUnits](Globe_Cartography_Types.PathProps.md#primitiveunits)
- [r](Globe_Cartography_Types.PathProps.md#r)
- [radius](Globe_Cartography_Types.PathProps.md#radius)
- [ref](Globe_Cartography_Types.PathProps.md#ref)
- [refX](Globe_Cartography_Types.PathProps.md#refx)
- [refY](Globe_Cartography_Types.PathProps.md#refy)
- [renderingIntent](Globe_Cartography_Types.PathProps.md#renderingintent)
- [repeatCount](Globe_Cartography_Types.PathProps.md#repeatcount)
- [repeatDur](Globe_Cartography_Types.PathProps.md#repeatdur)
- [requiredExtensions](Globe_Cartography_Types.PathProps.md#requiredextensions)
- [requiredFeatures](Globe_Cartography_Types.PathProps.md#requiredfeatures)
- [restart](Globe_Cartography_Types.PathProps.md#restart)
- [result](Globe_Cartography_Types.PathProps.md#result)
- [role](Globe_Cartography_Types.PathProps.md#role)
- [rotate](Globe_Cartography_Types.PathProps.md#rotate)
- [rx](Globe_Cartography_Types.PathProps.md#rx)
- [ry](Globe_Cartography_Types.PathProps.md#ry)
- [scale](Globe_Cartography_Types.PathProps.md#scale)
- [seed](Globe_Cartography_Types.PathProps.md#seed)
- [shapeRendering](Globe_Cartography_Types.PathProps.md#shaperendering)
- [slope](Globe_Cartography_Types.PathProps.md#slope)
- [spacing](Globe_Cartography_Types.PathProps.md#spacing)
- [specularConstant](Globe_Cartography_Types.PathProps.md#specularconstant)
- [specularExponent](Globe_Cartography_Types.PathProps.md#specularexponent)
- [speed](Globe_Cartography_Types.PathProps.md#speed)
- [spreadMethod](Globe_Cartography_Types.PathProps.md#spreadmethod)
- [startOffset](Globe_Cartography_Types.PathProps.md#startoffset)
- [stdDeviation](Globe_Cartography_Types.PathProps.md#stddeviation)
- [stemh](Globe_Cartography_Types.PathProps.md#stemh)
- [stemv](Globe_Cartography_Types.PathProps.md#stemv)
- [stitchTiles](Globe_Cartography_Types.PathProps.md#stitchtiles)
- [stopColor](Globe_Cartography_Types.PathProps.md#stopcolor)
- [stopOpacity](Globe_Cartography_Types.PathProps.md#stopopacity)
- [strikethroughPosition](Globe_Cartography_Types.PathProps.md#strikethroughposition)
- [strikethroughThickness](Globe_Cartography_Types.PathProps.md#strikethroughthickness)
- [string](Globe_Cartography_Types.PathProps.md#string)
- [stroke](Globe_Cartography_Types.PathProps.md#stroke)
- [strokeDasharray](Globe_Cartography_Types.PathProps.md#strokedasharray)
- [strokeDashoffset](Globe_Cartography_Types.PathProps.md#strokedashoffset)
- [strokeLinecap](Globe_Cartography_Types.PathProps.md#strokelinecap)
- [strokeLinejoin](Globe_Cartography_Types.PathProps.md#strokelinejoin)
- [strokeMiterlimit](Globe_Cartography_Types.PathProps.md#strokemiterlimit)
- [strokeOpacity](Globe_Cartography_Types.PathProps.md#strokeopacity)
- [strokeWidth](Globe_Cartography_Types.PathProps.md#strokewidth)
- [style](Globe_Cartography_Types.PathProps.md#style)
- [surfaceScale](Globe_Cartography_Types.PathProps.md#surfacescale)
- [systemLanguage](Globe_Cartography_Types.PathProps.md#systemlanguage)
- [tabIndex](Globe_Cartography_Types.PathProps.md#tabindex)
- [tableValues](Globe_Cartography_Types.PathProps.md#tablevalues)
- [target](Globe_Cartography_Types.PathProps.md#target)
- [targetX](Globe_Cartography_Types.PathProps.md#targetx)
- [targetY](Globe_Cartography_Types.PathProps.md#targety)
- [textAnchor](Globe_Cartography_Types.PathProps.md#textanchor)
- [textDecoration](Globe_Cartography_Types.PathProps.md#textdecoration)
- [textLength](Globe_Cartography_Types.PathProps.md#textlength)
- [textRendering](Globe_Cartography_Types.PathProps.md#textrendering)
- [to](Globe_Cartography_Types.PathProps.md#to)
- [transform](Globe_Cartography_Types.PathProps.md#transform)
- [type](Globe_Cartography_Types.PathProps.md#type)
- [u1](Globe_Cartography_Types.PathProps.md#u1)
- [u2](Globe_Cartography_Types.PathProps.md#u2)
- [underlinePosition](Globe_Cartography_Types.PathProps.md#underlineposition)
- [underlineThickness](Globe_Cartography_Types.PathProps.md#underlinethickness)
- [unicode](Globe_Cartography_Types.PathProps.md#unicode)
- [unicodeBidi](Globe_Cartography_Types.PathProps.md#unicodebidi)
- [unicodeRange](Globe_Cartography_Types.PathProps.md#unicoderange)
- [unitsPerEm](Globe_Cartography_Types.PathProps.md#unitsperem)
- [vAlphabetic](Globe_Cartography_Types.PathProps.md#valphabetic)
- [vHanging](Globe_Cartography_Types.PathProps.md#vhanging)
- [vIdeographic](Globe_Cartography_Types.PathProps.md#videographic)
- [vMathematical](Globe_Cartography_Types.PathProps.md#vmathematical)
- [values](Globe_Cartography_Types.PathProps.md#values)
- [vectorEffect](Globe_Cartography_Types.PathProps.md#vectoreffect)
- [version](Globe_Cartography_Types.PathProps.md#version)
- [vertAdvY](Globe_Cartography_Types.PathProps.md#vertadvy)
- [vertOriginX](Globe_Cartography_Types.PathProps.md#vertoriginx)
- [vertOriginY](Globe_Cartography_Types.PathProps.md#vertoriginy)
- [viewBox](Globe_Cartography_Types.PathProps.md#viewbox)
- [viewTarget](Globe_Cartography_Types.PathProps.md#viewtarget)
- [visibility](Globe_Cartography_Types.PathProps.md#visibility)
- [width](Globe_Cartography_Types.PathProps.md#width)
- [widths](Globe_Cartography_Types.PathProps.md#widths)
- [wordSpacing](Globe_Cartography_Types.PathProps.md#wordspacing)
- [writingMode](Globe_Cartography_Types.PathProps.md#writingmode)
- [x](Globe_Cartography_Types.PathProps.md#x)
- [x1](Globe_Cartography_Types.PathProps.md#x1)
- [x2](Globe_Cartography_Types.PathProps.md#x2)
- [xChannelSelector](Globe_Cartography_Types.PathProps.md#xchannelselector)
- [xHeight](Globe_Cartography_Types.PathProps.md#xheight)
- [xlinkActuate](Globe_Cartography_Types.PathProps.md#xlinkactuate)
- [xlinkArcrole](Globe_Cartography_Types.PathProps.md#xlinkarcrole)
- [xlinkHref](Globe_Cartography_Types.PathProps.md#xlinkhref)
- [xlinkRole](Globe_Cartography_Types.PathProps.md#xlinkrole)
- [xlinkShow](Globe_Cartography_Types.PathProps.md#xlinkshow)
- [xlinkTitle](Globe_Cartography_Types.PathProps.md#xlinktitle)
- [xlinkType](Globe_Cartography_Types.PathProps.md#xlinktype)
- [xmlBase](Globe_Cartography_Types.PathProps.md#xmlbase)
- [xmlLang](Globe_Cartography_Types.PathProps.md#xmllang)
- [xmlSpace](Globe_Cartography_Types.PathProps.md#xmlspace)
- [xmlns](Globe_Cartography_Types.PathProps.md#xmlns)
- [xmlnsXlink](Globe_Cartography_Types.PathProps.md#xmlnsxlink)
- [y](Globe_Cartography_Types.PathProps.md#y)
- [y1](Globe_Cartography_Types.PathProps.md#y1)
- [y2](Globe_Cartography_Types.PathProps.md#y2)
- [yChannelSelector](Globe_Cartography_Types.PathProps.md#ychannelselector)
- [z](Globe_Cartography_Types.PathProps.md#z)
- [zoomAndPan](Globe_Cartography_Types.PathProps.md#zoomandpan)

## Properties

### accentHeight

• `Optional` **accentHeight**: `string` \| `number`

#### Inherited from

React.SVGProps.accentHeight

#### Defined in

node_modules/@types/react/index.d.ts:2510

___

### accumulate

• `Optional` **accumulate**: ``"none"`` \| ``"sum"``

#### Inherited from

React.SVGProps.accumulate

#### Defined in

node_modules/@types/react/index.d.ts:2511

___

### additive

• `Optional` **additive**: ``"replace"`` \| ``"sum"``

#### Inherited from

React.SVGProps.additive

#### Defined in

node_modules/@types/react/index.d.ts:2512

___

### alignmentBaseline

• `Optional` **alignmentBaseline**: ``"inherit"`` \| ``"auto"`` \| ``"baseline"`` \| ``"before-edge"`` \| ``"text-before-edge"`` \| ``"middle"`` \| ``"central"`` \| ``"after-edge"`` \| ``"text-after-edge"`` \| ``"ideographic"`` \| ``"alphabetic"`` \| ``"hanging"`` \| ``"mathematical"``

#### Inherited from

React.SVGProps.alignmentBaseline

#### Defined in

node_modules/@types/react/index.d.ts:2513

___

### allowReorder

• `Optional` **allowReorder**: ``"no"`` \| ``"yes"``

#### Inherited from

React.SVGProps.allowReorder

#### Defined in

node_modules/@types/react/index.d.ts:2515

___

### alphabetic

• `Optional` **alphabetic**: `string` \| `number`

#### Inherited from

React.SVGProps.alphabetic

#### Defined in

node_modules/@types/react/index.d.ts:2516

___

### amplitude

• `Optional` **amplitude**: `string` \| `number`

#### Inherited from

React.SVGProps.amplitude

#### Defined in

node_modules/@types/react/index.d.ts:2517

___

### arabicForm

• `Optional` **arabicForm**: ``"initial"`` \| ``"medial"`` \| ``"terminal"`` \| ``"isolated"``

#### Inherited from

React.SVGProps.arabicForm

#### Defined in

node_modules/@types/react/index.d.ts:2518

___

### aria-activedescendant

• `Optional` **aria-activedescendant**: `string`

Identifies the currently active element when DOM focus is on a composite widget, textbox, group, or application.

#### Inherited from

React.SVGProps.aria-activedescendant

#### Defined in

node_modules/@types/react/index.d.ts:1569

___

### aria-atomic

• `Optional` **aria-atomic**: `Booleanish`

Indicates whether assistive technologies will present all, or only parts of, the changed region based on the change notifications defined by the aria-relevant attribute.

#### Inherited from

React.SVGProps.aria-atomic

#### Defined in

node_modules/@types/react/index.d.ts:1571

___

### aria-autocomplete

• `Optional` **aria-autocomplete**: ``"none"`` \| ``"list"`` \| ``"inline"`` \| ``"both"``

Indicates whether inputting text could trigger display of one or more predictions of the user's intended value for an input and specifies how predictions would be
presented if they are made.

#### Inherited from

React.SVGProps.aria-autocomplete

#### Defined in

node_modules/@types/react/index.d.ts:1576

___

### aria-busy

• `Optional` **aria-busy**: `Booleanish`

Indicates an element is being modified and that assistive technologies MAY want to wait until the modifications are complete before exposing them to the user.

#### Inherited from

React.SVGProps.aria-busy

#### Defined in

node_modules/@types/react/index.d.ts:1578

___

### aria-checked

• `Optional` **aria-checked**: `boolean` \| ``"false"`` \| ``"true"`` \| ``"mixed"``

Indicates the current "checked" state of checkboxes, radio buttons, and other widgets.

**`See`**

 - aria-pressed
 - aria-selected.

#### Inherited from

React.SVGProps.aria-checked

#### Defined in

node_modules/@types/react/index.d.ts:1583

___

### aria-colcount

• `Optional` **aria-colcount**: `number`

Defines the total number of columns in a table, grid, or treegrid.

**`See`**

aria-colindex.

#### Inherited from

React.SVGProps.aria-colcount

#### Defined in

node_modules/@types/react/index.d.ts:1588

___

### aria-colindex

• `Optional` **aria-colindex**: `number`

Defines an element's column index or position with respect to the total number of columns within a table, grid, or treegrid.

**`See`**

 - aria-colcount
 - aria-colspan.

#### Inherited from

React.SVGProps.aria-colindex

#### Defined in

node_modules/@types/react/index.d.ts:1593

___

### aria-colspan

• `Optional` **aria-colspan**: `number`

Defines the number of columns spanned by a cell or gridcell within a table, grid, or treegrid.

**`See`**

 - aria-colindex
 - aria-rowspan.

#### Inherited from

React.SVGProps.aria-colspan

#### Defined in

node_modules/@types/react/index.d.ts:1598

___

### aria-controls

• `Optional` **aria-controls**: `string`

Identifies the element (or elements) whose contents or presence are controlled by the current element.

**`See`**

aria-owns.

#### Inherited from

React.SVGProps.aria-controls

#### Defined in

node_modules/@types/react/index.d.ts:1603

___

### aria-current

• `Optional` **aria-current**: `boolean` \| ``"time"`` \| ``"false"`` \| ``"true"`` \| ``"page"`` \| ``"step"`` \| ``"location"`` \| ``"date"``

Indicates the element that represents the current item within a container or set of related elements.

#### Inherited from

React.SVGProps.aria-current

#### Defined in

node_modules/@types/react/index.d.ts:1605

___

### aria-describedby

• `Optional` **aria-describedby**: `string`

Identifies the element (or elements) that describes the object.

**`See`**

aria-labelledby

#### Inherited from

React.SVGProps.aria-describedby

#### Defined in

node_modules/@types/react/index.d.ts:1610

___

### aria-details

• `Optional` **aria-details**: `string`

Identifies the element that provides a detailed, extended description for the object.

**`See`**

aria-describedby.

#### Inherited from

React.SVGProps.aria-details

#### Defined in

node_modules/@types/react/index.d.ts:1615

___

### aria-disabled

• `Optional` **aria-disabled**: `Booleanish`

Indicates that the element is perceivable but disabled, so it is not editable or otherwise operable.

**`See`**

 - aria-hidden
 - aria-readonly.

#### Inherited from

React.SVGProps.aria-disabled

#### Defined in

node_modules/@types/react/index.d.ts:1620

___

### aria-dropeffect

• `Optional` **aria-dropeffect**: ``"link"`` \| ``"move"`` \| ``"none"`` \| ``"copy"`` \| ``"execute"`` \| ``"popup"``

Indicates what functions can be performed when a dragged object is released on the drop target.

**`Deprecated`**

in ARIA 1.1

#### Inherited from

React.SVGProps.aria-dropeffect

#### Defined in

node_modules/@types/react/index.d.ts:1625

___

### aria-errormessage

• `Optional` **aria-errormessage**: `string`

Identifies the element that provides an error message for the object.

**`See`**

 - aria-invalid
 - aria-describedby.

#### Inherited from

React.SVGProps.aria-errormessage

#### Defined in

node_modules/@types/react/index.d.ts:1630

___

### aria-expanded

• `Optional` **aria-expanded**: `Booleanish`

Indicates whether the element, or another grouping element it controls, is currently expanded or collapsed.

#### Inherited from

React.SVGProps.aria-expanded

#### Defined in

node_modules/@types/react/index.d.ts:1632

___

### aria-flowto

• `Optional` **aria-flowto**: `string`

Identifies the next element (or elements) in an alternate reading order of content which, at the user's discretion,
allows assistive technology to override the general default of reading in document source order.

#### Inherited from

React.SVGProps.aria-flowto

#### Defined in

node_modules/@types/react/index.d.ts:1637

___

### aria-grabbed

• `Optional` **aria-grabbed**: `Booleanish`

Indicates an element's "grabbed" state in a drag-and-drop operation.

**`Deprecated`**

in ARIA 1.1

#### Inherited from

React.SVGProps.aria-grabbed

#### Defined in

node_modules/@types/react/index.d.ts:1642

___

### aria-haspopup

• `Optional` **aria-haspopup**: `boolean` \| ``"dialog"`` \| ``"menu"`` \| ``"false"`` \| ``"true"`` \| ``"grid"`` \| ``"listbox"`` \| ``"tree"``

Indicates the availability and type of interactive popup element, such as menu or dialog, that can be triggered by an element.

#### Inherited from

React.SVGProps.aria-haspopup

#### Defined in

node_modules/@types/react/index.d.ts:1644

___

### aria-hidden

• `Optional` **aria-hidden**: `Booleanish`

Indicates whether the element is exposed to an accessibility API.

**`See`**

aria-disabled.

#### Inherited from

React.SVGProps.aria-hidden

#### Defined in

node_modules/@types/react/index.d.ts:1649

___

### aria-invalid

• `Optional` **aria-invalid**: `boolean` \| ``"false"`` \| ``"true"`` \| ``"grammar"`` \| ``"spelling"``

Indicates the entered value does not conform to the format expected by the application.

**`See`**

aria-errormessage.

#### Inherited from

React.SVGProps.aria-invalid

#### Defined in

node_modules/@types/react/index.d.ts:1654

___

### aria-keyshortcuts

• `Optional` **aria-keyshortcuts**: `string`

Indicates keyboard shortcuts that an author has implemented to activate or give focus to an element.

#### Inherited from

React.SVGProps.aria-keyshortcuts

#### Defined in

node_modules/@types/react/index.d.ts:1656

___

### aria-label

• `Optional` **aria-label**: `string`

Defines a string value that labels the current element.

**`See`**

aria-labelledby.

#### Inherited from

React.SVGProps.aria-label

#### Defined in

node_modules/@types/react/index.d.ts:1661

___

### aria-labelledby

• `Optional` **aria-labelledby**: `string`

Identifies the element (or elements) that labels the current element.

**`See`**

aria-describedby.

#### Inherited from

React.SVGProps.aria-labelledby

#### Defined in

node_modules/@types/react/index.d.ts:1666

___

### aria-level

• `Optional` **aria-level**: `number`

Defines the hierarchical level of an element within a structure.

#### Inherited from

React.SVGProps.aria-level

#### Defined in

node_modules/@types/react/index.d.ts:1668

___

### aria-live

• `Optional` **aria-live**: ``"off"`` \| ``"assertive"`` \| ``"polite"``

Indicates that an element will be updated, and describes the types of updates the user agents, assistive technologies, and user can expect from the live region.

#### Inherited from

React.SVGProps.aria-live

#### Defined in

node_modules/@types/react/index.d.ts:1670

___

### aria-modal

• `Optional` **aria-modal**: `Booleanish`

Indicates whether an element is modal when displayed.

#### Inherited from

React.SVGProps.aria-modal

#### Defined in

node_modules/@types/react/index.d.ts:1672

___

### aria-multiline

• `Optional` **aria-multiline**: `Booleanish`

Indicates whether a text box accepts multiple lines of input or only a single line.

#### Inherited from

React.SVGProps.aria-multiline

#### Defined in

node_modules/@types/react/index.d.ts:1674

___

### aria-multiselectable

• `Optional` **aria-multiselectable**: `Booleanish`

Indicates that the user may select more than one item from the current selectable descendants.

#### Inherited from

React.SVGProps.aria-multiselectable

#### Defined in

node_modules/@types/react/index.d.ts:1676

___

### aria-orientation

• `Optional` **aria-orientation**: ``"horizontal"`` \| ``"vertical"``

Indicates whether the element's orientation is horizontal, vertical, or unknown/ambiguous.

#### Inherited from

React.SVGProps.aria-orientation

#### Defined in

node_modules/@types/react/index.d.ts:1678

___

### aria-owns

• `Optional` **aria-owns**: `string`

Identifies an element (or elements) in order to define a visual, functional, or contextual parent/child relationship
between DOM elements where the DOM hierarchy cannot be used to represent the relationship.

**`See`**

aria-controls.

#### Inherited from

React.SVGProps.aria-owns

#### Defined in

node_modules/@types/react/index.d.ts:1684

___

### aria-placeholder

• `Optional` **aria-placeholder**: `string`

Defines a short hint (a word or short phrase) intended to aid the user with data entry when the control has no value.
A hint could be a sample value or a brief description of the expected format.

#### Inherited from

React.SVGProps.aria-placeholder

#### Defined in

node_modules/@types/react/index.d.ts:1689

___

### aria-posinset

• `Optional` **aria-posinset**: `number`

Defines an element's number or position in the current set of listitems or treeitems. Not required if all elements in the set are present in the DOM.

**`See`**

aria-setsize.

#### Inherited from

React.SVGProps.aria-posinset

#### Defined in

node_modules/@types/react/index.d.ts:1694

___

### aria-pressed

• `Optional` **aria-pressed**: `boolean` \| ``"false"`` \| ``"true"`` \| ``"mixed"``

Indicates the current "pressed" state of toggle buttons.

**`See`**

 - aria-checked
 - aria-selected.

#### Inherited from

React.SVGProps.aria-pressed

#### Defined in

node_modules/@types/react/index.d.ts:1699

___

### aria-readonly

• `Optional` **aria-readonly**: `Booleanish`

Indicates that the element is not editable, but is otherwise operable.

**`See`**

aria-disabled.

#### Inherited from

React.SVGProps.aria-readonly

#### Defined in

node_modules/@types/react/index.d.ts:1704

___

### aria-relevant

• `Optional` **aria-relevant**: ``"text"`` \| ``"all"`` \| ``"additions"`` \| ``"additions removals"`` \| ``"additions text"`` \| ``"removals"`` \| ``"removals additions"`` \| ``"removals text"`` \| ``"text additions"`` \| ``"text removals"``

Indicates what notifications the user agent will trigger when the accessibility tree within a live region is modified.

**`See`**

aria-atomic.

#### Inherited from

React.SVGProps.aria-relevant

#### Defined in

node_modules/@types/react/index.d.ts:1709

___

### aria-required

• `Optional` **aria-required**: `Booleanish`

Indicates that user input is required on the element before a form may be submitted.

#### Inherited from

React.SVGProps.aria-required

#### Defined in

node_modules/@types/react/index.d.ts:1711

___

### aria-roledescription

• `Optional` **aria-roledescription**: `string`

Defines a human-readable, author-localized description for the role of an element.

#### Inherited from

React.SVGProps.aria-roledescription

#### Defined in

node_modules/@types/react/index.d.ts:1713

___

### aria-rowcount

• `Optional` **aria-rowcount**: `number`

Defines the total number of rows in a table, grid, or treegrid.

**`See`**

aria-rowindex.

#### Inherited from

React.SVGProps.aria-rowcount

#### Defined in

node_modules/@types/react/index.d.ts:1718

___

### aria-rowindex

• `Optional` **aria-rowindex**: `number`

Defines an element's row index or position with respect to the total number of rows within a table, grid, or treegrid.

**`See`**

 - aria-rowcount
 - aria-rowspan.

#### Inherited from

React.SVGProps.aria-rowindex

#### Defined in

node_modules/@types/react/index.d.ts:1723

___

### aria-rowspan

• `Optional` **aria-rowspan**: `number`

Defines the number of rows spanned by a cell or gridcell within a table, grid, or treegrid.

**`See`**

 - aria-rowindex
 - aria-colspan.

#### Inherited from

React.SVGProps.aria-rowspan

#### Defined in

node_modules/@types/react/index.d.ts:1728

___

### aria-selected

• `Optional` **aria-selected**: `Booleanish`

Indicates the current "selected" state of various widgets.

**`See`**

 - aria-checked
 - aria-pressed.

#### Inherited from

React.SVGProps.aria-selected

#### Defined in

node_modules/@types/react/index.d.ts:1733

___

### aria-setsize

• `Optional` **aria-setsize**: `number`

Defines the number of items in the current set of listitems or treeitems. Not required if all elements in the set are present in the DOM.

**`See`**

aria-posinset.

#### Inherited from

React.SVGProps.aria-setsize

#### Defined in

node_modules/@types/react/index.d.ts:1738

___

### aria-sort

• `Optional` **aria-sort**: ``"none"`` \| ``"ascending"`` \| ``"descending"`` \| ``"other"``

Indicates if items in a table or grid are sorted in ascending or descending order.

#### Inherited from

React.SVGProps.aria-sort

#### Defined in

node_modules/@types/react/index.d.ts:1740

___

### aria-valuemax

• `Optional` **aria-valuemax**: `number`

Defines the maximum allowed value for a range widget.

#### Inherited from

React.SVGProps.aria-valuemax

#### Defined in

node_modules/@types/react/index.d.ts:1742

___

### aria-valuemin

• `Optional` **aria-valuemin**: `number`

Defines the minimum allowed value for a range widget.

#### Inherited from

React.SVGProps.aria-valuemin

#### Defined in

node_modules/@types/react/index.d.ts:1744

___

### aria-valuenow

• `Optional` **aria-valuenow**: `number`

Defines the current value for a range widget.

**`See`**

aria-valuetext.

#### Inherited from

React.SVGProps.aria-valuenow

#### Defined in

node_modules/@types/react/index.d.ts:1749

___

### aria-valuetext

• `Optional` **aria-valuetext**: `string`

Defines the human readable text alternative of aria-valuenow for a range widget.

#### Inherited from

React.SVGProps.aria-valuetext

#### Defined in

node_modules/@types/react/index.d.ts:1751

___

### ascent

• `Optional` **ascent**: `string` \| `number`

#### Inherited from

React.SVGProps.ascent

#### Defined in

node_modules/@types/react/index.d.ts:2519

___

### attributeName

• `Optional` **attributeName**: `string`

#### Inherited from

React.SVGProps.attributeName

#### Defined in

node_modules/@types/react/index.d.ts:2520

___

### attributeType

• `Optional` **attributeType**: `string`

#### Inherited from

React.SVGProps.attributeType

#### Defined in

node_modules/@types/react/index.d.ts:2521

___

### autoReverse

• `Optional` **autoReverse**: `Booleanish`

#### Inherited from

React.SVGProps.autoReverse

#### Defined in

node_modules/@types/react/index.d.ts:2522

___

### azimuth

• `Optional` **azimuth**: `string` \| `number`

#### Inherited from

React.SVGProps.azimuth

#### Defined in

node_modules/@types/react/index.d.ts:2523

___

### baseFrequency

• `Optional` **baseFrequency**: `string` \| `number`

#### Inherited from

React.SVGProps.baseFrequency

#### Defined in

node_modules/@types/react/index.d.ts:2524

___

### baseProfile

• `Optional` **baseProfile**: `string` \| `number`

#### Inherited from

React.SVGProps.baseProfile

#### Defined in

node_modules/@types/react/index.d.ts:2526

___

### baselineShift

• `Optional` **baselineShift**: `string` \| `number`

#### Inherited from

React.SVGProps.baselineShift

#### Defined in

node_modules/@types/react/index.d.ts:2525

___

### bbox

• `Optional` **bbox**: `string` \| `number`

#### Inherited from

React.SVGProps.bbox

#### Defined in

node_modules/@types/react/index.d.ts:2527

___

### begin

• `Optional` **begin**: `string` \| `number`

#### Inherited from

React.SVGProps.begin

#### Defined in

node_modules/@types/react/index.d.ts:2528

___

### bias

• `Optional` **bias**: `string` \| `number`

#### Inherited from

React.SVGProps.bias

#### Defined in

node_modules/@types/react/index.d.ts:2529

___

### by

• `Optional` **by**: `string` \| `number`

#### Inherited from

React.SVGProps.by

#### Defined in

node_modules/@types/react/index.d.ts:2530

___

### calcMode

• `Optional` **calcMode**: `string` \| `number`

#### Inherited from

React.SVGProps.calcMode

#### Defined in

node_modules/@types/react/index.d.ts:2531

___

### capHeight

• `Optional` **capHeight**: `string` \| `number`

#### Inherited from

React.SVGProps.capHeight

#### Defined in

node_modules/@types/react/index.d.ts:2532

___

### children

• `Optional` **children**: `ReactNode`

#### Inherited from

React.SVGProps.children

#### Defined in

node_modules/@types/react/index.d.ts:1359

___

### className

• `Optional` **className**: `string`

#### Inherited from

React.SVGProps.className

#### Defined in

node_modules/@types/react/index.d.ts:2489

___

### clip

• `Optional` **clip**: `string` \| `number`

#### Inherited from

React.SVGProps.clip

#### Defined in

node_modules/@types/react/index.d.ts:2533

___

### clipPath

• `Optional` **clipPath**: `string`

#### Inherited from

React.SVGProps.clipPath

#### Defined in

node_modules/@types/react/index.d.ts:2534

___

### clipPathUnits

• `Optional` **clipPathUnits**: `string` \| `number`

#### Inherited from

React.SVGProps.clipPathUnits

#### Defined in

node_modules/@types/react/index.d.ts:2535

___

### clipRule

• `Optional` **clipRule**: `string` \| `number`

#### Inherited from

React.SVGProps.clipRule

#### Defined in

node_modules/@types/react/index.d.ts:2536

___

### color

• `Optional` **color**: `string`

#### Inherited from

React.SVGProps.color

#### Defined in

node_modules/@types/react/index.d.ts:2490

___

### colorInterpolation

• `Optional` **colorInterpolation**: `string` \| `number`

#### Inherited from

React.SVGProps.colorInterpolation

#### Defined in

node_modules/@types/react/index.d.ts:2537

___

### colorInterpolationFilters

• `Optional` **colorInterpolationFilters**: ``"inherit"`` \| ``"auto"`` \| ``"sRGB"`` \| ``"linearRGB"``

#### Inherited from

React.SVGProps.colorInterpolationFilters

#### Defined in

node_modules/@types/react/index.d.ts:2538

___

### colorProfile

• `Optional` **colorProfile**: `string` \| `number`

#### Inherited from

React.SVGProps.colorProfile

#### Defined in

node_modules/@types/react/index.d.ts:2539

___

### colorRendering

• `Optional` **colorRendering**: `string` \| `number`

#### Inherited from

React.SVGProps.colorRendering

#### Defined in

node_modules/@types/react/index.d.ts:2540

___

### contentScriptType

• `Optional` **contentScriptType**: `string` \| `number`

#### Inherited from

React.SVGProps.contentScriptType

#### Defined in

node_modules/@types/react/index.d.ts:2541

___

### contentStyleType

• `Optional` **contentStyleType**: `string` \| `number`

#### Inherited from

React.SVGProps.contentStyleType

#### Defined in

node_modules/@types/react/index.d.ts:2542

___

### crossOrigin

• `Optional` **crossOrigin**: ``""`` \| ``"anonymous"`` \| ``"use-credentials"``

#### Inherited from

React.SVGProps.crossOrigin

#### Defined in

node_modules/@types/react/index.d.ts:2507

___

### cursor

• `Optional` **cursor**: `string` \| `number`

#### Inherited from

React.SVGProps.cursor

#### Defined in

node_modules/@types/react/index.d.ts:2543

___

### cx

• `Optional` **cx**: `string` \| `number`

#### Inherited from

React.SVGProps.cx

#### Defined in

node_modules/@types/react/index.d.ts:2544

___

### cy

• `Optional` **cy**: `string` \| `number`

#### Inherited from

React.SVGProps.cy

#### Defined in

node_modules/@types/react/index.d.ts:2545

___

### d

• `Optional` **d**: `string`

#### Inherited from

React.SVGProps.d

#### Defined in

node_modules/@types/react/index.d.ts:2546

___

### dangerouslySetInnerHTML

• `Optional` **dangerouslySetInnerHTML**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `__html` | `string` |

#### Inherited from

React.SVGProps.dangerouslySetInnerHTML

#### Defined in

node_modules/@types/react/index.d.ts:1360

___

### decelerate

• `Optional` **decelerate**: `string` \| `number`

#### Inherited from

React.SVGProps.decelerate

#### Defined in

node_modules/@types/react/index.d.ts:2547

___

### descent

• `Optional` **descent**: `string` \| `number`

#### Inherited from

React.SVGProps.descent

#### Defined in

node_modules/@types/react/index.d.ts:2548

___

### diffuseConstant

• `Optional` **diffuseConstant**: `string` \| `number`

#### Inherited from

React.SVGProps.diffuseConstant

#### Defined in

node_modules/@types/react/index.d.ts:2549

___

### direction

• `Optional` **direction**: `string` \| `number`

#### Inherited from

React.SVGProps.direction

#### Defined in

node_modules/@types/react/index.d.ts:2550

___

### display

• `Optional` **display**: `string` \| `number`

#### Inherited from

React.SVGProps.display

#### Defined in

node_modules/@types/react/index.d.ts:2551

___

### divisor

• `Optional` **divisor**: `string` \| `number`

#### Inherited from

React.SVGProps.divisor

#### Defined in

node_modules/@types/react/index.d.ts:2552

___

### dominantBaseline

• `Optional` **dominantBaseline**: `string` \| `number`

#### Inherited from

React.SVGProps.dominantBaseline

#### Defined in

node_modules/@types/react/index.d.ts:2553

___

### dur

• `Optional` **dur**: `string` \| `number`

#### Inherited from

React.SVGProps.dur

#### Defined in

node_modules/@types/react/index.d.ts:2554

___

### dx

• `Optional` **dx**: `string` \| `number`

#### Inherited from

React.SVGProps.dx

#### Defined in

node_modules/@types/react/index.d.ts:2555

___

### dy

• `Optional` **dy**: `string` \| `number`

#### Inherited from

React.SVGProps.dy

#### Defined in

node_modules/@types/react/index.d.ts:2556

___

### edgeMode

• `Optional` **edgeMode**: `string` \| `number`

#### Inherited from

React.SVGProps.edgeMode

#### Defined in

node_modules/@types/react/index.d.ts:2557

___

### elevation

• `Optional` **elevation**: `string` \| `number`

#### Inherited from

React.SVGProps.elevation

#### Defined in

node_modules/@types/react/index.d.ts:2558

___

### enableBackground

• `Optional` **enableBackground**: `string` \| `number`

#### Inherited from

React.SVGProps.enableBackground

#### Defined in

node_modules/@types/react/index.d.ts:2559

___

### end

• `Optional` **end**: `string` \| `number`

#### Inherited from

React.SVGProps.end

#### Defined in

node_modules/@types/react/index.d.ts:2560

___

### exponent

• `Optional` **exponent**: `string` \| `number`

#### Inherited from

React.SVGProps.exponent

#### Defined in

node_modules/@types/react/index.d.ts:2561

___

### externalResourcesRequired

• `Optional` **externalResourcesRequired**: `Booleanish`

#### Inherited from

React.SVGProps.externalResourcesRequired

#### Defined in

node_modules/@types/react/index.d.ts:2562

___

### fill

• `Optional` **fill**: `string`

#### Inherited from

React.SVGProps.fill

#### Defined in

node_modules/@types/react/index.d.ts:2563

___

### fillOpacity

• `Optional` **fillOpacity**: `string` \| `number`

#### Inherited from

React.SVGProps.fillOpacity

#### Defined in

node_modules/@types/react/index.d.ts:2564

___

### fillRule

• `Optional` **fillRule**: ``"inherit"`` \| ``"nonzero"`` \| ``"evenodd"``

#### Inherited from

React.SVGProps.fillRule

#### Defined in

node_modules/@types/react/index.d.ts:2565

___

### filter

• `Optional` **filter**: `string`

#### Inherited from

React.SVGProps.filter

#### Defined in

node_modules/@types/react/index.d.ts:2566

___

### filterRes

• `Optional` **filterRes**: `string` \| `number`

#### Inherited from

React.SVGProps.filterRes

#### Defined in

node_modules/@types/react/index.d.ts:2567

___

### filterUnits

• `Optional` **filterUnits**: `string` \| `number`

#### Inherited from

React.SVGProps.filterUnits

#### Defined in

node_modules/@types/react/index.d.ts:2568

___

### floodColor

• `Optional` **floodColor**: `string` \| `number`

#### Inherited from

React.SVGProps.floodColor

#### Defined in

node_modules/@types/react/index.d.ts:2569

___

### floodOpacity

• `Optional` **floodOpacity**: `string` \| `number`

#### Inherited from

React.SVGProps.floodOpacity

#### Defined in

node_modules/@types/react/index.d.ts:2570

___

### focusable

• `Optional` **focusable**: ``"auto"`` \| `Booleanish`

#### Inherited from

React.SVGProps.focusable

#### Defined in

node_modules/@types/react/index.d.ts:2571

___

### fontFamily

• `Optional` **fontFamily**: `string`

#### Inherited from

React.SVGProps.fontFamily

#### Defined in

node_modules/@types/react/index.d.ts:2572

___

### fontSize

• `Optional` **fontSize**: `string` \| `number`

#### Inherited from

React.SVGProps.fontSize

#### Defined in

node_modules/@types/react/index.d.ts:2573

___

### fontSizeAdjust

• `Optional` **fontSizeAdjust**: `string` \| `number`

#### Inherited from

React.SVGProps.fontSizeAdjust

#### Defined in

node_modules/@types/react/index.d.ts:2574

___

### fontStretch

• `Optional` **fontStretch**: `string` \| `number`

#### Inherited from

React.SVGProps.fontStretch

#### Defined in

node_modules/@types/react/index.d.ts:2575

___

### fontStyle

• `Optional` **fontStyle**: `string` \| `number`

#### Inherited from

React.SVGProps.fontStyle

#### Defined in

node_modules/@types/react/index.d.ts:2576

___

### fontVariant

• `Optional` **fontVariant**: `string` \| `number`

#### Inherited from

React.SVGProps.fontVariant

#### Defined in

node_modules/@types/react/index.d.ts:2577

___

### fontWeight

• `Optional` **fontWeight**: `string` \| `number`

#### Inherited from

React.SVGProps.fontWeight

#### Defined in

node_modules/@types/react/index.d.ts:2578

___

### format

• `Optional` **format**: `string` \| `number`

#### Inherited from

React.SVGProps.format

#### Defined in

node_modules/@types/react/index.d.ts:2579

___

### fr

• `Optional` **fr**: `string` \| `number`

#### Inherited from

React.SVGProps.fr

#### Defined in

node_modules/@types/react/index.d.ts:2580

___

### from

• `Optional` **from**: `string` \| `number`

#### Inherited from

React.SVGProps.from

#### Defined in

node_modules/@types/react/index.d.ts:2581

___

### fx

• `Optional` **fx**: `string` \| `number`

#### Inherited from

React.SVGProps.fx

#### Defined in

node_modules/@types/react/index.d.ts:2582

___

### fy

• `Optional` **fy**: `string` \| `number`

#### Inherited from

React.SVGProps.fy

#### Defined in

node_modules/@types/react/index.d.ts:2583

___

### g1

• `Optional` **g1**: `string` \| `number`

#### Inherited from

React.SVGProps.g1

#### Defined in

node_modules/@types/react/index.d.ts:2584

___

### g2

• `Optional` **g2**: `string` \| `number`

#### Inherited from

React.SVGProps.g2

#### Defined in

node_modules/@types/react/index.d.ts:2585

___

### geo

• **geo**: `Feature`<`Geometry`, `GeoJsonProperties`\>

#### Defined in

[src/components/globe/cartography/types.ts:17](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/types.ts#L17)

___

### glyphName

• `Optional` **glyphName**: `string` \| `number`

#### Inherited from

React.SVGProps.glyphName

#### Defined in

node_modules/@types/react/index.d.ts:2586

___

### glyphOrientationHorizontal

• `Optional` **glyphOrientationHorizontal**: `string` \| `number`

#### Inherited from

React.SVGProps.glyphOrientationHorizontal

#### Defined in

node_modules/@types/react/index.d.ts:2587

___

### glyphOrientationVertical

• `Optional` **glyphOrientationVertical**: `string` \| `number`

#### Inherited from

React.SVGProps.glyphOrientationVertical

#### Defined in

node_modules/@types/react/index.d.ts:2588

___

### glyphRef

• `Optional` **glyphRef**: `string` \| `number`

#### Inherited from

React.SVGProps.glyphRef

#### Defined in

node_modules/@types/react/index.d.ts:2589

___

### gradientTransform

• `Optional` **gradientTransform**: `string`

#### Inherited from

React.SVGProps.gradientTransform

#### Defined in

node_modules/@types/react/index.d.ts:2590

___

### gradientUnits

• `Optional` **gradientUnits**: `string`

#### Inherited from

React.SVGProps.gradientUnits

#### Defined in

node_modules/@types/react/index.d.ts:2591

___

### hanging

• `Optional` **hanging**: `string` \| `number`

#### Inherited from

React.SVGProps.hanging

#### Defined in

node_modules/@types/react/index.d.ts:2592

___

### height

• `Optional` **height**: `string` \| `number`

#### Inherited from

React.SVGProps.height

#### Defined in

node_modules/@types/react/index.d.ts:2491

___

### horizAdvX

• `Optional` **horizAdvX**: `string` \| `number`

#### Inherited from

React.SVGProps.horizAdvX

#### Defined in

node_modules/@types/react/index.d.ts:2593

___

### horizOriginX

• `Optional` **horizOriginX**: `string` \| `number`

#### Inherited from

React.SVGProps.horizOriginX

#### Defined in

node_modules/@types/react/index.d.ts:2594

___

### hover

• `Optional` **hover**: [`HandleHoverEventProps`](Globe_Events_Types.HandleHoverEventProps.md)<`Function`, `Record`<`string`, `any`\>\>

#### Inherited from

Partial.hover

#### Defined in

[src/components/globe/events/types.ts:32](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L32)

___

### href

• `Optional` **href**: `string`

#### Inherited from

React.SVGProps.href

#### Defined in

node_modules/@types/react/index.d.ts:2595

___

### id

• `Optional` **id**: `string`

#### Inherited from

React.SVGProps.id

#### Defined in

node_modules/@types/react/index.d.ts:2492

___

### ideographic

• `Optional` **ideographic**: `string` \| `number`

#### Inherited from

React.SVGProps.ideographic

#### Defined in

node_modules/@types/react/index.d.ts:2596

___

### imageRendering

• `Optional` **imageRendering**: `string` \| `number`

#### Inherited from

React.SVGProps.imageRendering

#### Defined in

node_modules/@types/react/index.d.ts:2597

___

### in

• `Optional` **in**: `string`

#### Inherited from

React.SVGProps.in

#### Defined in

node_modules/@types/react/index.d.ts:2599

___

### in2

• `Optional` **in2**: `string` \| `number`

#### Inherited from

React.SVGProps.in2

#### Defined in

node_modules/@types/react/index.d.ts:2598

___

### intercept

• `Optional` **intercept**: `string` \| `number`

#### Inherited from

React.SVGProps.intercept

#### Defined in

node_modules/@types/react/index.d.ts:2600

___

### k

• `Optional` **k**: `string` \| `number`

#### Inherited from

React.SVGProps.k

#### Defined in

node_modules/@types/react/index.d.ts:2605

___

### k1

• `Optional` **k1**: `string` \| `number`

#### Inherited from

React.SVGProps.k1

#### Defined in

node_modules/@types/react/index.d.ts:2601

___

### k2

• `Optional` **k2**: `string` \| `number`

#### Inherited from

React.SVGProps.k2

#### Defined in

node_modules/@types/react/index.d.ts:2602

___

### k3

• `Optional` **k3**: `string` \| `number`

#### Inherited from

React.SVGProps.k3

#### Defined in

node_modules/@types/react/index.d.ts:2603

___

### k4

• `Optional` **k4**: `string` \| `number`

#### Inherited from

React.SVGProps.k4

#### Defined in

node_modules/@types/react/index.d.ts:2604

___

### kernelMatrix

• `Optional` **kernelMatrix**: `string` \| `number`

#### Inherited from

React.SVGProps.kernelMatrix

#### Defined in

node_modules/@types/react/index.d.ts:2606

___

### kernelUnitLength

• `Optional` **kernelUnitLength**: `string` \| `number`

#### Inherited from

React.SVGProps.kernelUnitLength

#### Defined in

node_modules/@types/react/index.d.ts:2607

___

### kerning

• `Optional` **kerning**: `string` \| `number`

#### Inherited from

React.SVGProps.kerning

#### Defined in

node_modules/@types/react/index.d.ts:2608

___

### key

• `Optional` **key**: ``null`` \| `Key`

#### Inherited from

React.SVGProps.key

#### Defined in

node_modules/@types/react/index.d.ts:137

___

### keyPoints

• `Optional` **keyPoints**: `string` \| `number`

#### Inherited from

React.SVGProps.keyPoints

#### Defined in

node_modules/@types/react/index.d.ts:2609

___

### keySplines

• `Optional` **keySplines**: `string` \| `number`

#### Inherited from

React.SVGProps.keySplines

#### Defined in

node_modules/@types/react/index.d.ts:2610

___

### keyTimes

• `Optional` **keyTimes**: `string` \| `number`

#### Inherited from

React.SVGProps.keyTimes

#### Defined in

node_modules/@types/react/index.d.ts:2611

___

### lang

• `Optional` **lang**: `string`

#### Inherited from

React.SVGProps.lang

#### Defined in

node_modules/@types/react/index.d.ts:2493

___

### lengthAdjust

• `Optional` **lengthAdjust**: `string` \| `number`

#### Inherited from

React.SVGProps.lengthAdjust

#### Defined in

node_modules/@types/react/index.d.ts:2612

___

### letterSpacing

• `Optional` **letterSpacing**: `string` \| `number`

#### Inherited from

React.SVGProps.letterSpacing

#### Defined in

node_modules/@types/react/index.d.ts:2613

___

### lightingColor

• `Optional` **lightingColor**: `string` \| `number`

#### Inherited from

React.SVGProps.lightingColor

#### Defined in

node_modules/@types/react/index.d.ts:2614

___

### limitingConeAngle

• `Optional` **limitingConeAngle**: `string` \| `number`

#### Inherited from

React.SVGProps.limitingConeAngle

#### Defined in

node_modules/@types/react/index.d.ts:2615

___

### local

• `Optional` **local**: `string` \| `number`

#### Inherited from

React.SVGProps.local

#### Defined in

node_modules/@types/react/index.d.ts:2616

___

### markerEnd

• `Optional` **markerEnd**: `string`

#### Inherited from

React.SVGProps.markerEnd

#### Defined in

node_modules/@types/react/index.d.ts:2617

___

### markerHeight

• `Optional` **markerHeight**: `string` \| `number`

#### Inherited from

React.SVGProps.markerHeight

#### Defined in

node_modules/@types/react/index.d.ts:2618

___

### markerMid

• `Optional` **markerMid**: `string`

#### Inherited from

React.SVGProps.markerMid

#### Defined in

node_modules/@types/react/index.d.ts:2619

___

### markerStart

• `Optional` **markerStart**: `string`

#### Inherited from

React.SVGProps.markerStart

#### Defined in

node_modules/@types/react/index.d.ts:2620

___

### markerUnits

• `Optional` **markerUnits**: `string` \| `number`

#### Inherited from

React.SVGProps.markerUnits

#### Defined in

node_modules/@types/react/index.d.ts:2621

___

### markerWidth

• `Optional` **markerWidth**: `string` \| `number`

#### Inherited from

React.SVGProps.markerWidth

#### Defined in

node_modules/@types/react/index.d.ts:2622

___

### mask

• `Optional` **mask**: `string`

#### Inherited from

React.SVGProps.mask

#### Defined in

node_modules/@types/react/index.d.ts:2623

___

### maskContentUnits

• `Optional` **maskContentUnits**: `string` \| `number`

#### Inherited from

React.SVGProps.maskContentUnits

#### Defined in

node_modules/@types/react/index.d.ts:2624

___

### maskUnits

• `Optional` **maskUnits**: `string` \| `number`

#### Inherited from

React.SVGProps.maskUnits

#### Defined in

node_modules/@types/react/index.d.ts:2625

___

### mathematical

• `Optional` **mathematical**: `string` \| `number`

#### Inherited from

React.SVGProps.mathematical

#### Defined in

node_modules/@types/react/index.d.ts:2626

___

### max

• `Optional` **max**: `string` \| `number`

#### Inherited from

React.SVGProps.max

#### Defined in

node_modules/@types/react/index.d.ts:2494

___

### media

• `Optional` **media**: `string`

#### Inherited from

React.SVGProps.media

#### Defined in

node_modules/@types/react/index.d.ts:2495

___

### method

• `Optional` **method**: `string`

#### Inherited from

React.SVGProps.method

#### Defined in

node_modules/@types/react/index.d.ts:2496

___

### min

• `Optional` **min**: `string` \| `number`

#### Inherited from

React.SVGProps.min

#### Defined in

node_modules/@types/react/index.d.ts:2497

___

### mode

• `Optional` **mode**: `string` \| `number`

#### Inherited from

React.SVGProps.mode

#### Defined in

node_modules/@types/react/index.d.ts:2627

___

### name

• `Optional` **name**: `string`

#### Inherited from

React.SVGProps.name

#### Defined in

node_modules/@types/react/index.d.ts:2498

___

### numOctaves

• `Optional` **numOctaves**: `string` \| `number`

#### Inherited from

React.SVGProps.numOctaves

#### Defined in

node_modules/@types/react/index.d.ts:2628

___

### offset

• `Optional` **offset**: `string` \| `number`

#### Inherited from

React.SVGProps.offset

#### Defined in

node_modules/@types/react/index.d.ts:2629

___

### onAbort

• `Optional` **onAbort**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAbort

#### Defined in

node_modules/@types/react/index.d.ts:1415

___

### onAbortCapture

• `Optional` **onAbortCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAbortCapture

#### Defined in

node_modules/@types/react/index.d.ts:1416

___

### onAnimationEnd

• `Optional` **onAnimationEnd**: `AnimationEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAnimationEnd

#### Defined in

node_modules/@types/react/index.d.ts:1545

___

### onAnimationEndCapture

• `Optional` **onAnimationEndCapture**: `AnimationEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAnimationEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1546

___

### onAnimationIteration

• `Optional` **onAnimationIteration**: `AnimationEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAnimationIteration

#### Defined in

node_modules/@types/react/index.d.ts:1547

___

### onAnimationIterationCapture

• `Optional` **onAnimationIterationCapture**: `AnimationEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAnimationIterationCapture

#### Defined in

node_modules/@types/react/index.d.ts:1548

___

### onAnimationStart

• `Optional` **onAnimationStart**: `AnimationEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAnimationStart

#### Defined in

node_modules/@types/react/index.d.ts:1543

___

### onAnimationStartCapture

• `Optional` **onAnimationStartCapture**: `AnimationEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAnimationStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1544

___

### onAuxClick

• `Optional` **onAuxClick**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAuxClick

#### Defined in

node_modules/@types/react/index.d.ts:1461

___

### onAuxClickCapture

• `Optional` **onAuxClickCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onAuxClickCapture

#### Defined in

node_modules/@types/react/index.d.ts:1462

___

### onBeforeInput

• `Optional` **onBeforeInput**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onBeforeInput

#### Defined in

node_modules/@types/react/index.d.ts:1389

___

### onBeforeInputCapture

• `Optional` **onBeforeInputCapture**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onBeforeInputCapture

#### Defined in

node_modules/@types/react/index.d.ts:1390

___

### onBlur

• `Optional` **onBlur**: `FocusEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onBlur

#### Defined in

node_modules/@types/react/index.d.ts:1383

___

### onBlurCapture

• `Optional` **onBlurCapture**: `FocusEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onBlurCapture

#### Defined in

node_modules/@types/react/index.d.ts:1384

___

### onCanPlay

• `Optional` **onCanPlay**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCanPlay

#### Defined in

node_modules/@types/react/index.d.ts:1417

___

### onCanPlayCapture

• `Optional` **onCanPlayCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCanPlayCapture

#### Defined in

node_modules/@types/react/index.d.ts:1418

___

### onCanPlayThrough

• `Optional` **onCanPlayThrough**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCanPlayThrough

#### Defined in

node_modules/@types/react/index.d.ts:1419

___

### onCanPlayThroughCapture

• `Optional` **onCanPlayThroughCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCanPlayThroughCapture

#### Defined in

node_modules/@types/react/index.d.ts:1420

___

### onChange

• `Optional` **onChange**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onChange

#### Defined in

node_modules/@types/react/index.d.ts:1387

___

### onChangeCapture

• `Optional` **onChangeCapture**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onChangeCapture

#### Defined in

node_modules/@types/react/index.d.ts:1388

___

### onClick

• `Optional` **onClick**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onClick

#### Defined in

node_modules/@types/react/index.d.ts:1463

___

### onClickCapture

• `Optional` **onClickCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onClickCapture

#### Defined in

node_modules/@types/react/index.d.ts:1464

___

### onCompositionEnd

• `Optional` **onCompositionEnd**: `CompositionEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCompositionEnd

#### Defined in

node_modules/@types/react/index.d.ts:1373

___

### onCompositionEndCapture

• `Optional` **onCompositionEndCapture**: `CompositionEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCompositionEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1374

___

### onCompositionStart

• `Optional` **onCompositionStart**: `CompositionEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCompositionStart

#### Defined in

node_modules/@types/react/index.d.ts:1375

___

### onCompositionStartCapture

• `Optional` **onCompositionStartCapture**: `CompositionEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCompositionStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1376

___

### onCompositionUpdate

• `Optional` **onCompositionUpdate**: `CompositionEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCompositionUpdate

#### Defined in

node_modules/@types/react/index.d.ts:1377

___

### onCompositionUpdateCapture

• `Optional` **onCompositionUpdateCapture**: `CompositionEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCompositionUpdateCapture

#### Defined in

node_modules/@types/react/index.d.ts:1378

___

### onContextMenu

• `Optional` **onContextMenu**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onContextMenu

#### Defined in

node_modules/@types/react/index.d.ts:1465

___

### onContextMenuCapture

• `Optional` **onContextMenuCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onContextMenuCapture

#### Defined in

node_modules/@types/react/index.d.ts:1466

___

### onCopy

• `Optional` **onCopy**: `ClipboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCopy

#### Defined in

node_modules/@types/react/index.d.ts:1365

___

### onCopyCapture

• `Optional` **onCopyCapture**: `ClipboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCopyCapture

#### Defined in

node_modules/@types/react/index.d.ts:1366

___

### onCut

• `Optional` **onCut**: `ClipboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCut

#### Defined in

node_modules/@types/react/index.d.ts:1367

___

### onCutCapture

• `Optional` **onCutCapture**: `ClipboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onCutCapture

#### Defined in

node_modules/@types/react/index.d.ts:1368

___

### onDoubleClick

• `Optional` **onDoubleClick**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDoubleClick

#### Defined in

node_modules/@types/react/index.d.ts:1467

___

### onDoubleClickCapture

• `Optional` **onDoubleClickCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDoubleClickCapture

#### Defined in

node_modules/@types/react/index.d.ts:1468

___

### onDrag

• `Optional` **onDrag**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDrag

#### Defined in

node_modules/@types/react/index.d.ts:1469

___

### onDragCapture

• `Optional` **onDragCapture**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragCapture

#### Defined in

node_modules/@types/react/index.d.ts:1470

___

### onDragEnd

• `Optional` **onDragEnd**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragEnd

#### Defined in

node_modules/@types/react/index.d.ts:1471

___

### onDragEndCapture

• `Optional` **onDragEndCapture**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1472

___

### onDragEnter

• `Optional` **onDragEnter**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragEnter

#### Defined in

node_modules/@types/react/index.d.ts:1473

___

### onDragEnterCapture

• `Optional` **onDragEnterCapture**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragEnterCapture

#### Defined in

node_modules/@types/react/index.d.ts:1474

___

### onDragExit

• `Optional` **onDragExit**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragExit

#### Defined in

node_modules/@types/react/index.d.ts:1475

___

### onDragExitCapture

• `Optional` **onDragExitCapture**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragExitCapture

#### Defined in

node_modules/@types/react/index.d.ts:1476

___

### onDragLeave

• `Optional` **onDragLeave**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragLeave

#### Defined in

node_modules/@types/react/index.d.ts:1477

___

### onDragLeaveCapture

• `Optional` **onDragLeaveCapture**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragLeaveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1478

___

### onDragOver

• `Optional` **onDragOver**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragOver

#### Defined in

node_modules/@types/react/index.d.ts:1479

___

### onDragOverCapture

• `Optional` **onDragOverCapture**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragOverCapture

#### Defined in

node_modules/@types/react/index.d.ts:1480

___

### onDragStart

• `Optional` **onDragStart**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragStart

#### Defined in

node_modules/@types/react/index.d.ts:1481

___

### onDragStartCapture

• `Optional` **onDragStartCapture**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDragStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1482

___

### onDrop

• `Optional` **onDrop**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDrop

#### Defined in

node_modules/@types/react/index.d.ts:1483

___

### onDropCapture

• `Optional` **onDropCapture**: `DragEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDropCapture

#### Defined in

node_modules/@types/react/index.d.ts:1484

___

### onDurationChange

• `Optional` **onDurationChange**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDurationChange

#### Defined in

node_modules/@types/react/index.d.ts:1421

___

### onDurationChangeCapture

• `Optional` **onDurationChangeCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onDurationChangeCapture

#### Defined in

node_modules/@types/react/index.d.ts:1422

___

### onEmptied

• `Optional` **onEmptied**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onEmptied

#### Defined in

node_modules/@types/react/index.d.ts:1423

___

### onEmptiedCapture

• `Optional` **onEmptiedCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onEmptiedCapture

#### Defined in

node_modules/@types/react/index.d.ts:1424

___

### onEncrypted

• `Optional` **onEncrypted**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onEncrypted

#### Defined in

node_modules/@types/react/index.d.ts:1425

___

### onEncryptedCapture

• `Optional` **onEncryptedCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onEncryptedCapture

#### Defined in

node_modules/@types/react/index.d.ts:1426

___

### onEnded

• `Optional` **onEnded**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onEnded

#### Defined in

node_modules/@types/react/index.d.ts:1427

___

### onEndedCapture

• `Optional` **onEndedCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onEndedCapture

#### Defined in

node_modules/@types/react/index.d.ts:1428

___

### onError

• `Optional` **onError**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onError

#### Defined in

node_modules/@types/react/index.d.ts:1403

___

### onErrorCapture

• `Optional` **onErrorCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onErrorCapture

#### Defined in

node_modules/@types/react/index.d.ts:1404

___

### onFocus

• `Optional` **onFocus**: `FocusEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onFocus

#### Defined in

node_modules/@types/react/index.d.ts:1381

___

### onFocusCapture

• `Optional` **onFocusCapture**: `FocusEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onFocusCapture

#### Defined in

node_modules/@types/react/index.d.ts:1382

___

### onGotPointerCapture

• `Optional` **onGotPointerCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onGotPointerCapture

#### Defined in

node_modules/@types/react/index.d.ts:1529

___

### onGotPointerCaptureCapture

• `Optional` **onGotPointerCaptureCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onGotPointerCaptureCapture

#### Defined in

node_modules/@types/react/index.d.ts:1530

___

### onInput

• `Optional` **onInput**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onInput

#### Defined in

node_modules/@types/react/index.d.ts:1391

___

### onInputCapture

• `Optional` **onInputCapture**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onInputCapture

#### Defined in

node_modules/@types/react/index.d.ts:1392

___

### onInvalid

• `Optional` **onInvalid**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onInvalid

#### Defined in

node_modules/@types/react/index.d.ts:1397

___

### onInvalidCapture

• `Optional` **onInvalidCapture**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onInvalidCapture

#### Defined in

node_modules/@types/react/index.d.ts:1398

___

### onKeyDown

• `Optional` **onKeyDown**: `KeyboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onKeyDown

#### Defined in

node_modules/@types/react/index.d.ts:1407

___

### onKeyDownCapture

• `Optional` **onKeyDownCapture**: `KeyboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onKeyDownCapture

#### Defined in

node_modules/@types/react/index.d.ts:1408

___

### onKeyPress

• `Optional` **onKeyPress**: `KeyboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onKeyPress

#### Defined in

node_modules/@types/react/index.d.ts:1409

___

### onKeyPressCapture

• `Optional` **onKeyPressCapture**: `KeyboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onKeyPressCapture

#### Defined in

node_modules/@types/react/index.d.ts:1410

___

### onKeyUp

• `Optional` **onKeyUp**: `KeyboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onKeyUp

#### Defined in

node_modules/@types/react/index.d.ts:1411

___

### onKeyUpCapture

• `Optional` **onKeyUpCapture**: `KeyboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onKeyUpCapture

#### Defined in

node_modules/@types/react/index.d.ts:1412

___

### onLoad

• `Optional` **onLoad**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLoad

#### Defined in

node_modules/@types/react/index.d.ts:1401

___

### onLoadCapture

• `Optional` **onLoadCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLoadCapture

#### Defined in

node_modules/@types/react/index.d.ts:1402

___

### onLoadStart

• `Optional` **onLoadStart**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLoadStart

#### Defined in

node_modules/@types/react/index.d.ts:1433

___

### onLoadStartCapture

• `Optional` **onLoadStartCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLoadStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1434

___

### onLoadedData

• `Optional` **onLoadedData**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLoadedData

#### Defined in

node_modules/@types/react/index.d.ts:1429

___

### onLoadedDataCapture

• `Optional` **onLoadedDataCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLoadedDataCapture

#### Defined in

node_modules/@types/react/index.d.ts:1430

___

### onLoadedMetadata

• `Optional` **onLoadedMetadata**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLoadedMetadata

#### Defined in

node_modules/@types/react/index.d.ts:1431

___

### onLoadedMetadataCapture

• `Optional` **onLoadedMetadataCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLoadedMetadataCapture

#### Defined in

node_modules/@types/react/index.d.ts:1432

___

### onLostPointerCapture

• `Optional` **onLostPointerCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLostPointerCapture

#### Defined in

node_modules/@types/react/index.d.ts:1531

___

### onLostPointerCaptureCapture

• `Optional` **onLostPointerCaptureCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onLostPointerCaptureCapture

#### Defined in

node_modules/@types/react/index.d.ts:1532

___

### onMouseDown

• `Optional` **onMouseDown**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseDown

#### Defined in

node_modules/@types/react/index.d.ts:1485

___

### onMouseDownCapture

• `Optional` **onMouseDownCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseDownCapture

#### Defined in

node_modules/@types/react/index.d.ts:1486

___

### onMouseEnter

• `Optional` **onMouseEnter**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseEnter

#### Defined in

node_modules/@types/react/index.d.ts:1487

___

### onMouseLeave

• `Optional` **onMouseLeave**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseLeave

#### Defined in

node_modules/@types/react/index.d.ts:1488

___

### onMouseMove

• `Optional` **onMouseMove**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseMove

#### Defined in

node_modules/@types/react/index.d.ts:1489

___

### onMouseMoveCapture

• `Optional` **onMouseMoveCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseMoveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1490

___

### onMouseOut

• `Optional` **onMouseOut**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseOut

#### Defined in

node_modules/@types/react/index.d.ts:1491

___

### onMouseOutCapture

• `Optional` **onMouseOutCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseOutCapture

#### Defined in

node_modules/@types/react/index.d.ts:1492

___

### onMouseOver

• `Optional` **onMouseOver**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseOver

#### Defined in

node_modules/@types/react/index.d.ts:1493

___

### onMouseOverCapture

• `Optional` **onMouseOverCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseOverCapture

#### Defined in

node_modules/@types/react/index.d.ts:1494

___

### onMouseUp

• `Optional` **onMouseUp**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseUp

#### Defined in

node_modules/@types/react/index.d.ts:1495

___

### onMouseUpCapture

• `Optional` **onMouseUpCapture**: `MouseEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onMouseUpCapture

#### Defined in

node_modules/@types/react/index.d.ts:1496

___

### onPaste

• `Optional` **onPaste**: `ClipboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPaste

#### Defined in

node_modules/@types/react/index.d.ts:1369

___

### onPasteCapture

• `Optional` **onPasteCapture**: `ClipboardEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPasteCapture

#### Defined in

node_modules/@types/react/index.d.ts:1370

___

### onPause

• `Optional` **onPause**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPause

#### Defined in

node_modules/@types/react/index.d.ts:1435

___

### onPauseCapture

• `Optional` **onPauseCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPauseCapture

#### Defined in

node_modules/@types/react/index.d.ts:1436

___

### onPlay

• `Optional` **onPlay**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPlay

#### Defined in

node_modules/@types/react/index.d.ts:1437

___

### onPlayCapture

• `Optional` **onPlayCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPlayCapture

#### Defined in

node_modules/@types/react/index.d.ts:1438

___

### onPlaying

• `Optional` **onPlaying**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPlaying

#### Defined in

node_modules/@types/react/index.d.ts:1439

___

### onPlayingCapture

• `Optional` **onPlayingCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPlayingCapture

#### Defined in

node_modules/@types/react/index.d.ts:1440

___

### onPointerCancel

• `Optional` **onPointerCancel**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerCancel

#### Defined in

node_modules/@types/react/index.d.ts:1519

___

### onPointerCancelCapture

• `Optional` **onPointerCancelCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerCancelCapture

#### Defined in

node_modules/@types/react/index.d.ts:1520

___

### onPointerDown

• `Optional` **onPointerDown**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerDown

#### Defined in

node_modules/@types/react/index.d.ts:1513

___

### onPointerDownCapture

• `Optional` **onPointerDownCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerDownCapture

#### Defined in

node_modules/@types/react/index.d.ts:1514

___

### onPointerEnter

• `Optional` **onPointerEnter**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerEnter

#### Defined in

node_modules/@types/react/index.d.ts:1521

___

### onPointerEnterCapture

• `Optional` **onPointerEnterCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerEnterCapture

#### Defined in

node_modules/@types/react/index.d.ts:1522

___

### onPointerLeave

• `Optional` **onPointerLeave**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerLeave

#### Defined in

node_modules/@types/react/index.d.ts:1523

___

### onPointerLeaveCapture

• `Optional` **onPointerLeaveCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerLeaveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1524

___

### onPointerMove

• `Optional` **onPointerMove**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerMove

#### Defined in

node_modules/@types/react/index.d.ts:1515

___

### onPointerMoveCapture

• `Optional` **onPointerMoveCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerMoveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1516

___

### onPointerOut

• `Optional` **onPointerOut**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerOut

#### Defined in

node_modules/@types/react/index.d.ts:1527

___

### onPointerOutCapture

• `Optional` **onPointerOutCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerOutCapture

#### Defined in

node_modules/@types/react/index.d.ts:1528

___

### onPointerOver

• `Optional` **onPointerOver**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerOver

#### Defined in

node_modules/@types/react/index.d.ts:1525

___

### onPointerOverCapture

• `Optional` **onPointerOverCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerOverCapture

#### Defined in

node_modules/@types/react/index.d.ts:1526

___

### onPointerUp

• `Optional` **onPointerUp**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerUp

#### Defined in

node_modules/@types/react/index.d.ts:1517

___

### onPointerUpCapture

• `Optional` **onPointerUpCapture**: `PointerEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onPointerUpCapture

#### Defined in

node_modules/@types/react/index.d.ts:1518

___

### onProgress

• `Optional` **onProgress**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onProgress

#### Defined in

node_modules/@types/react/index.d.ts:1441

___

### onProgressCapture

• `Optional` **onProgressCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onProgressCapture

#### Defined in

node_modules/@types/react/index.d.ts:1442

___

### onRateChange

• `Optional` **onRateChange**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onRateChange

#### Defined in

node_modules/@types/react/index.d.ts:1443

___

### onRateChangeCapture

• `Optional` **onRateChangeCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onRateChangeCapture

#### Defined in

node_modules/@types/react/index.d.ts:1444

___

### onReset

• `Optional` **onReset**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onReset

#### Defined in

node_modules/@types/react/index.d.ts:1393

___

### onResetCapture

• `Optional` **onResetCapture**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onResetCapture

#### Defined in

node_modules/@types/react/index.d.ts:1394

___

### onScroll

• `Optional` **onScroll**: `UIEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onScroll

#### Defined in

node_modules/@types/react/index.d.ts:1535

___

### onScrollCapture

• `Optional` **onScrollCapture**: `UIEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onScrollCapture

#### Defined in

node_modules/@types/react/index.d.ts:1536

___

### onSeeked

• `Optional` **onSeeked**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSeeked

#### Defined in

node_modules/@types/react/index.d.ts:1445

___

### onSeekedCapture

• `Optional` **onSeekedCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSeekedCapture

#### Defined in

node_modules/@types/react/index.d.ts:1446

___

### onSeeking

• `Optional` **onSeeking**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSeeking

#### Defined in

node_modules/@types/react/index.d.ts:1447

___

### onSeekingCapture

• `Optional` **onSeekingCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSeekingCapture

#### Defined in

node_modules/@types/react/index.d.ts:1448

___

### onSelect

• `Optional` **onSelect**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSelect

#### Defined in

node_modules/@types/react/index.d.ts:1499

___

### onSelectCapture

• `Optional` **onSelectCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSelectCapture

#### Defined in

node_modules/@types/react/index.d.ts:1500

___

### onStalled

• `Optional` **onStalled**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onStalled

#### Defined in

node_modules/@types/react/index.d.ts:1449

___

### onStalledCapture

• `Optional` **onStalledCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onStalledCapture

#### Defined in

node_modules/@types/react/index.d.ts:1450

___

### onSubmit

• `Optional` **onSubmit**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSubmit

#### Defined in

node_modules/@types/react/index.d.ts:1395

___

### onSubmitCapture

• `Optional` **onSubmitCapture**: `FormEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSubmitCapture

#### Defined in

node_modules/@types/react/index.d.ts:1396

___

### onSuspend

• `Optional` **onSuspend**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSuspend

#### Defined in

node_modules/@types/react/index.d.ts:1451

___

### onSuspendCapture

• `Optional` **onSuspendCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onSuspendCapture

#### Defined in

node_modules/@types/react/index.d.ts:1452

___

### onTimeUpdate

• `Optional` **onTimeUpdate**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTimeUpdate

#### Defined in

node_modules/@types/react/index.d.ts:1453

___

### onTimeUpdateCapture

• `Optional` **onTimeUpdateCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTimeUpdateCapture

#### Defined in

node_modules/@types/react/index.d.ts:1454

___

### onTouchCancel

• `Optional` **onTouchCancel**: `TouchEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTouchCancel

#### Defined in

node_modules/@types/react/index.d.ts:1503

___

### onTouchCancelCapture

• `Optional` **onTouchCancelCapture**: `TouchEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTouchCancelCapture

#### Defined in

node_modules/@types/react/index.d.ts:1504

___

### onTouchEnd

• `Optional` **onTouchEnd**: `TouchEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTouchEnd

#### Defined in

node_modules/@types/react/index.d.ts:1505

___

### onTouchEndCapture

• `Optional` **onTouchEndCapture**: `TouchEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTouchEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1506

___

### onTouchMove

• `Optional` **onTouchMove**: `TouchEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTouchMove

#### Defined in

node_modules/@types/react/index.d.ts:1507

___

### onTouchMoveCapture

• `Optional` **onTouchMoveCapture**: `TouchEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTouchMoveCapture

#### Defined in

node_modules/@types/react/index.d.ts:1508

___

### onTouchStart

• `Optional` **onTouchStart**: `TouchEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTouchStart

#### Defined in

node_modules/@types/react/index.d.ts:1509

___

### onTouchStartCapture

• `Optional` **onTouchStartCapture**: `TouchEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTouchStartCapture

#### Defined in

node_modules/@types/react/index.d.ts:1510

___

### onTransitionEnd

• `Optional` **onTransitionEnd**: `TransitionEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTransitionEnd

#### Defined in

node_modules/@types/react/index.d.ts:1551

___

### onTransitionEndCapture

• `Optional` **onTransitionEndCapture**: `TransitionEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onTransitionEndCapture

#### Defined in

node_modules/@types/react/index.d.ts:1552

___

### onVolumeChange

• `Optional` **onVolumeChange**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onVolumeChange

#### Defined in

node_modules/@types/react/index.d.ts:1455

___

### onVolumeChangeCapture

• `Optional` **onVolumeChangeCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onVolumeChangeCapture

#### Defined in

node_modules/@types/react/index.d.ts:1456

___

### onWaiting

• `Optional` **onWaiting**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onWaiting

#### Defined in

node_modules/@types/react/index.d.ts:1457

___

### onWaitingCapture

• `Optional` **onWaitingCapture**: `ReactEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onWaitingCapture

#### Defined in

node_modules/@types/react/index.d.ts:1458

___

### onWheel

• `Optional` **onWheel**: `WheelEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onWheel

#### Defined in

node_modules/@types/react/index.d.ts:1539

___

### onWheelCapture

• `Optional` **onWheelCapture**: `WheelEventHandler`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.onWheelCapture

#### Defined in

node_modules/@types/react/index.d.ts:1540

___

### opacity

• `Optional` **opacity**: `string` \| `number`

#### Inherited from

React.SVGProps.opacity

#### Defined in

node_modules/@types/react/index.d.ts:2630

___

### operator

• `Optional` **operator**: `string` \| `number`

#### Inherited from

React.SVGProps.operator

#### Defined in

node_modules/@types/react/index.d.ts:2631

___

### order

• `Optional` **order**: `string` \| `number`

#### Inherited from

React.SVGProps.order

#### Defined in

node_modules/@types/react/index.d.ts:2632

___

### orient

• `Optional` **orient**: `string` \| `number`

#### Inherited from

React.SVGProps.orient

#### Defined in

node_modules/@types/react/index.d.ts:2633

___

### orientation

• `Optional` **orientation**: `string` \| `number`

#### Inherited from

React.SVGProps.orientation

#### Defined in

node_modules/@types/react/index.d.ts:2634

___

### origin

• `Optional` **origin**: `string` \| `number`

#### Inherited from

React.SVGProps.origin

#### Defined in

node_modules/@types/react/index.d.ts:2635

___

### overflow

• `Optional` **overflow**: `string` \| `number`

#### Inherited from

React.SVGProps.overflow

#### Defined in

node_modules/@types/react/index.d.ts:2636

___

### overlinePosition

• `Optional` **overlinePosition**: `string` \| `number`

#### Inherited from

React.SVGProps.overlinePosition

#### Defined in

node_modules/@types/react/index.d.ts:2637

___

### overlineThickness

• `Optional` **overlineThickness**: `string` \| `number`

#### Inherited from

React.SVGProps.overlineThickness

#### Defined in

node_modules/@types/react/index.d.ts:2638

___

### paintOrder

• `Optional` **paintOrder**: `string` \| `number`

#### Inherited from

React.SVGProps.paintOrder

#### Defined in

node_modules/@types/react/index.d.ts:2639

___

### panose1

• `Optional` **panose1**: `string` \| `number`

#### Inherited from

React.SVGProps.panose1

#### Defined in

node_modules/@types/react/index.d.ts:2640

___

### path

• `Optional` **path**: `string`

#### Inherited from

React.SVGProps.path

#### Defined in

node_modules/@types/react/index.d.ts:2641

___

### pathLength

• `Optional` **pathLength**: `string` \| `number`

#### Inherited from

React.SVGProps.pathLength

#### Defined in

node_modules/@types/react/index.d.ts:2642

___

### patternContentUnits

• `Optional` **patternContentUnits**: `string`

#### Inherited from

React.SVGProps.patternContentUnits

#### Defined in

node_modules/@types/react/index.d.ts:2643

___

### patternTransform

• `Optional` **patternTransform**: `string` \| `number`

#### Inherited from

React.SVGProps.patternTransform

#### Defined in

node_modules/@types/react/index.d.ts:2644

___

### patternUnits

• `Optional` **patternUnits**: `string`

#### Inherited from

React.SVGProps.patternUnits

#### Defined in

node_modules/@types/react/index.d.ts:2645

___

### pointerEvents

• `Optional` **pointerEvents**: `string` \| `number`

#### Inherited from

React.SVGProps.pointerEvents

#### Defined in

node_modules/@types/react/index.d.ts:2646

___

### points

• `Optional` **points**: `string`

#### Inherited from

React.SVGProps.points

#### Defined in

node_modules/@types/react/index.d.ts:2647

___

### pointsAtX

• `Optional` **pointsAtX**: `string` \| `number`

#### Inherited from

React.SVGProps.pointsAtX

#### Defined in

node_modules/@types/react/index.d.ts:2648

___

### pointsAtY

• `Optional` **pointsAtY**: `string` \| `number`

#### Inherited from

React.SVGProps.pointsAtY

#### Defined in

node_modules/@types/react/index.d.ts:2649

___

### pointsAtZ

• `Optional` **pointsAtZ**: `string` \| `number`

#### Inherited from

React.SVGProps.pointsAtZ

#### Defined in

node_modules/@types/react/index.d.ts:2650

___

### preserveAlpha

• `Optional` **preserveAlpha**: `Booleanish`

#### Inherited from

React.SVGProps.preserveAlpha

#### Defined in

node_modules/@types/react/index.d.ts:2651

___

### preserveAspectRatio

• `Optional` **preserveAspectRatio**: `string`

#### Inherited from

React.SVGProps.preserveAspectRatio

#### Defined in

node_modules/@types/react/index.d.ts:2652

___

### primitiveUnits

• `Optional` **primitiveUnits**: `string` \| `number`

#### Inherited from

React.SVGProps.primitiveUnits

#### Defined in

node_modules/@types/react/index.d.ts:2653

___

### r

• `Optional` **r**: `string` \| `number`

#### Inherited from

React.SVGProps.r

#### Defined in

node_modules/@types/react/index.d.ts:2654

___

### radius

• `Optional` **radius**: `string` \| `number`

#### Inherited from

React.SVGProps.radius

#### Defined in

node_modules/@types/react/index.d.ts:2655

___

### ref

• `Optional` **ref**: `LegacyRef`<`SVGPathElement`\>

#### Inherited from

React.SVGProps.ref

#### Defined in

node_modules/@types/react/index.d.ts:143

___

### refX

• `Optional` **refX**: `string` \| `number`

#### Inherited from

React.SVGProps.refX

#### Defined in

node_modules/@types/react/index.d.ts:2656

___

### refY

• `Optional` **refY**: `string` \| `number`

#### Inherited from

React.SVGProps.refY

#### Defined in

node_modules/@types/react/index.d.ts:2657

___

### renderingIntent

• `Optional` **renderingIntent**: `string` \| `number`

#### Inherited from

React.SVGProps.renderingIntent

#### Defined in

node_modules/@types/react/index.d.ts:2658

___

### repeatCount

• `Optional` **repeatCount**: `string` \| `number`

#### Inherited from

React.SVGProps.repeatCount

#### Defined in

node_modules/@types/react/index.d.ts:2659

___

### repeatDur

• `Optional` **repeatDur**: `string` \| `number`

#### Inherited from

React.SVGProps.repeatDur

#### Defined in

node_modules/@types/react/index.d.ts:2660

___

### requiredExtensions

• `Optional` **requiredExtensions**: `string` \| `number`

#### Inherited from

React.SVGProps.requiredExtensions

#### Defined in

node_modules/@types/react/index.d.ts:2661

___

### requiredFeatures

• `Optional` **requiredFeatures**: `string` \| `number`

#### Inherited from

React.SVGProps.requiredFeatures

#### Defined in

node_modules/@types/react/index.d.ts:2662

___

### restart

• `Optional` **restart**: `string` \| `number`

#### Inherited from

React.SVGProps.restart

#### Defined in

node_modules/@types/react/index.d.ts:2663

___

### result

• `Optional` **result**: `string`

#### Inherited from

React.SVGProps.result

#### Defined in

node_modules/@types/react/index.d.ts:2664

___

### role

• `Optional` **role**: `AriaRole`

#### Inherited from

React.SVGProps.role

#### Defined in

node_modules/@types/react/index.d.ts:2505

___

### rotate

• `Optional` **rotate**: `string` \| `number`

#### Inherited from

React.SVGProps.rotate

#### Defined in

node_modules/@types/react/index.d.ts:2665

___

### rx

• `Optional` **rx**: `string` \| `number`

#### Inherited from

React.SVGProps.rx

#### Defined in

node_modules/@types/react/index.d.ts:2666

___

### ry

• `Optional` **ry**: `string` \| `number`

#### Inherited from

React.SVGProps.ry

#### Defined in

node_modules/@types/react/index.d.ts:2667

___

### scale

• `Optional` **scale**: `string` \| `number`

#### Inherited from

React.SVGProps.scale

#### Defined in

node_modules/@types/react/index.d.ts:2668

___

### seed

• `Optional` **seed**: `string` \| `number`

#### Inherited from

React.SVGProps.seed

#### Defined in

node_modules/@types/react/index.d.ts:2669

___

### shapeRendering

• `Optional` **shapeRendering**: `string` \| `number`

#### Inherited from

React.SVGProps.shapeRendering

#### Defined in

node_modules/@types/react/index.d.ts:2670

___

### slope

• `Optional` **slope**: `string` \| `number`

#### Inherited from

React.SVGProps.slope

#### Defined in

node_modules/@types/react/index.d.ts:2671

___

### spacing

• `Optional` **spacing**: `string` \| `number`

#### Inherited from

React.SVGProps.spacing

#### Defined in

node_modules/@types/react/index.d.ts:2672

___

### specularConstant

• `Optional` **specularConstant**: `string` \| `number`

#### Inherited from

React.SVGProps.specularConstant

#### Defined in

node_modules/@types/react/index.d.ts:2673

___

### specularExponent

• `Optional` **specularExponent**: `string` \| `number`

#### Inherited from

React.SVGProps.specularExponent

#### Defined in

node_modules/@types/react/index.d.ts:2674

___

### speed

• `Optional` **speed**: `string` \| `number`

#### Inherited from

React.SVGProps.speed

#### Defined in

node_modules/@types/react/index.d.ts:2675

___

### spreadMethod

• `Optional` **spreadMethod**: `string`

#### Inherited from

React.SVGProps.spreadMethod

#### Defined in

node_modules/@types/react/index.d.ts:2676

___

### startOffset

• `Optional` **startOffset**: `string` \| `number`

#### Inherited from

React.SVGProps.startOffset

#### Defined in

node_modules/@types/react/index.d.ts:2677

___

### stdDeviation

• `Optional` **stdDeviation**: `string` \| `number`

#### Inherited from

React.SVGProps.stdDeviation

#### Defined in

node_modules/@types/react/index.d.ts:2678

___

### stemh

• `Optional` **stemh**: `string` \| `number`

#### Inherited from

React.SVGProps.stemh

#### Defined in

node_modules/@types/react/index.d.ts:2679

___

### stemv

• `Optional` **stemv**: `string` \| `number`

#### Inherited from

React.SVGProps.stemv

#### Defined in

node_modules/@types/react/index.d.ts:2680

___

### stitchTiles

• `Optional` **stitchTiles**: `string` \| `number`

#### Inherited from

React.SVGProps.stitchTiles

#### Defined in

node_modules/@types/react/index.d.ts:2681

___

### stopColor

• `Optional` **stopColor**: `string`

#### Inherited from

React.SVGProps.stopColor

#### Defined in

node_modules/@types/react/index.d.ts:2682

___

### stopOpacity

• `Optional` **stopOpacity**: `string` \| `number`

#### Inherited from

React.SVGProps.stopOpacity

#### Defined in

node_modules/@types/react/index.d.ts:2683

___

### strikethroughPosition

• `Optional` **strikethroughPosition**: `string` \| `number`

#### Inherited from

React.SVGProps.strikethroughPosition

#### Defined in

node_modules/@types/react/index.d.ts:2684

___

### strikethroughThickness

• `Optional` **strikethroughThickness**: `string` \| `number`

#### Inherited from

React.SVGProps.strikethroughThickness

#### Defined in

node_modules/@types/react/index.d.ts:2685

___

### string

• `Optional` **string**: `string` \| `number`

#### Inherited from

React.SVGProps.string

#### Defined in

node_modules/@types/react/index.d.ts:2686

___

### stroke

• `Optional` **stroke**: `string`

#### Inherited from

React.SVGProps.stroke

#### Defined in

node_modules/@types/react/index.d.ts:2687

___

### strokeDasharray

• `Optional` **strokeDasharray**: `string` \| `number`

#### Inherited from

React.SVGProps.strokeDasharray

#### Defined in

node_modules/@types/react/index.d.ts:2688

___

### strokeDashoffset

• `Optional` **strokeDashoffset**: `string` \| `number`

#### Inherited from

React.SVGProps.strokeDashoffset

#### Defined in

node_modules/@types/react/index.d.ts:2689

___

### strokeLinecap

• `Optional` **strokeLinecap**: ``"inherit"`` \| ``"butt"`` \| ``"round"`` \| ``"square"``

#### Inherited from

React.SVGProps.strokeLinecap

#### Defined in

node_modules/@types/react/index.d.ts:2690

___

### strokeLinejoin

• `Optional` **strokeLinejoin**: ``"inherit"`` \| ``"round"`` \| ``"miter"`` \| ``"bevel"``

#### Inherited from

React.SVGProps.strokeLinejoin

#### Defined in

node_modules/@types/react/index.d.ts:2691

___

### strokeMiterlimit

• `Optional` **strokeMiterlimit**: `string` \| `number`

#### Inherited from

React.SVGProps.strokeMiterlimit

#### Defined in

node_modules/@types/react/index.d.ts:2692

___

### strokeOpacity

• `Optional` **strokeOpacity**: `string` \| `number`

#### Inherited from

React.SVGProps.strokeOpacity

#### Defined in

node_modules/@types/react/index.d.ts:2693

___

### strokeWidth

• `Optional` **strokeWidth**: `string` \| `number`

#### Inherited from

React.SVGProps.strokeWidth

#### Defined in

node_modules/@types/react/index.d.ts:2694

___

### style

• `Optional` **style**: `CSSProperties`

#### Inherited from

React.SVGProps.style

#### Defined in

node_modules/@types/react/index.d.ts:2499

___

### surfaceScale

• `Optional` **surfaceScale**: `string` \| `number`

#### Inherited from

React.SVGProps.surfaceScale

#### Defined in

node_modules/@types/react/index.d.ts:2695

___

### systemLanguage

• `Optional` **systemLanguage**: `string` \| `number`

#### Inherited from

React.SVGProps.systemLanguage

#### Defined in

node_modules/@types/react/index.d.ts:2696

___

### tabIndex

• `Optional` **tabIndex**: `number`

#### Inherited from

React.SVGProps.tabIndex

#### Defined in

node_modules/@types/react/index.d.ts:2506

___

### tableValues

• `Optional` **tableValues**: `string` \| `number`

#### Inherited from

React.SVGProps.tableValues

#### Defined in

node_modules/@types/react/index.d.ts:2697

___

### target

• `Optional` **target**: `string`

#### Inherited from

React.SVGProps.target

#### Defined in

node_modules/@types/react/index.d.ts:2500

___

### targetX

• `Optional` **targetX**: `string` \| `number`

#### Inherited from

React.SVGProps.targetX

#### Defined in

node_modules/@types/react/index.d.ts:2698

___

### targetY

• `Optional` **targetY**: `string` \| `number`

#### Inherited from

React.SVGProps.targetY

#### Defined in

node_modules/@types/react/index.d.ts:2699

___

### textAnchor

• `Optional` **textAnchor**: `string`

#### Inherited from

React.SVGProps.textAnchor

#### Defined in

node_modules/@types/react/index.d.ts:2700

___

### textDecoration

• `Optional` **textDecoration**: `string` \| `number`

#### Inherited from

React.SVGProps.textDecoration

#### Defined in

node_modules/@types/react/index.d.ts:2701

___

### textLength

• `Optional` **textLength**: `string` \| `number`

#### Inherited from

React.SVGProps.textLength

#### Defined in

node_modules/@types/react/index.d.ts:2702

___

### textRendering

• `Optional` **textRendering**: `string` \| `number`

#### Inherited from

React.SVGProps.textRendering

#### Defined in

node_modules/@types/react/index.d.ts:2703

___

### to

• `Optional` **to**: `string` \| `number`

#### Inherited from

React.SVGProps.to

#### Defined in

node_modules/@types/react/index.d.ts:2704

___

### transform

• `Optional` **transform**: `string`

#### Inherited from

React.SVGProps.transform

#### Defined in

node_modules/@types/react/index.d.ts:2705

___

### type

• `Optional` **type**: `string`

#### Inherited from

React.SVGProps.type

#### Defined in

node_modules/@types/react/index.d.ts:2501

___

### u1

• `Optional` **u1**: `string` \| `number`

#### Inherited from

React.SVGProps.u1

#### Defined in

node_modules/@types/react/index.d.ts:2706

___

### u2

• `Optional` **u2**: `string` \| `number`

#### Inherited from

React.SVGProps.u2

#### Defined in

node_modules/@types/react/index.d.ts:2707

___

### underlinePosition

• `Optional` **underlinePosition**: `string` \| `number`

#### Inherited from

React.SVGProps.underlinePosition

#### Defined in

node_modules/@types/react/index.d.ts:2708

___

### underlineThickness

• `Optional` **underlineThickness**: `string` \| `number`

#### Inherited from

React.SVGProps.underlineThickness

#### Defined in

node_modules/@types/react/index.d.ts:2709

___

### unicode

• `Optional` **unicode**: `string` \| `number`

#### Inherited from

React.SVGProps.unicode

#### Defined in

node_modules/@types/react/index.d.ts:2710

___

### unicodeBidi

• `Optional` **unicodeBidi**: `string` \| `number`

#### Inherited from

React.SVGProps.unicodeBidi

#### Defined in

node_modules/@types/react/index.d.ts:2711

___

### unicodeRange

• `Optional` **unicodeRange**: `string` \| `number`

#### Inherited from

React.SVGProps.unicodeRange

#### Defined in

node_modules/@types/react/index.d.ts:2712

___

### unitsPerEm

• `Optional` **unitsPerEm**: `string` \| `number`

#### Inherited from

React.SVGProps.unitsPerEm

#### Defined in

node_modules/@types/react/index.d.ts:2713

___

### vAlphabetic

• `Optional` **vAlphabetic**: `string` \| `number`

#### Inherited from

React.SVGProps.vAlphabetic

#### Defined in

node_modules/@types/react/index.d.ts:2714

___

### vHanging

• `Optional` **vHanging**: `string` \| `number`

#### Inherited from

React.SVGProps.vHanging

#### Defined in

node_modules/@types/react/index.d.ts:2721

___

### vIdeographic

• `Optional` **vIdeographic**: `string` \| `number`

#### Inherited from

React.SVGProps.vIdeographic

#### Defined in

node_modules/@types/react/index.d.ts:2722

___

### vMathematical

• `Optional` **vMathematical**: `string` \| `number`

#### Inherited from

React.SVGProps.vMathematical

#### Defined in

node_modules/@types/react/index.d.ts:2726

___

### values

• `Optional` **values**: `string`

#### Inherited from

React.SVGProps.values

#### Defined in

node_modules/@types/react/index.d.ts:2715

___

### vectorEffect

• `Optional` **vectorEffect**: `string` \| `number`

#### Inherited from

React.SVGProps.vectorEffect

#### Defined in

node_modules/@types/react/index.d.ts:2716

___

### version

• `Optional` **version**: `string`

#### Inherited from

React.SVGProps.version

#### Defined in

node_modules/@types/react/index.d.ts:2717

___

### vertAdvY

• `Optional` **vertAdvY**: `string` \| `number`

#### Inherited from

React.SVGProps.vertAdvY

#### Defined in

node_modules/@types/react/index.d.ts:2718

___

### vertOriginX

• `Optional` **vertOriginX**: `string` \| `number`

#### Inherited from

React.SVGProps.vertOriginX

#### Defined in

node_modules/@types/react/index.d.ts:2719

___

### vertOriginY

• `Optional` **vertOriginY**: `string` \| `number`

#### Inherited from

React.SVGProps.vertOriginY

#### Defined in

node_modules/@types/react/index.d.ts:2720

___

### viewBox

• `Optional` **viewBox**: `string`

#### Inherited from

React.SVGProps.viewBox

#### Defined in

node_modules/@types/react/index.d.ts:2723

___

### viewTarget

• `Optional` **viewTarget**: `string` \| `number`

#### Inherited from

React.SVGProps.viewTarget

#### Defined in

node_modules/@types/react/index.d.ts:2724

___

### visibility

• `Optional` **visibility**: `string` \| `number`

#### Inherited from

React.SVGProps.visibility

#### Defined in

node_modules/@types/react/index.d.ts:2725

___

### width

• `Optional` **width**: `string` \| `number`

#### Inherited from

React.SVGProps.width

#### Defined in

node_modules/@types/react/index.d.ts:2502

___

### widths

• `Optional` **widths**: `string` \| `number`

#### Inherited from

React.SVGProps.widths

#### Defined in

node_modules/@types/react/index.d.ts:2727

___

### wordSpacing

• `Optional` **wordSpacing**: `string` \| `number`

#### Inherited from

React.SVGProps.wordSpacing

#### Defined in

node_modules/@types/react/index.d.ts:2728

___

### writingMode

• `Optional` **writingMode**: `string` \| `number`

#### Inherited from

React.SVGProps.writingMode

#### Defined in

node_modules/@types/react/index.d.ts:2729

___

### x

• `Optional` **x**: `string` \| `number`

#### Inherited from

React.SVGProps.x

#### Defined in

node_modules/@types/react/index.d.ts:2732

___

### x1

• `Optional` **x1**: `string` \| `number`

#### Inherited from

React.SVGProps.x1

#### Defined in

node_modules/@types/react/index.d.ts:2730

___

### x2

• `Optional` **x2**: `string` \| `number`

#### Inherited from

React.SVGProps.x2

#### Defined in

node_modules/@types/react/index.d.ts:2731

___

### xChannelSelector

• `Optional` **xChannelSelector**: `string`

#### Inherited from

React.SVGProps.xChannelSelector

#### Defined in

node_modules/@types/react/index.d.ts:2733

___

### xHeight

• `Optional` **xHeight**: `string` \| `number`

#### Inherited from

React.SVGProps.xHeight

#### Defined in

node_modules/@types/react/index.d.ts:2734

___

### xlinkActuate

• `Optional` **xlinkActuate**: `string`

#### Inherited from

React.SVGProps.xlinkActuate

#### Defined in

node_modules/@types/react/index.d.ts:2735

___

### xlinkArcrole

• `Optional` **xlinkArcrole**: `string`

#### Inherited from

React.SVGProps.xlinkArcrole

#### Defined in

node_modules/@types/react/index.d.ts:2736

___

### xlinkHref

• `Optional` **xlinkHref**: `string`

#### Inherited from

React.SVGProps.xlinkHref

#### Defined in

node_modules/@types/react/index.d.ts:2737

___

### xlinkRole

• `Optional` **xlinkRole**: `string`

#### Inherited from

React.SVGProps.xlinkRole

#### Defined in

node_modules/@types/react/index.d.ts:2738

___

### xlinkShow

• `Optional` **xlinkShow**: `string`

#### Inherited from

React.SVGProps.xlinkShow

#### Defined in

node_modules/@types/react/index.d.ts:2739

___

### xlinkTitle

• `Optional` **xlinkTitle**: `string`

#### Inherited from

React.SVGProps.xlinkTitle

#### Defined in

node_modules/@types/react/index.d.ts:2740

___

### xlinkType

• `Optional` **xlinkType**: `string`

#### Inherited from

React.SVGProps.xlinkType

#### Defined in

node_modules/@types/react/index.d.ts:2741

___

### xmlBase

• `Optional` **xmlBase**: `string`

#### Inherited from

React.SVGProps.xmlBase

#### Defined in

node_modules/@types/react/index.d.ts:2742

___

### xmlLang

• `Optional` **xmlLang**: `string`

#### Inherited from

React.SVGProps.xmlLang

#### Defined in

node_modules/@types/react/index.d.ts:2743

___

### xmlSpace

• `Optional` **xmlSpace**: `string`

#### Inherited from

React.SVGProps.xmlSpace

#### Defined in

node_modules/@types/react/index.d.ts:2746

___

### xmlns

• `Optional` **xmlns**: `string`

#### Inherited from

React.SVGProps.xmlns

#### Defined in

node_modules/@types/react/index.d.ts:2744

___

### xmlnsXlink

• `Optional` **xmlnsXlink**: `string`

#### Inherited from

React.SVGProps.xmlnsXlink

#### Defined in

node_modules/@types/react/index.d.ts:2745

___

### y

• `Optional` **y**: `string` \| `number`

#### Inherited from

React.SVGProps.y

#### Defined in

node_modules/@types/react/index.d.ts:2749

___

### y1

• `Optional` **y1**: `string` \| `number`

#### Inherited from

React.SVGProps.y1

#### Defined in

node_modules/@types/react/index.d.ts:2747

___

### y2

• `Optional` **y2**: `string` \| `number`

#### Inherited from

React.SVGProps.y2

#### Defined in

node_modules/@types/react/index.d.ts:2748

___

### yChannelSelector

• `Optional` **yChannelSelector**: `string`

#### Inherited from

React.SVGProps.yChannelSelector

#### Defined in

node_modules/@types/react/index.d.ts:2750

___

### z

• `Optional` **z**: `string` \| `number`

#### Inherited from

React.SVGProps.z

#### Defined in

node_modules/@types/react/index.d.ts:2751

___

### zoomAndPan

• `Optional` **zoomAndPan**: `string`

#### Inherited from

React.SVGProps.zoomAndPan

#### Defined in

node_modules/@types/react/index.d.ts:2752

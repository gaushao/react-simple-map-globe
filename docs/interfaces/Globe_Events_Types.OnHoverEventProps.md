[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Events Types](../modules/Globe_Events_Types.md) / OnHoverEventProps

# Interface: OnHoverEventProps<ForwardedData\>

[Globe Events Types](../modules/Globe_Events_Types.md).OnHoverEventProps

## Type parameters

| Name | Type |
| :------ | :------ |
| `ForwardedData` | `void` |

## Table of contents

### Properties

- [onMouseEnter](Globe_Events_Types.OnHoverEventProps.md#onmouseenter)
- [onMouseLeave](Globe_Events_Types.OnHoverEventProps.md#onmouseleave)

## Properties

### onMouseEnter

• **onMouseEnter**: [`ForwardedEventCb`](Globe_Events_Types.ForwardedEventCb.md)<`ForwardedData`\>

#### Defined in

[src/components/globe/events/types.ts:60](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L60)

___

### onMouseLeave

• **onMouseLeave**: [`ForwardedEventCb`](Globe_Events_Types.ForwardedEventCb.md)<`ForwardedData`\>

#### Defined in

[src/components/globe/events/types.ts:61](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L61)

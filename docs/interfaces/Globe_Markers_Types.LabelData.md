[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Markers Types](../modules/Globe_Markers_Types.md) / LabelData

# Interface: LabelData

[Globe Markers Types](../modules/Globe_Markers_Types.md).LabelData

label data

## Table of contents

### Properties

- [text](Globe_Markers_Types.LabelData.md#text)

## Properties

### text

• **text**: `string`

#### Defined in

[src/components/globe/markers/types.ts:45](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/markers/types.ts#L45)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Mouse](../modules/Globe_Camera_Mouse.md) / OnWheelEvent

# Interface: OnWheelEvent

[Globe Camera Mouse](../modules/Globe_Camera_Mouse.md).OnWheelEvent

## Hierarchy

- `WheelEventHandler`

  ↳ **`OnWheelEvent`**

## Callable

### OnWheelEvent

▸ **OnWheelEvent**(`event`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `event` | `WheelEvent`<`Element`\> |

#### Returns

`void`

#### Defined in

node_modules/@types/react/index.d.ts:1307

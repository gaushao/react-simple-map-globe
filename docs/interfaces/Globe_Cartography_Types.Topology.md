[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Types](../modules/Globe_Cartography_Types.md) / Topology

# Interface: Topology

[Globe Cartography Types](../modules/Globe_Cartography_Types.md).Topology

## Hierarchy

- `Topology`

  ↳ **`Topology`**

## Table of contents

### Properties

- [arcs](Globe_Cartography_Types.Topology.md#arcs)
- [bbox](Globe_Cartography_Types.Topology.md#bbox)
- [objects](Globe_Cartography_Types.Topology.md#objects)
- [transform](Globe_Cartography_Types.Topology.md#transform)
- [type](Globe_Cartography_Types.Topology.md#type)

## Properties

### arcs

• **arcs**: `Arc`[]

#### Inherited from

TopoJSON.Topology.arcs

#### Defined in

node_modules/@types/topojson-specification/index.d.ts:29

___

### bbox

• `Optional` **bbox**: `BBox`

#### Inherited from

TopoJSON.Topology.bbox

#### Defined in

node_modules/@types/topojson-specification/index.d.ts:22

___

### objects

• **objects**: `Objects`<`GeoJsonProperties`\>

#### Inherited from

TopoJSON.Topology.objects

#### Defined in

node_modules/@types/topojson-specification/index.d.ts:28

___

### transform

• `Optional` **transform**: `Transform`

#### Inherited from

TopoJSON.Topology.transform

#### Defined in

node_modules/@types/topojson-specification/index.d.ts:30

___

### type

• **type**: ``"Topology"``

#### Inherited from

TopoJSON.Topology.type

#### Defined in

node_modules/@types/topojson-specification/index.d.ts:27

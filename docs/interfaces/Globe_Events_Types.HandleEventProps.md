[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Events Types](../modules/Globe_Events_Types.md) / HandleEventProps

# Interface: HandleEventProps<CB, P\>

[Globe Events Types](../modules/Globe_Events_Types.md).HandleEventProps

can be used to assist some events setup

## Type parameters

| Name | Type |
| :------ | :------ |
| `CB` | `Function` |
| `P` | `Record`<`string`, `any`\> |

## Table of contents

### Properties

- [hover](Globe_Events_Types.HandleEventProps.md#hover)

## Properties

### hover

• **hover**: [`HandleHoverEventProps`](Globe_Events_Types.HandleHoverEventProps.md)<`CB`, `P`\>

#### Defined in

[src/components/globe/events/types.ts:32](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L32)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Menu Types](../modules/Globe_Menu_Types.md) / MenuProps

# Interface: MenuProps

[Globe Menu Types](../modules/Globe_Menu_Types.md).MenuProps

## Table of contents

### Properties

- [markers](Globe_Menu_Types.MenuProps.md#markers)
- [settings](Globe_Menu_Types.MenuProps.md#settings)

## Properties

### markers

• `Optional` **markers**: [`MutableState`](../modules/Globe_Types.md#mutablestate)<[`MarkerData`](Globe_Markers_Types.MarkerData.md)[]\>

#### Defined in

[src/components/menu/types.ts:10](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/types.ts#L10)

___

### settings

• `Optional` **settings**: [`GlobeSettings`](Globe_Types.GlobeSettings.md)

#### Defined in

[src/components/menu/types.ts:9](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/types.ts#L9)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Events Types](../modules/Globe_Events_Types.md) / EventsProps

# Interface: EventsProps<ComponentProps, ForwardedData\>

[Globe Events Types](../modules/Globe_Events_Types.md).EventsProps

## Type parameters

| Name | Type |
| :------ | :------ |
| `ComponentProps` | `Record`<`string`, `any`\> |
| `ForwardedData` | `Record`<`string`, `any`\> |

## Table of contents

### Properties

- [forward](Globe_Events_Types.EventsProps.md#forward)
- [props](Globe_Events_Types.EventsProps.md#props)

## Properties

### forward

• **forward**: `ForwardedData`

#### Defined in

[src/components/globe/events/types.ts:43](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L43)

___

### props

• **props**: `ComponentProps`

#### Defined in

[src/components/globe/events/types.ts:42](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L42)

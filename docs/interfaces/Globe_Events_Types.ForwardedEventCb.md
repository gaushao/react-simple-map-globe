[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Events Types](../modules/Globe_Events_Types.md) / ForwardedEventCb

# Interface: ForwardedEventCb<ForwardedData\>

[Globe Events Types](../modules/Globe_Events_Types.md).ForwardedEventCb

## Type parameters

| Name | Type |
| :------ | :------ |
| `ForwardedData` | `void` |

## Callable

### ForwardedEventCb

▸ **ForwardedEventCb**(`forwardedEvent`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `forwardedEvent` | [`ForwardedMouseEvent`](Globe_Events_Types.ForwardedMouseEvent.md) & `ForwardedData` |

#### Returns

`void`

#### Defined in

[src/components/globe/events/types.ts:54](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L54)

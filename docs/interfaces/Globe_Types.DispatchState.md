[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Types](../modules/Globe_Types.md) / DispatchState

# Interface: DispatchState<T\>

[Globe Types](../modules/Globe_Types.md).DispatchState

## Type parameters

| Name |
| :------ |
| `T` |

## Hierarchy

- `Dispatch`<`SetStateAction`<`T`\>\>

  ↳ **`DispatchState`**

## Callable

### DispatchState

▸ **DispatchState**(`value`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `SetStateAction`<`T`\> |

#### Returns

`void`

#### Defined in

node_modules/@types/react/index.d.ts:889

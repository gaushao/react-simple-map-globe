[React Simple Globe](../README.md) / [Exports](../modules.md) / [Dev Form Input](../modules/Dev_Form_Input.md) / InputOnChangeValue

# Interface: InputOnChangeValue<T\>

[Dev Form Input](../modules/Dev_Form_Input.md).InputOnChangeValue

## Type parameters

| Name |
| :------ |
| `T` |

## Callable

### InputOnChangeValue

▸ **InputOnChangeValue**(`v`): `void`

value proxy

#### Parameters

| Name | Type |
| :------ | :------ |
| `v` | `T` |

#### Returns

`void`

#### Defined in

[src/components/form/Input.tsx:17](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Input.tsx#L17)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Dev Form Types](../modules/Dev_Form_Types.md) / FormDispatch

# Interface: FormDispatch<T\>

[Dev Form Types](../modules/Dev_Form_Types.md).FormDispatch

## Type parameters

| Name | Type |
| :------ | :------ |
| `T` | `any` |

## Callable

### FormDispatch

▸ **FormDispatch**(`path`, `value`): `void`

dispatches `value` into [FormContextValue.data](Dev_Form_Types.FormContextValue.md#data) `path`

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` |
| `value` | `T` |

#### Returns

`void`

#### Defined in

[src/components/form/types.ts:10](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/types.ts#L10)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Dev Form Types](../modules/Dev_Form_Types.md) / InitialProps

# Interface: InitialProps<Initial\>

[Dev Form Types](../modules/Dev_Form_Types.md).InitialProps

props forwarded into form provider as [FormContextValue.data](Dev_Form_Types.FormContextValue.md#data)

**`Property`**

form data initial state

## Type parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `Initial` | `any` | type of `initial` |

## Table of contents

### Properties

- [initial](Dev_Form_Types.InitialProps.md#initial)

## Properties

### initial

• **initial**: `Initial`

#### Defined in

[src/components/form/types.ts:18](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/types.ts#L18)

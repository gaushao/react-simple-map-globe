[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Types](../modules/Globe_Cartography_Types.md) / PathForwardedData

# Interface: PathForwardedData

[Globe Cartography Types](../modules/Globe_Cartography_Types.md).PathForwardedData

forwarded into Path events by OnEvents

## Table of contents

### Properties

- [globePathData](Globe_Cartography_Types.PathForwardedData.md#globepathdata)

## Properties

### globePathData

• **globePathData**: `Feature`<`Geometry`, `GeoJsonProperties`\>

#### Defined in

[src/components/globe/cartography/types.ts:24](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/types.ts#L24)

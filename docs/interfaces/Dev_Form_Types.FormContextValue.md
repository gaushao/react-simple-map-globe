[React Simple Globe](../README.md) / [Exports](../modules.md) / [Dev Form Types](../modules/Dev_Form_Types.md) / FormContextValue

# Interface: FormContextValue<T\>

[Dev Form Types](../modules/Dev_Form_Types.md).FormContextValue

**`Property`**

form data

**`Property`**

dispatch callback

## Type parameters

| Name |
| :------ |
| `T` |

## Table of contents

### Properties

- [data](Dev_Form_Types.FormContextValue.md#data)
- [dispatch](Dev_Form_Types.FormContextValue.md#dispatch)

## Properties

### data

• **data**: `T`

#### Defined in

[src/components/form/types.ts:25](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/types.ts#L25)

___

### dispatch

• **dispatch**: [`FormDispatch`](Dev_Form_Types.FormDispatch.md)<`T`\>

#### Defined in

[src/components/form/types.ts:26](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/types.ts#L26)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Types](../modules/Globe_Camera_Types.md) / CameraForwardedData

# Interface: CameraForwardedData

[Globe Camera Types](../modules/Globe_Camera_Types.md).CameraForwardedData

data will be forwarded to events of Camera component

**`Globe Camera Data`**

forwarded data

## Table of contents

### Properties

- [globeCameraData](Globe_Camera_Types.CameraForwardedData.md#globecameradata)

## Properties

### globeCameraData

• **globeCameraData**: [`CameraData`](../classes/Globe_Camera_Classes.CameraData.md) & { `isDragging`: `boolean`  }

#### Defined in

[src/components/globe/camera/types.ts:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/types.ts#L14)

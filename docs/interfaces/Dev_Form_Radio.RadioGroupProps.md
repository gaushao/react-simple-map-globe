[React Simple Globe](../README.md) / [Exports](../modules.md) / [Dev Form Radio](../modules/Dev_Form_Radio.md) / RadioGroupProps

# Interface: RadioGroupProps

[Dev Form Radio](../modules/Dev_Form_Radio.md).RadioGroupProps

## Table of contents

### Properties

- [onSelect](Dev_Form_Radio.RadioGroupProps.md#onselect)
- [options](Dev_Form_Radio.RadioGroupProps.md#options)
- [selected](Dev_Form_Radio.RadioGroupProps.md#selected)

## Properties

### onSelect

• **onSelect**: (`value`: ``null`` \| `string`) => `void`

#### Type declaration

▸ (`value`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `value` | ``null`` \| `string` |

##### Returns

`void`

#### Defined in

[src/components/form/Radio.tsx:68](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L68)

___

### options

• **options**: [`RadioOption`](Dev_Form_Radio.RadioOption.md)[]

#### Defined in

[src/components/form/Radio.tsx:67](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L67)

___

### selected

• **selected**: ``null`` \| `string`

#### Defined in

[src/components/form/Radio.tsx:66](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L66)

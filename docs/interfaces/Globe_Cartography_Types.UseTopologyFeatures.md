[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Types](../modules/Globe_Cartography_Types.md) / UseTopologyFeatures

# Interface: UseTopologyFeatures

[Globe Cartography Types](../modules/Globe_Cartography_Types.md).UseTopologyFeatures

## Callable

### UseTopologyFeatures

▸ **UseTopologyFeatures**(`name?`): [`Feature`](Globe_Cartography_Types.Feature.md)[]

#### Parameters

| Name | Type |
| :------ | :------ |
| `name?` | `string` |

#### Returns

[`Feature`](Globe_Cartography_Types.Feature.md)[]

#### Defined in

[src/components/globe/cartography/types.ts:54](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/types.ts#L54)

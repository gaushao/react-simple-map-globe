[React Simple Globe](../README.md) / [Exports](../modules.md) / [Dev Form Radio](../modules/Dev_Form_Radio.md) / RadioOption

# Interface: RadioOption

[Dev Form Radio](../modules/Dev_Form_Radio.md).RadioOption

## Table of contents

### Properties

- [name](Dev_Form_Radio.RadioOption.md#name)
- [value](Dev_Form_Radio.RadioOption.md#value)

## Properties

### name

• **name**: ``null`` \| `string`

#### Defined in

[src/components/form/Radio.tsx:11](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L11)

___

### value

• **value**: ``null`` \| `string`

#### Defined in

[src/components/form/Radio.tsx:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L12)

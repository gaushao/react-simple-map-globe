[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Types](../modules/Globe_Cartography_Types.md) / CartographyProps

# Interface: CartographyProps

[Globe Cartography Types](../modules/Globe_Cartography_Types.md).CartographyProps

## Table of contents

### Properties

- [geo](Globe_Cartography_Types.CartographyProps.md#geo)
- [settings](Globe_Cartography_Types.CartographyProps.md#settings)

## Properties

### geo

• `Optional` **geo**: `Partial`<[`PathProps`](Globe_Cartography_Types.PathProps.md)\>

#### Defined in

[src/components/globe/cartography/types.ts:32](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/types.ts#L32)

___

### settings

• `Optional` **settings**: [`MutableState`](../modules/Globe_Types.md#mutablestate)<[`CartographyData`](../classes/Globe_Cartography_Classes.CartographyData.md)\>

#### Defined in

[src/components/globe/cartography/types.ts:31](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/types.ts#L31)

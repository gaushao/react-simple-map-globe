[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Topology](../modules/Globe_Cartography_Topology.md) / TopologyProps

# Interface: TopologyProps

[Globe Cartography Topology](../modules/Globe_Cartography_Topology.md).TopologyProps

## Table of contents

### Properties

- [data](Globe_Cartography_Topology.TopologyProps.md#data)
- [geo](Globe_Cartography_Topology.TopologyProps.md#geo)

## Properties

### data

• **data**: `Topology`<`Objects`<`GeoJsonProperties`\>\>

#### Defined in

[src/components/globe/cartography/Topology.tsx:16](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/Topology.tsx#L16)

___

### geo

• `Optional` **geo**: `Partial`<[`PathProps`](Globe_Cartography_Types.PathProps.md)\>

#### Defined in

[src/components/globe/cartography/Topology.tsx:17](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/Topology.tsx#L17)

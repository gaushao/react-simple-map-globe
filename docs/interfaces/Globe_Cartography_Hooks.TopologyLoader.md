[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Hooks](../modules/Globe_Cartography_Hooks.md) / TopologyLoader

# Interface: TopologyLoader

[Globe Cartography Hooks](../modules/Globe_Cartography_Hooks.md).TopologyLoader

## Table of contents

### Properties

- [features](Globe_Cartography_Hooks.TopologyLoader.md#features)
- [keys](Globe_Cartography_Hooks.TopologyLoader.md#keys)
- [topologies](Globe_Cartography_Hooks.TopologyLoader.md#topologies)
- [topology](Globe_Cartography_Hooks.TopologyLoader.md#topology)

## Properties

### features

• **features**: `Feature`<`Geometry`, `GeoJsonProperties`\>[][]

#### Defined in

[src/components/globe/cartography/hooks.tsx:33](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/hooks.tsx#L33)

___

### keys

• **keys**: `string`[]

#### Defined in

[src/components/globe/cartography/hooks.tsx:34](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/hooks.tsx#L34)

___

### topologies

• **topologies**: [`Topologies`](../modules/Globe_Cartography_Hooks.md#topologies)

#### Defined in

[src/components/globe/cartography/hooks.tsx:35](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/hooks.tsx#L35)

___

### topology

• `Optional` **topology**: `Topology`<`Objects`<`GeoJsonProperties`\>\>

#### Defined in

[src/components/globe/cartography/hooks.tsx:32](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/hooks.tsx#L32)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Mouse](../modules/Globe_Camera_Mouse.md) / MouseMoveController

# Interface: MouseMoveController

[Globe Camera Mouse](../modules/Globe_Camera_Mouse.md).MouseMoveController

## Table of contents

### Properties

- [move](Globe_Camera_Mouse.MouseMoveController.md#move)
- [stop](Globe_Camera_Mouse.MouseMoveController.md#stop)
- [x](Globe_Camera_Mouse.MouseMoveController.md#x)
- [y](Globe_Camera_Mouse.MouseMoveController.md#y)

## Properties

### move

• **move**: [`OnMouseMoveEvent`](Globe_Camera_Mouse.OnMouseMoveEvent.md)

#### Defined in

[src/components/globe/camera/hooks/mouse.ts:34](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/mouse.ts#L34)

___

### stop

• **stop**: [`OnMouseStopEvent`](Globe_Camera_Mouse.OnMouseStopEvent.md)

#### Defined in

[src/components/globe/camera/hooks/mouse.ts:35](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/mouse.ts#L35)

___

### x

• **x**: `number`

#### Defined in

[src/components/globe/camera/hooks/mouse.ts:32](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/mouse.ts#L32)

___

### y

• **y**: `number`

#### Defined in

[src/components/globe/camera/hooks/mouse.ts:33](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/mouse.ts#L33)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Types](../modules/Globe_Types.md) / BooleanState

# Interface: BooleanState

[Globe Types](../modules/Globe_Types.md).BooleanState

## Table of contents

### Properties

- [bool](Globe_Types.BooleanState.md#bool)
- [falsy](Globe_Types.BooleanState.md#falsy)
- [negate](Globe_Types.BooleanState.md#negate)
- [trully](Globe_Types.BooleanState.md#trully)

## Properties

### bool

• **bool**: `boolean`

boolean state

#### Defined in

[src/components/globe/types.ts:31](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L31)

___

### falsy

• **falsy**: () => `void`

#### Type declaration

▸ (): `void`

sets bool to false

##### Returns

`void`

#### Defined in

[src/components/globe/types.ts:35](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L35)

___

### negate

• **negate**: () => `void`

#### Type declaration

▸ (): `void`

sets bool to !bool

##### Returns

`void`

#### Defined in

[src/components/globe/types.ts:37](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L37)

___

### trully

• **trully**: () => `void`

#### Type declaration

▸ (): `void`

sets bool to true

##### Returns

`void`

#### Defined in

[src/components/globe/types.ts:33](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L33)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Touch](../modules/Globe_Camera_Touch.md) / TouchPinchDelta

# Interface: TouchPinchDelta

[Globe Camera Touch](../modules/Globe_Camera_Touch.md).TouchPinchDelta

todo docs

## Table of contents

### Properties

- [delta](Globe_Camera_Touch.TouchPinchDelta.md#delta)
- [pinch](Globe_Camera_Touch.TouchPinchDelta.md#pinch)
- [release](Globe_Camera_Touch.TouchPinchDelta.md#release)

## Properties

### delta

• **delta**: `number`

#### Defined in

[src/components/globe/camera/hooks/touch.ts:100](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L100)

___

### pinch

• **pinch**: [`OnTouchMoveEvent`](Globe_Camera_Touch.OnTouchMoveEvent.md)

#### Defined in

[src/components/globe/camera/hooks/touch.ts:101](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L101)

___

### release

• **release**: [`OnTouchStopEvent`](Globe_Camera_Touch.OnTouchStopEvent.md)

#### Defined in

[src/components/globe/camera/hooks/touch.ts:102](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L102)

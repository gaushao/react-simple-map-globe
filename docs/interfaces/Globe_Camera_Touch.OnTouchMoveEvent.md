[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Touch](../modules/Globe_Camera_Touch.md) / OnTouchMoveEvent

# Interface: OnTouchMoveEvent

[Globe Camera Touch](../modules/Globe_Camera_Touch.md).OnTouchMoveEvent

## Hierarchy

- `TouchEventHandler`

  ↳ **`OnTouchMoveEvent`**

## Callable

### OnTouchMoveEvent

▸ **OnTouchMoveEvent**(`event`): `void`

TouchEventHandler

#### Parameters

| Name | Type |
| :------ | :------ |
| `event` | `TouchEvent`<`Element`\> |

#### Returns

`void`

#### Defined in

node_modules/@types/react/index.d.ts:1307

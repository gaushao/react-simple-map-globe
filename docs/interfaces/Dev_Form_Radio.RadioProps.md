[React Simple Globe](../README.md) / [Exports](../modules.md) / [Dev Form Radio](../modules/Dev_Form_Radio.md) / RadioProps

# Interface: RadioProps

[Dev Form Radio](../modules/Dev_Form_Radio.md).RadioProps

## Table of contents

### Properties

- [checked](Dev_Form_Radio.RadioProps.md#checked)
- [onChange](Dev_Form_Radio.RadioProps.md#onchange)
- [option](Dev_Form_Radio.RadioProps.md#option)

## Properties

### checked

• **checked**: `boolean`

#### Defined in

[src/components/form/Radio.tsx:17](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L17)

___

### onChange

• **onChange**: `ChangeEventHandler`<`HTMLInputElement`\>

#### Defined in

[src/components/form/Radio.tsx:18](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L18)

___

### option

• **option**: [`RadioOption`](Dev_Form_Radio.RadioOption.md)

#### Defined in

[src/components/form/Radio.tsx:16](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L16)

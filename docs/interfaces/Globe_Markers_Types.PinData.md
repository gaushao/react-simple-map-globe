[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Markers Types](../modules/Globe_Markers_Types.md) / PinData

# Interface: PinData

[Globe Markers Types](../modules/Globe_Markers_Types.md).PinData

svg props overrides theme

## Table of contents

### Properties

- [shape](Globe_Markers_Types.PinData.md#shape)

## Properties

### shape

• `Optional` **shape**: [`PinShapes`](../modules/Globe_Markers_Types.md#pinshapes)

#### Defined in

[src/components/globe/markers/types.ts:32](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/markers/types.ts#L32)

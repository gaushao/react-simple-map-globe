[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Types](../modules/Globe_Types.md) / GlobeSettings

# Interface: GlobeSettings

[Globe Types](../modules/Globe_Types.md).GlobeSettings

every setting module can offer

## Table of contents

### Properties

- [camera](Globe_Types.GlobeSettings.md#camera)
- [cartography](Globe_Types.GlobeSettings.md#cartography)
- [view](Globe_Types.GlobeSettings.md#view)

## Properties

### camera

• `Optional` **camera**: [`MutableState`](../modules/Globe_Types.md#mutablestate)<[`CameraData`](../classes/Globe_Camera_Classes.CameraData.md)\>

#### Defined in

[src/components/globe/types.ts:54](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L54)

___

### cartography

• `Optional` **cartography**: [`MutableState`](../modules/Globe_Types.md#mutablestate)<[`CartographyData`](../classes/Globe_Cartography_Classes.CartographyData.md)\>

#### Defined in

[src/components/globe/types.ts:56](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L56)

___

### view

• `Optional` **view**: [`MutableState`](../modules/Globe_Types.md#mutablestate)<[`ViewData`](../classes/Globe_View_Classes.ViewData.md)\>

#### Defined in

[src/components/globe/types.ts:55](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/types.ts#L55)

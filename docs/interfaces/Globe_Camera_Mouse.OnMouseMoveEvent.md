[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Mouse](../modules/Globe_Camera_Mouse.md) / OnMouseMoveEvent

# Interface: OnMouseMoveEvent

[Globe Camera Mouse](../modules/Globe_Camera_Mouse.md).OnMouseMoveEvent

## Hierarchy

- `MouseEventHandler`

  ↳ **`OnMouseMoveEvent`**

## Callable

### OnMouseMoveEvent

▸ **OnMouseMoveEvent**(`event`): `void`

#### Parameters

| Name | Type |
| :------ | :------ |
| `event` | `MouseEvent`<`Element`, `MouseEvent`\> |

#### Returns

`void`

#### Defined in

node_modules/@types/react/index.d.ts:1307

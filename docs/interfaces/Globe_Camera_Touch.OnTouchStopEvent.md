[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Touch](../modules/Globe_Camera_Touch.md) / OnTouchStopEvent

# Interface: OnTouchStopEvent

[Globe Camera Touch](../modules/Globe_Camera_Touch.md).OnTouchStopEvent

## Hierarchy

- `TouchEventHandler`

  ↳ **`OnTouchStopEvent`**

## Callable

### OnTouchStopEvent

▸ **OnTouchStopEvent**(): `any`

TouchEventHandler

#### Returns

`any`

#### Defined in

[src/components/globe/camera/hooks/touch.ts:21](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L21)

### OnTouchStopEvent

▸ **OnTouchStopEvent**(`event`): `void`

TouchEventHandler

#### Parameters

| Name | Type |
| :------ | :------ |
| `event` | `TouchEvent`<`Element`\> |

#### Returns

`void`

#### Defined in

node_modules/@types/react/index.d.ts:1307

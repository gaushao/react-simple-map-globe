[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Events Types](../modules/Globe_Events_Types.md) / EventData

# Interface: EventData<CB, P\>

[Globe Events Types](../modules/Globe_Events_Types.md).EventData

used set default handlers

**`Property`**

will overwrite current component props

**`Property`**

triggers with respective event callback

**`Property`**

injected through callback

## Type parameters

| Name | Type |
| :------ | :------ |
| `CB` | `Function` |
| `P` | `Record`<`string`, `any`\> |

## Table of contents

### Properties

- [callback](Globe_Events_Types.EventData.md#callback)
- [props](Globe_Events_Types.EventData.md#props)

## Properties

### callback

• `Optional` **callback**: `CB`

#### Defined in

[src/components/globe/events/types.ts:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L14)

___

### props

• `Optional` **props**: `P`

#### Defined in

[src/components/globe/events/types.ts:15](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L15)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Touch](../modules/Globe_Camera_Touch.md) / TouchMoveController

# Interface: TouchMoveController

[Globe Camera Touch](../modules/Globe_Camera_Touch.md).TouchMoveController

## Table of contents

### Properties

- [id](Globe_Camera_Touch.TouchMoveController.md#id)
- [move](Globe_Camera_Touch.TouchMoveController.md#move)
- [stop](Globe_Camera_Touch.TouchMoveController.md#stop)
- [x](Globe_Camera_Touch.TouchMoveController.md#x)
- [y](Globe_Camera_Touch.TouchMoveController.md#y)

## Properties

### id

• **id**: ``null`` \| `number`

#### Defined in

[src/components/globe/camera/hooks/touch.ts:25](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L25)

___

### move

• **move**: [`OnTouchMoveEvent`](Globe_Camera_Touch.OnTouchMoveEvent.md)

#### Defined in

[src/components/globe/camera/hooks/touch.ts:28](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L28)

___

### stop

• **stop**: [`OnTouchStopEvent`](Globe_Camera_Touch.OnTouchStopEvent.md)

#### Defined in

[src/components/globe/camera/hooks/touch.ts:29](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L29)

___

### x

• **x**: `number`

#### Defined in

[src/components/globe/camera/hooks/touch.ts:26](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L26)

___

### y

• **y**: `number`

#### Defined in

[src/components/globe/camera/hooks/touch.ts:27](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/touch.ts#L27)

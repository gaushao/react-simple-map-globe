[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Events Types](../modules/Globe_Events_Types.md) / HandleHoverEventProps

# Interface: HandleHoverEventProps<CB, P\>

[Globe Events Types](../modules/Globe_Events_Types.md).HandleHoverEventProps

can be set to easily handle `onMouseEnter` and `onMouseLeave` events

## Type parameters

| Name | Type |
| :------ | :------ |
| `CB` | `Function` |
| `P` | `Record`<`string`, `any`\> |

## Table of contents

### Properties

- [enter](Globe_Events_Types.HandleHoverEventProps.md#enter)
- [leave](Globe_Events_Types.HandleHoverEventProps.md#leave)

## Properties

### enter

• `Optional` **enter**: [`EventData`](Globe_Events_Types.EventData.md)<`CB`, `P`\>

#### Defined in

[src/components/globe/events/types.ts:23](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L23)

___

### leave

• `Optional` **leave**: [`EventData`](Globe_Events_Types.EventData.md)<`CB`, `P`\>

#### Defined in

[src/components/globe/events/types.ts:24](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/types.ts#L24)

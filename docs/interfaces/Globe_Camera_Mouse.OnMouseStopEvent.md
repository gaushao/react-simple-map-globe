[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Mouse](../modules/Globe_Camera_Mouse.md) / OnMouseStopEvent

# Interface: OnMouseStopEvent

[Globe Camera Mouse](../modules/Globe_Camera_Mouse.md).OnMouseStopEvent

## Hierarchy

- `MouseEventHandler`

  ↳ **`OnMouseStopEvent`**

## Callable

### OnMouseStopEvent

▸ **OnMouseStopEvent**(): `void`

MouseEventHandler

#### Returns

`void`

#### Defined in

[src/components/globe/camera/hooks/mouse.ts:21](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/mouse.ts#L21)

### OnMouseStopEvent

▸ **OnMouseStopEvent**(`event`): `void`

MouseEventHandler

#### Parameters

| Name | Type |
| :------ | :------ |
| `event` | `MouseEvent`<`Element`, `MouseEvent`\> |

#### Returns

`void`

#### Defined in

node_modules/@types/react/index.d.ts:1307

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Classes](../modules/Globe_Camera_Classes.md) / CameraProps

# Class: CameraProps

[Globe Camera Classes](../modules/Globe_Camera_Classes.md).CameraProps

**`Property`**

## Table of contents

### Constructors

- [constructor](Globe_Camera_Classes.CameraProps.md#constructor)

### Properties

- [settings](Globe_Camera_Classes.CameraProps.md#settings)

## Constructors

### constructor

• **new CameraProps**()

## Properties

### settings

• `Optional` **settings**: [`MutableState`](../modules/Globe_Types.md#mutablestate)<[`CameraData`](Globe_Camera_Classes.CameraData.md)\>

#### Defined in

[src/components/globe/camera/classes.ts:21](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/classes.ts#L21)

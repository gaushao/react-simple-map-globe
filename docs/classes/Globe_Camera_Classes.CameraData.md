[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Camera Classes](../modules/Globe_Camera_Classes.md) / CameraData

# Class: CameraData

[Globe Camera Classes](../modules/Globe_Camera_Classes.md).CameraData

## Table of contents

### Constructors

- [constructor](Globe_Camera_Classes.CameraData.md#constructor)

### Properties

- [rotation](Globe_Camera_Classes.CameraData.md#rotation)

## Constructors

### constructor

• **new CameraData**(`rotation?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `rotation` | [`Coord`](Globe_Classes.Coord.md) | `EMPTY_COORD` |

#### Defined in

[src/components/globe/camera/classes.ts:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/classes.ts#L14)

## Properties

### rotation

• **rotation**: [`Coord`](Globe_Classes.Coord.md) = `EMPTY_COORD`

#### Defined in

[src/components/globe/camera/classes.ts:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/classes.ts#L14)

[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe View Context](../modules/Globe_View_Context.md) / ViewContextValue

# Class: ViewContextValue

[Globe View Context](../modules/Globe_View_Context.md).ViewContextValue

## Table of contents

### Constructors

- [constructor](Globe_View_Context.ViewContextValue.md#constructor)

### Properties

- [dimensions](Globe_View_Context.ViewContextValue.md#dimensions)
- [scaling](Globe_View_Context.ViewContextValue.md#scaling)

## Constructors

### constructor

• **new ViewContextValue**()

## Properties

### dimensions

• **dimensions**: [`MutableStates`](../modules/Globe_Types.md#mutablestates)<`number`\> = `EMPTY_MUTABLE_STATE`

#### Defined in

[src/components/globe/view/Context.tsx:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/Context.tsx#L12)

___

### scaling

• **scaling**: [`MutableState`](../modules/Globe_Types.md#mutablestate)<`number`\> = `EMPTY_MUTABLE_STATE`

#### Defined in

[src/components/globe/view/Context.tsx:11](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/Context.tsx#L11)

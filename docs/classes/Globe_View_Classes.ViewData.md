[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe View Classes](../modules/Globe_View_Classes.md) / ViewData

# Class: ViewData

[Globe View Classes](../modules/Globe_View_Classes.md).ViewData

## Table of contents

### Constructors

- [constructor](Globe_View_Classes.ViewData.md#constructor)

### Properties

- [dimensions](Globe_View_Classes.ViewData.md#dimensions)
- [fetchToTarget](Globe_View_Classes.ViewData.md#fetchtotarget)
- [scale](Globe_View_Classes.ViewData.md#scale)
- [target](Globe_View_Classes.ViewData.md#target)

## Constructors

### constructor

• **new ViewData**(`dimensions?`, `scale?`, `target?`, `fetchToTarget?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `dimensions` | [`Point`](Globe_Classes.Point.md) | `EMPTY_POINT` |
| `scale` | `number` | `INITIAL_SCALE` |
| `target` | ``null`` \| `HTMLElement` | `null` |
| `fetchToTarget` | `boolean` | `false` |

#### Defined in

[src/components/globe/view/classes.ts:15](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/classes.ts#L15)

## Properties

### dimensions

• **dimensions**: [`Point`](Globe_Classes.Point.md) = `EMPTY_POINT`

#### Defined in

[src/components/globe/view/classes.ts:16](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/classes.ts#L16)

___

### fetchToTarget

• **fetchToTarget**: `boolean` = `false`

#### Defined in

[src/components/globe/view/classes.ts:19](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/classes.ts#L19)

___

### scale

• **scale**: `number` = `INITIAL_SCALE`

#### Defined in

[src/components/globe/view/classes.ts:17](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/classes.ts#L17)

___

### target

• **target**: ``null`` \| `HTMLElement` = `null`

#### Defined in

[src/components/globe/view/classes.ts:18](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/classes.ts#L18)

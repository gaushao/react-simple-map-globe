[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe View Classes](../modules/Globe_View_Classes.md) / ViewProps

# Class: ViewProps

[Globe View Classes](../modules/Globe_View_Classes.md).ViewProps

## Table of contents

### Constructors

- [constructor](Globe_View_Classes.ViewProps.md#constructor)

### Properties

- [settings](Globe_View_Classes.ViewProps.md#settings)

## Constructors

### constructor

• **new ViewProps**()

## Properties

### settings

• `Optional` **settings**: [`MutableState`](../modules/Globe_Types.md#mutablestate)<[`ViewData`](Globe_View_Classes.ViewData.md)\>

#### Defined in

[src/components/globe/view/classes.ts:27](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/classes.ts#L27)

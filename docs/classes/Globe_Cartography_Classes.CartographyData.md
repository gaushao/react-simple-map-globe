[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Classes](../modules/Globe_Cartography_Classes.md) / CartographyData

# Class: CartographyData

[Globe Cartography Classes](../modules/Globe_Cartography_Classes.md).CartographyData

## Table of contents

### Constructors

- [constructor](Globe_Cartography_Classes.CartographyData.md#constructor)

### Properties

- [jsons](Globe_Cartography_Classes.CartographyData.md#jsons)
- [urls](Globe_Cartography_Classes.CartographyData.md#urls)

## Constructors

### constructor

• **new CartographyData**(`urls?`, `jsons?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `urls?` | `string`[] |
| `jsons?` | `JSON`[] |

#### Defined in

[src/components/globe/cartography/classes.ts:10](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/classes.ts#L10)

## Properties

### jsons

• `Optional` **jsons**: `JSON`[]

#### Defined in

[src/components/globe/cartography/classes.ts:10](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/classes.ts#L10)

___

### urls

• `Optional` **urls**: `string`[]

#### Defined in

[src/components/globe/cartography/classes.ts:10](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/classes.ts#L10)

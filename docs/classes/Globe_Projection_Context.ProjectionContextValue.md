[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Projection Context](../modules/Globe_Projection_Context.md) / ProjectionContextValue

# Class: ProjectionContextValue

[Globe Projection Context](../modules/Globe_Projection_Context.md).ProjectionContextValue

**`Property`**

## Table of contents

### Constructors

- [constructor](Globe_Projection_Context.ProjectionContextValue.md#constructor)

### Properties

- [height](Globe_Projection_Context.ProjectionContextValue.md#height)
- [path](Globe_Projection_Context.ProjectionContextValue.md#path)
- [projection](Globe_Projection_Context.ProjectionContextValue.md#projection)
- [width](Globe_Projection_Context.ProjectionContextValue.md#width)

## Constructors

### constructor

• **new ProjectionContextValue**(`width?`, `height?`, `projection?`, `path?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `width` | `number` | `0` |
| `height` | `number` | `0` |
| `projection` | ``null`` \| [`D3GeoProjection`](../interfaces/Globe_Projection_Context.D3GeoProjection.md) | `null` |
| `path` | ``null`` \| `GeoPath`<`any`, `GeoPermissibleObjects`\> | `null` |

#### Defined in

[src/components/globe/projection/Context.ts:19](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/Context.ts#L19)

## Properties

### height

• **height**: `number` = `0`

#### Defined in

[src/components/globe/projection/Context.ts:21](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/Context.ts#L21)

___

### path

• **path**: ``null`` \| `GeoPath`<`any`, `GeoPermissibleObjects`\> = `null`

#### Defined in

[src/components/globe/projection/Context.ts:23](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/Context.ts#L23)

___

### projection

• **projection**: ``null`` \| [`D3GeoProjection`](../interfaces/Globe_Projection_Context.D3GeoProjection.md) = `null`

#### Defined in

[src/components/globe/projection/Context.ts:22](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/Context.ts#L22)

___

### width

• **width**: `number` = `0`

#### Defined in

[src/components/globe/projection/Context.ts:20](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/Context.ts#L20)

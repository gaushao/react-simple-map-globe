[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Cartography Constants](../modules/Globe_Cartography_Constants.md) / CartographyUrl

# Enumeration: CartographyUrl

[Globe Cartography Constants](../modules/Globe_Cartography_Constants.md).CartographyUrl

## Table of contents

### Enumeration Members

- [COUNTRIES](Globe_Cartography_Constants.CartographyUrl.md#countries)
- [LAND](Globe_Cartography_Constants.CartographyUrl.md#land)
- [OCEANS](Globe_Cartography_Constants.CartographyUrl.md#oceans)

## Enumeration Members

### COUNTRIES

• **COUNTRIES** = ``"https://cdn.jsdelivr.net/npm/world-atlas@2/countries-110m.json"``

#### Defined in

[src/components/globe/cartography/constants.ts:13](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/constants.ts#L13)

___

### LAND

• **LAND** = ``"https://cdn.jsdelivr.net/npm/world-atlas@2/land-110m.json"``

#### Defined in

[src/components/globe/cartography/constants.ts:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/constants.ts#L12)

___

### OCEANS

• **OCEANS** = ``"https://gist.githubusercontent.com/jrrickard/8755532505a40f3b8317/raw/ecd98849d3a5f4502b773b986254f19af3b8d8fb/oceans.json"``

#### Defined in

[src/components/globe/cartography/constants.ts:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/constants.ts#L14)

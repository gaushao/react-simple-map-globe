[React Simple Globe](../README.md) / [Exports](../modules.md) / [Globe Markers Types](../modules/Globe_Markers_Types.md) / PinShape

# Enumeration: PinShape

[Globe Markers Types](../modules/Globe_Markers_Types.md).PinShape

used to switch from marker shapes at Pin

## Table of contents

### Enumeration Members

- [CIRCLE](Globe_Markers_Types.PinShape.md#circle)
- [LOCATION](Globe_Markers_Types.PinShape.md#location)

## Enumeration Members

### CIRCLE

• **CIRCLE** = ``"circle"``

#### Defined in

[src/components/globe/markers/types.ts:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/markers/types.ts#L14)

___

### LOCATION

• **LOCATION** = ``"location"``

#### Defined in

[src/components/globe/markers/types.ts:15](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/markers/types.ts#L15)

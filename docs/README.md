React Simple Globe / [Exports](modules.md)

### Try

- [Live](https://gaushao.gitlab.io/react-simple-map-globe)
- [Docs](https://gaushao.gitlab.io/react-simple-map-globe/docs) report missing/wrong documentation [here](https://gitlab.com/gaushao/react-simple-map-globe/-/issues/4)
- [DocsMD](https://gitlab.com/gaushao/react-simple-map-globe/-/tree/master/docs)

### Node 14

```
$ node -v
v14.21.1
```

### Install

```
$ npm i -g yarn
$ yarn
```

### Add Missing Third-Part Declarations

at `src/react-app-env.d.ts`

```
declare module 'library-name' {
    ...
}

```

### Dev

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

#### dev: app

```
$ yarn start
```

#### dev: docs

```
$ yarn devdocs
```

```
$ npm i -g http-server
$ http-server build/docs
```

### Build

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### build: app

for building local remove homepage from package.json

```
$ yarn build
$ http-server build
```

### Publish

Actually GitlabCI runs `npx tsc --build` at `src/components/globe` every master push. In order to change package registry do that at `src/components/globe/package.json`, edit publishConfig as needed, replacing scope_name and project_id

```
  "publishConfig": {
    "@<scope_name>:registry": "https://gitlab.com/api/v4/projects/<project_id>/packages/npm/",
    "access": "public"
  },
```

`.npmrc` file must to be placed at root folder, points scope_name and project_id to an authorized project

```
@<scope_name>:registry=https://gitlab.com/api/v4/projects/<project_id>/packages/npm/
//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}
//gitlab.com/api/v4/projects/<project_id>/packages/npm/:_authToken=${CI_JOB_TOKEN}
```

GitlabCI will parse CI_JOB_TOKEN into available token regard pusher premissions

CI_PROJECT_ID can be used aswell

```
@<scope_name>:registry=https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/
//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}
//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}
```

#### publish: prepare

runs `npx tsc --build src/components/globe/`, config can be set into `src/components/globe/tsconfig.json`

```
$ yarn prepare
```

#### build: docs

_HTML_

```
$ yarn docs
$ serve -s build/docs
```

_MD_

```
$ yarn docsmd
```

#### push: docs

will build md docs and push into master by running `pushdocsmd.sh`

```
$ yarn pushdocsmd
```

### Test

**(not tested)**

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

```
$ yarn test
```

### Eject

**(not tested)**

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

```
$ yarn eject
```

## Learn More

- [React documentation](https://reactjs.org/).
- [Create React App Docs](https://facebook.github.io/create-react-app/docs/getting-started)
- [D3JS](https://d3js.org/)
- [GeoJSON](https://geojson.org/)
- [React Simple Maps](https://www.react-simple-maps.io/)
- [Z Creative Labs](https://zcreativelabs.com/blog/)
- [TypeDoc](https://typedoc.org/)
- [X-Y Reflection](https://www.mashupmath.com/blog/reflection-over-x-y-axis)
- [Observable](https://observablehq.com/)
- [WebGL](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API)
- [DevDocs](https://devdocs.io/)
- [React D3](https://github.com/react-d3-library/react-d3-library)
- [D3 Geo](https://github.com/d3/d3-geo)
- [SVG Rendering](https://codepen.io/tigt/post/improving-svg-rendering-performance)
- [Optimizing SVG](https://css-tricks.com/tools-for-optimizing-svg/)
- [React Optimization](https://www.codementor.io/blog/react-optimization-5wiwjnf9hj)

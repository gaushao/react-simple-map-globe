[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev App

# Module: Dev App

application root used to develop globe module

## Table of contents

### Component Functions

- [default](Dev_App.md#default)

## Component Functions

### default

▸ **default**(): `Element`

setup development props into [Globe](Globe.md) and [Menu](Menu.md)

**`Hooks`**

useSettings useMarkers

#### Returns

`Element`

>`div`
>>[Menu](Menu.md)\
>>[Globe](Globe.md)

#### Defined in

[src/App.tsx:31](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/App.tsx#L31)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev Hooks

# Module: Dev Hooks

application root
easly setting of globe states

## Table of contents

### State Functions

- [useMarkers](Dev_Hooks.md#usemarkers)
- [useSettings](Dev_Hooks.md#usesettings)

### Tool Functions

- [useDebugger](Dev_Hooks.md#usedebugger)

## State Functions

### useMarkers

▸ **useMarkers**(): [`MutableState`](Globe_Types.md#mutablestate)<[`MarkerData`](../interfaces/Globe_Markers_Types.MarkerData.md)[]\>

setup state for markers

#### Returns

[`MutableState`](Globe_Types.md#mutablestate)<[`MarkerData`](../interfaces/Globe_Markers_Types.MarkerData.md)[]\>

#### Defined in

[src/hooks.ts:30](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/hooks.ts#L30)

___

### useSettings

▸ **useSettings**(): [`GlobeSettings`](../interfaces/Globe_Types.GlobeSettings.md)

initialize settings states

#### Returns

[`GlobeSettings`](../interfaces/Globe_Types.GlobeSettings.md)

#### Defined in

[src/hooks.ts:19](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/hooks.ts#L19)

___

## Tool Functions

### useDebugger

▸ **useDebugger**(): (`text`: `string`) => `void`

set innerText to debug div

#### Returns

`fn`

▸ (`text`): `void`

##### Parameters

| Name | Type |
| :------ | :------ |
| `text` | `string` |

##### Returns

`void`

#### Defined in

[src/hooks.ts:39](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/hooks.ts#L39)

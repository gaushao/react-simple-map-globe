[React Simple Globe](../README.md) / [Exports](../modules.md) / Assets Images

# Module: Assets Images

## Table of contents

### Variables

- [IMAGES](Assets_Images.md#images)

## Variables

### IMAGES

• `Const` **IMAGES**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `CAT` | `string` |

#### Defined in

[src/assets/png/index.ts:8](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/assets/png/index.ts#L8)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Events

# Module: Globe Events

## Table of contents

### Component Functions

- [default](Globe_Events.md#default)

## Component Functions

### default

▸ **default**<`Props`, `Data`\>(`__namedParameters`): `ReactElement`<`Props`, `string` \| `JSXElementConstructor`<`any`\>\>

will only inject data if event is found in props
useSupportedEventKeys on `props`
if `hasSupportedEvents` nest OnEvents
if `hasSupportedHover` nest OnHover

#### Type parameters

| Name | Type |
| :------ | :------ |
| `Props` | `any` |
| `Data` | `any` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<[`EventsProps`](../interfaces/Globe_Events_Types.EventsProps.md)<`Props` & `Partial`<[`HandleEventProps`](../interfaces/Globe_Events_Types.HandleEventProps.md)<`Function`, `Record`<`string`, `any`\>\>\>, `Data`\>\> |

#### Returns

`ReactElement`<`Props`, `string` \| `JSXElementConstructor`<`any`\>\>

>children
>>OnEvents
>>OnHover

#### Defined in

[src/components/globe/events/index.tsx:25](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/index.tsx#L25)

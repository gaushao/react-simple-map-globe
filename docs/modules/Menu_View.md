[React Simple Globe](../README.md) / [Exports](../modules.md) / Menu View

# Module: Menu View

ViewSettings UI configuration

## Table of contents

### References

- [default](Menu_View.md#default)

### Component Functions

- [ViewMenu](Menu_View.md#viewmenu)

## References

### default

Renames and re-exports [ViewMenu](Menu_View.md#viewmenu)

## Component Functions

### ViewMenu

▸ **ViewMenu**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`MenuProps`](../interfaces/Globe_Menu_Types.MenuProps.md) |

#### Returns

`Element`

#### Defined in

[src/components/menu/ViewMenu.tsx:20](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/ViewMenu.tsx#L20)

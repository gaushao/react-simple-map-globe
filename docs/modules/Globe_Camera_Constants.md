[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Camera Constants

# Module: Globe Camera Constants

## Table of contents

### Empty Variables

- [EMPTY\_CAMERA\_DATA](Globe_Camera_Constants.md#empty_camera_data)

### Initial Variables

- [CONSTANTS](Globe_Camera_Constants.md#constants)

### Other Variables

- [INITIAL\_ORIGIN\_X](Globe_Camera_Constants.md#initial_origin_x)
- [INITIAL\_ORIGIN\_Y](Globe_Camera_Constants.md#initial_origin_y)
- [INITIAL\_ROTATION](Globe_Camera_Constants.md#initial_rotation)
- [ROTATION\_SENSITIVITY](Globe_Camera_Constants.md#rotation_sensitivity)
- [ROTATION\_Y\_LIMIT](Globe_Camera_Constants.md#rotation_y_limit)

## Empty Variables

### EMPTY\_CAMERA\_DATA

• `Const` **EMPTY\_CAMERA\_DATA**: [`CameraData`](../classes/Globe_Camera_Classes.CameraData.md)

#### Defined in

[src/components/globe/camera/constants.ts:29](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/constants.ts#L29)

___

## Initial Variables

### CONSTANTS

• `Const` **CONSTANTS**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `INITIAL_ORIGIN_X` | `number` |
| `INITIAL_ORIGIN_Y` | `number` |
| `ROTATION_SENSITIVITY` | `number` |
| `ROTATION_Y_LIMIT` | `number` |

#### Defined in

[src/components/globe/camera/constants.ts:19](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/constants.ts#L19)

___

## Other Variables

### INITIAL\_ORIGIN\_X

• `Const` **INITIAL\_ORIGIN\_X**: ``0``

#### Defined in

[src/components/globe/camera/constants.ts:10](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/constants.ts#L10)

___

### INITIAL\_ORIGIN\_Y

• `Const` **INITIAL\_ORIGIN\_Y**: ``0``

#### Defined in

[src/components/globe/camera/constants.ts:11](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/constants.ts#L11)

___

### INITIAL\_ROTATION

• `Const` **INITIAL\_ROTATION**: [`Coord`](../classes/Globe_Classes.Coord.md)

#### Defined in

[src/components/globe/camera/constants.ts:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/constants.ts#L12)

___

### ROTATION\_SENSITIVITY

• `Const` **ROTATION\_SENSITIVITY**: ``85``

#### Defined in

[src/components/globe/camera/constants.ts:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/constants.ts#L14)

___

### ROTATION\_Y\_LIMIT

• `Const` **ROTATION\_Y\_LIMIT**: ``60``

#### Defined in

[src/components/globe/camera/constants.ts:13](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/constants.ts#L13)

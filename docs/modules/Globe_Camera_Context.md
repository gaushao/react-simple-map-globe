[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Camera Context

# Module: Globe Camera Context

## Table of contents

### Classes

- [CameraContextValue](../classes/Globe_Camera_Context.CameraContextValue.md)

### Context Variables

- [CameraContext](Globe_Camera_Context.md#cameracontext)

### Context Functions

- [CameraProvider](Globe_Camera_Context.md#cameraprovider)

## Context Variables

### CameraContext

• `Const` **CameraContext**: `Context`<[`CameraContextValue`](../classes/Globe_Camera_Context.CameraContextValue.md)\>

#### Defined in

[src/components/globe/camera/Context.tsx:28](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/Context.tsx#L28)

## Context Functions

### CameraProvider

▸ **CameraProvider**(`props`): `Element`

provides [CameraContextValue](../classes/Globe_Camera_Context.CameraContextValue.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | `PropsWithChildren`<`Pick`<[`CameraProps`](../classes/Globe_Camera_Classes.CameraProps.md), ``"settings"``\>\> |

#### Returns

`Element`

[CameraContext](Globe_Camera_Context.md#cameracontext)

#### Defined in

[src/components/globe/camera/Context.tsx:37](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/Context.tsx#L37)

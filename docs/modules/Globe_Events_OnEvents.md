[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Events OnEvents

# Module: Globe Events OnEvents

## Table of contents

### References

- [default](Globe_Events_OnEvents.md#default)

### Component Functions

- [OnEvents](Globe_Events_OnEvents.md#onevents)

## References

### default

Renames and re-exports [OnEvents](Globe_Events_OnEvents.md#onevents)

## Component Functions

### OnEvents

▸ **OnEvents**<`ElementType`, `ComponentProps`, `ForwardedData`\>(`__namedParameters`): `Element`

forwards `data` into `supported` events

#### Type parameters

| Name | Type |
| :------ | :------ |
| `ElementType` | `any` |
| `ComponentProps` | `Record`<`string`, `any`\> |
| `ForwardedData` | `Record`<`string`, `any`\> |

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<{ `forward`: `ForwardedData` ; `supported`: `string`[] ; `unsupported?`: `string`[]  } & `ComponentProps`\> |

#### Returns

`Element`

cloned children with data injected into `supported` events

#### Defined in

[src/components/globe/events/OnEvents.tsx:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/OnEvents.tsx#L14)

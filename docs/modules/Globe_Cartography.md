[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Cartography

# Module: Globe Cartography

load url and validate its data

## Table of contents

### Functions

- [Cartography](Globe_Cartography.md#cartography)

## Functions

### Cartography

▸ **Cartography**(`__namedParameters`): `JSX.Element` \| ``null``

maps CartographyData into supported formats render

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<[`CartographyProps`](../interfaces/Globe_Cartography_Types.CartographyProps.md)\> |

#### Returns

`JSX.Element` \| ``null``

Topology | `null`

#### Defined in

[src/components/globe/cartography/index.tsx:44](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/index.tsx#L44)

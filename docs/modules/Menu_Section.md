[React Simple Globe](../README.md) / [Exports](../modules.md) / Menu Section

# Module: Menu Section

UI menu container

## Table of contents

### References

- [default](Menu_Section.md#default)

### Component Functions

- [Section](Menu_Section.md#section)

## References

### default

Renames and re-exports [Section](Menu_Section.md#section)

## Component Functions

### Section

▸ **Section**(`__namedParameters`): `Element`

toggles visibility of children by clicking title

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<`SectionProps`\> |

#### Returns

`Element`

#### Defined in

[src/components/menu/Section.tsx:23](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/Section.tsx#L23)

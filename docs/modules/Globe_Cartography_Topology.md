[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Cartography Topology

# Module: Globe Cartography Topology

D3 topojson projection

## Table of contents

### Props Interfaces

- [TopologyProps](../interfaces/Globe_Cartography_Topology.TopologyProps.md)

### Functions

- [Topology](Globe_Cartography_Topology.md#topology)

## Functions

### Topology

▸ **Topology**(`__namedParameters`): ``null`` \| `Element`

**`Hook`**

useInCameraRange `inCameraRange` [WIP](https://gitlab.com/gaushao/react-simple-map-globe/-/issues/2)

**`Hook`**

useTopologyLoader from [TopologyProps.data](../interfaces/Globe_Cartography_Topology.TopologyProps.md#data)

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`TopologyProps`](../interfaces/Globe_Cartography_Topology.TopologyProps.md) |

#### Returns

``null`` \| `Element`

map of `inCameraRange` Feature<Geometry, GeoJsonProperties> rendered by GeoPath

#### Defined in

[src/components/globe/cartography/Topology.tsx:27](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/Topology.tsx#L27)

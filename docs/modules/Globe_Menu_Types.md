[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Menu Types

# Module: Globe Menu Types

## Table of contents

### Interfaces

- [MenuProps](../interfaces/Globe_Menu_Types.MenuProps.md)

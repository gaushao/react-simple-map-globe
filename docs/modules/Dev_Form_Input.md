[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev Form Input

# Module: Dev Form Input

form input element wrapper

## Table of contents

### Callback Interfaces

- [InputOnChangeValue](../interfaces/Dev_Form_Input.InputOnChangeValue.md)

### Change Event Functions

- [useOnInputChange](Dev_Form_Input.md#useoninputchange)
- [useOnNumberChange](Dev_Form_Input.md#useonnumberchange)

### Component Functions

- [default](Dev_Form_Input.md#default)

## Change Event Functions

### useOnInputChange

▸ **useOnInputChange**<`T`\>(`callback?`): `undefined` \| (`__namedParameters`: `any`) => ``null`` \| `void`

callsback value of `React.ChangeEventHandler`
[Crash onDrag](https://gitlab.com/gaushao/react-simple-map-globe/-/issues/3)

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `callback?` | [`InputOnChangeValue`](../interfaces/Dev_Form_Input.InputOnChangeValue.md)<`T`\> | InputOnChangeValue<T> |

#### Returns

`undefined` \| (`__namedParameters`: `any`) => ``null`` \| `void`

InputOnChangeValue<T> | undefined

#### Defined in

[src/components/form/Input.tsx:28](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Input.tsx#L28)

___

### useOnNumberChange

▸ **useOnNumberChange**(`callback?`): `undefined` \| (`__namedParameters`: `any`) => ``null`` \| `void`

ensure [useOnInputChange](Dev_Form_Input.md#useoninputchange) to callback `number` value
Crash onDrag | https://gitlab.com/gaushao/react-simple-map-globe/-/issues/3

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `callback?` | [`InputOnChangeValue`](../interfaces/Dev_Form_Input.InputOnChangeValue.md)<`number`\> | InputOnChangeValue<number> |

#### Returns

`undefined` \| (`__namedParameters`: `any`) => ``null`` \| `void`

InputOnChangeValue<number> | undefined

#### Defined in

[src/components/form/Input.tsx:43](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Input.tsx#L43)

___

## Component Functions

### default

▸ **default**<`T`\>(`__namedParameters`): `Element`

form input component defines onChangeCallback

```typescript
onInputChange = useOnInputChange<T>(onChangeValue)
onNumberChange = useOnNumberChange(onChangeNumber)
onChangeCallback = onInputChange || onNumberChange || onChange
```

#### Type parameters

| Name | Type |
| :------ | :------ |
| `T` | `any` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `InputHTMLAttributes`<`Element`\> & { `onChangeNumber?`: [`InputOnChangeValue`](../interfaces/Dev_Form_Input.InputOnChangeValue.md)<`number`\> ; `onChangeValue?`: [`InputOnChangeValue`](../interfaces/Dev_Form_Input.InputOnChangeValue.md)<`T`\>  } |

#### Returns

`Element`

#### Defined in

[src/components/form/Input.tsx:73](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Input.tsx#L73)

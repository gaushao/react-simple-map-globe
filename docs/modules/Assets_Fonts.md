[React Simple Globe](../README.md) / [Exports](../modules.md) / Assets Fonts

# Module: Assets Fonts

## Table of contents

### Variables

- [FONTS](Assets_Fonts.md#fonts)

## Variables

### FONTS

• `Const` **FONTS**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `CQ_MONO` | `any` |

#### Defined in

[src/assets/fonts/index.ts:7](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/assets/fonts/index.ts#L7)

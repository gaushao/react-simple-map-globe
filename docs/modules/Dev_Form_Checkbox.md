[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev Form Checkbox

# Module: Dev Form Checkbox

type Checkbox input element wrapper

## Table of contents

### References

- [default](Dev_Form_Checkbox.md#default)

### Interfaces

- [CheckboxProps](../interfaces/Dev_Form_Checkbox.CheckboxProps.md)

### Component Functions

- [Checkbox](Dev_Form_Checkbox.md#checkbox)

## References

### default

Renames and re-exports [Checkbox](Dev_Form_Checkbox.md#checkbox)

## Component Functions

### Checkbox

▸ **Checkbox**(`__namedParameters`): `Element`

Checkbox field with label

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<[`CheckboxProps`](../interfaces/Dev_Form_Checkbox.CheckboxProps.md)\> |

#### Returns

`Element`

#### Defined in

[src/components/form/Checkbox.tsx:22](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Checkbox.tsx#L22)

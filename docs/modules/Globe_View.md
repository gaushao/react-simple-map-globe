[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe View

# Module: Globe View

rendering view port

## Table of contents

### Component Functions

- [View](Globe_View.md#view)

## Component Functions

### View

▸ **View**(`__namedParameters`): `Element`

view port wrapper

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<[`ViewProps`](../classes/Globe_View_Classes.ViewProps.md)\> |

#### Returns

`Element`

`wrap`\
ViewProvider

#### Defined in

[src/components/globe/view/index.tsx:27](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/index.tsx#L27)

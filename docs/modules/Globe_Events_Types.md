[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Events Types

# Module: Globe Events Types

## Table of contents

### Alias Interfaces

- [ForwardedMouseEvent](../interfaces/Globe_Events_Types.ForwardedMouseEvent.md)

### Callback Interfaces

- [ForwardedEventCb](../interfaces/Globe_Events_Types.ForwardedEventCb.md)

### Props Interfaces

- [EventData](../interfaces/Globe_Events_Types.EventData.md)
- [EventsProps](../interfaces/Globe_Events_Types.EventsProps.md)
- [HandleEventProps](../interfaces/Globe_Events_Types.HandleEventProps.md)
- [HandleHoverEventProps](../interfaces/Globe_Events_Types.HandleHoverEventProps.md)
- [OnHoverEventProps](../interfaces/Globe_Events_Types.OnHoverEventProps.md)

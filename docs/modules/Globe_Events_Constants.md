[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Events Constants

# Module: Globe Events Constants

## Table of contents

### Core Variables

- [SUPPORTED\_EVENTS](Globe_Events_Constants.md#supported_events)

### Empty Variables

- [EMPTY\_EVENT\_PROPS](Globe_Events_Constants.md#empty_event_props)

## Core Variables

### SUPPORTED\_EVENTS

• `Const` **SUPPORTED\_EVENTS**: [``"onAuxClick"``, ``"onAuxClickCapture"``, ``"onClick"``, ``"onClickCapture"``, ``"onContextMenu"``, ``"onContextMenuCapture"``, ``"onDoubleClick"``, ``"onDoubleClickCapture"``, ``"onDrag"``, ``"onDragCapture"``, ``"onDragEnd"``, ``"onDragEndCapture"``, ``"onDragEnter"``, ``"onDragEnterCapture"``, ``"onDragExit"``, ``"onDragExitCapture"``, ``"onDragLeave"``, ``"onDragLeaveCapture"``, ``"onDragOver"``, ``"onDragOverCapture"``, ``"onDragStart"``, ``"onDragStartCapture"``, ``"onDrop"``, ``"onDropCapture"``, ``"onMouseDown"``, ``"onMouseDownCapture"``, ``"onMouseEnter"``, ``"onMouseLeave"``, ``"onMouseMove"``, ``"onMouseMoveCapture"``, ``"onMouseOut"``, ``"onMouseOutCapture"``, ``"onMouseOver"``, ``"onMouseOverCapture"``, ``"onMouseUp"``, ``"onMouseUpCapture"``]

#### Defined in

[src/components/globe/events/constants.ts:20](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/constants.ts#L20)

___

## Empty Variables

### EMPTY\_EVENT\_PROPS

• `Const` **EMPTY\_EVENT\_PROPS**: [`EventData`](../interfaces/Globe_Events_Types.EventData.md)

#### Defined in

[src/components/globe/events/constants.ts:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/constants.ts#L12)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev Form

# Module: Dev Form

FormProvider wrapper

## Table of contents

### References

- [default](Dev_Form.md#default)

### Component Functions

- [Form](Dev_Form.md#form)

## References

### default

Renames and re-exports [Form](Dev_Form.md#form)

## Component Functions

### Form

▸ **Form**<`T`\>(`props`): `Element`

wrap form element into FormProvider

**`Prop`**

form InitialProps.initial data

#### Type parameters

| Name | Description |
| :------ | :------ |
| `T` | type of InitialProps |

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | `PropsWithChildren`<[`InitialProps`](../interfaces/Dev_Form_Types.InitialProps.md)<`T`\>\> |

#### Returns

`Element`

`wrap`\
FormProvider

#### Defined in

[src/components/form/index.tsx:23](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/index.tsx#L23)

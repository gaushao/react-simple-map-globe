[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Camera Types

# Module: Globe Camera Types

## Table of contents

### Data Interfaces

- [CameraForwardedData](../interfaces/Globe_Camera_Types.CameraForwardedData.md)

### Props Interfaces

- [ScreenProps](../interfaces/Globe_Camera_Types.ScreenProps.md)

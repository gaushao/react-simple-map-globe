[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Constants

# Module: Globe Constants

## Table of contents

### Empty Variables

- [EMPTY\_AXLE](Globe_Constants.md#empty_axle)
- [EMPTY\_COORD](Globe_Constants.md#empty_coord)
- [EMPTY\_LINE](Globe_Constants.md#empty_line)
- [EMPTY\_MUTABLE\_STATE](Globe_Constants.md#empty_mutable_state)
- [EMPTY\_OBJECT](Globe_Constants.md#empty_object)
- [EMPTY\_POINT](Globe_Constants.md#empty_point)
- [EMPTY\_REF](Globe_Constants.md#empty_ref)

### Empty Functions

- [EMPTY\_DISPATCH\_PROXY](Globe_Constants.md#empty_dispatch_proxy)

## Empty Variables

### EMPTY\_AXLE

• `Const` **EMPTY\_AXLE**: `PointType`

#### Defined in

[src/components/globe/constants.ts:40](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/constants.ts#L40)

___

### EMPTY\_COORD

• `Const` **EMPTY\_COORD**: [`Coord`](../classes/Globe_Classes.Coord.md)

#### Defined in

[src/components/globe/constants.ts:31](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/constants.ts#L31)

___

### EMPTY\_LINE

• `Const` **EMPTY\_LINE**: [`Line`](../classes/Globe_Classes.Line.md)

#### Defined in

[src/components/globe/constants.ts:35](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/constants.ts#L35)

___

### EMPTY\_MUTABLE\_STATE

• `Const` **EMPTY\_MUTABLE\_STATE**: [`MutableState`](Globe_Types.md#mutablestate)<`any`\>

`[null, noop]`\
used as initial value for MutableState

#### Defined in

[src/components/globe/constants.ts:21](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/constants.ts#L21)

___

### EMPTY\_OBJECT

• `Const` **EMPTY\_OBJECT**: `Readonly`<{}\>

#### Defined in

[src/components/globe/constants.ts:45](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/constants.ts#L45)

___

### EMPTY\_POINT

• `Const` **EMPTY\_POINT**: [`Point`](../classes/Globe_Classes.Point.md)

#### Defined in

[src/components/globe/constants.ts:26](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/constants.ts#L26)

___

### EMPTY\_REF

• `Const` **EMPTY\_REF**: `Readonly`<{ `current`: ``null`` = null }\>

#### Defined in

[src/components/globe/constants.ts:50](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/constants.ts#L50)

## Empty Functions

### EMPTY\_DISPATCH\_PROXY

▸ **EMPTY_DISPATCH_PROXY**(`value`): `void`

`() => noop`\
used as initial EMPTY_MUTABLE_STATES dispatch

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `any` |

#### Returns

`void`

#### Defined in

node_modules/@types/react/index.d.ts:889

[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Cartography Hooks

# Module: Globe Cartography Hooks

## Table of contents

### Interfaces

- [TopologyLoader](../interfaces/Globe_Cartography_Hooks.TopologyLoader.md)

### Type Aliases

- [Topologies](Globe_Cartography_Hooks.md#topologies)

### Callback Functions

- [useTopologyFeatures](Globe_Cartography_Hooks.md#usetopologyfeatures)

### Memo Functions

- [useProjectionPath](Globe_Cartography_Hooks.md#useprojectionpath)
- [useTopologyLoader](Globe_Cartography_Hooks.md#usetopologyloader)

## Type Aliases

### Topologies

Ƭ **Topologies**: `Record`<`string`, `GeoJSON.Feature`[]\>

#### Defined in

[src/components/globe/cartography/hooks.tsx:29](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/hooks.tsx#L29)

## Callback Functions

### useTopologyFeatures

▸ **useTopologyFeatures**(`topology?`): [`UseTopologyFeatures`](../interfaces/Globe_Cartography_Types.UseTopologyFeatures.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `topology?` | [`Topology`](../interfaces/Globe_Cartography_Types.Topology.md) |

#### Returns

[`UseTopologyFeatures`](../interfaces/Globe_Cartography_Types.UseTopologyFeatures.md)

callback to get topojson object features

#### Defined in

[src/components/globe/cartography/hooks.tsx:15](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/hooks.tsx#L15)

___

## Memo Functions

### useProjectionPath

▸ **useProjectionPath**(`geo`, `style?`): ``null`` \| `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `geo` | `Feature`<`Geometry`, `GeoJsonProperties`\> |
| `style?` | `CSSProperties` |

#### Returns

``null`` \| `Element`

projected SVGPathElement

#### Defined in

[src/components/globe/cartography/hooks.tsx:69](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/hooks.tsx#L69)

___

### useTopologyLoader

▸ **useTopologyLoader**(`topology?`): [`TopologyLoader`](../interfaces/Globe_Cartography_Hooks.TopologyLoader.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `topology?` | `Topology`<`Objects`<`GeoJsonProperties`\>\> |

#### Returns

[`TopologyLoader`](../interfaces/Globe_Cartography_Hooks.TopologyLoader.md)

topojson object features

#### Defined in

[src/components/globe/cartography/hooks.tsx:41](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/hooks.tsx#L41)

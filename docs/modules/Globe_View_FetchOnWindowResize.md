[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe View FetchOnWindowResize

# Module: Globe View FetchOnWindowResize

sets view port dimensions

## Table of contents

### Component Functions

- [default](Globe_View_FetchOnWindowResize.md#default)

## Component Functions

### default

▸ **default**(`__namedParameters`): ``null``

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `FetchToBodyOnResizeProps` |

#### Returns

``null``

null

#### Defined in

[src/components/globe/view/FetchOnWindowResize.tsx:23](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/FetchOnWindowResize.tsx#L23)

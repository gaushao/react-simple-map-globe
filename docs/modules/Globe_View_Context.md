[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe View Context

# Module: Globe View Context

provides state and dispatch features to view port handling

## Table of contents

### Classes

- [ViewContextValue](../classes/Globe_View_Context.ViewContextValue.md)

### Context Variables

- [ViewContext](Globe_View_Context.md#viewcontext)

### Provider Functions

- [ViewProvider](Globe_View_Context.md#viewprovider)

## Context Variables

### ViewContext

• `Const` **ViewContext**: `Context`<[`ViewContextValue`](../classes/Globe_View_Context.ViewContextValue.md)\>

#### Defined in

[src/components/globe/view/Context.tsx:21](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/Context.tsx#L21)

## Provider Functions

### ViewProvider

▸ **ViewProvider**(`props`): `Element`

**`Prop`**

[ViewContextValue](../classes/Globe_View_Context.ViewContextValue.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | `PropsWithChildren`<{ `value`: [`ViewContextValue`](../classes/Globe_View_Context.ViewContextValue.md)  }\> |

#### Returns

`Element`

`wrap`\
[ViewContext](Globe_View_Context.md#viewcontext)

#### Defined in

[src/components/globe/view/Context.tsx:34](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/Context.tsx#L34)

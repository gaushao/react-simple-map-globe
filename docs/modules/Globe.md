[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe

# Module: Globe

## Table of contents

### Component Functions

- [default](Globe.md#default)

## Component Functions

### default

▸ **default**(`props`): `Element`

wraps every context around svg elements

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `props` | `PropsWithChildren`<[`GlobeProps`](../interfaces/Globe_Types.GlobeProps.md)\> | `children` will be forwarded into svg projection |

#### Returns

`Element`

[View](Globe.md)
>[Camera](Globe.md)
>>[Projection](Globe.md)
>>>[Cartography](Globe.md)\
>>>[Markers](Globe.md)\
>>>`children`

#### Defined in

[src/components/globe/index.tsx:25](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/index.tsx#L25)

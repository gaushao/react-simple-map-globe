[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Events OnHover

# Module: Globe Events OnHover

handle props switch on mouse hover
see HandleEventProps hover

## Table of contents

### References

- [default](Globe_Events_OnHover.md#default)

### Component Functions

- [Handler](Globe_Events_OnHover.md#handler)
- [OnHover](Globe_Events_OnHover.md#onhover)

## References

### default

Renames and re-exports [OnHover](Globe_Events_OnHover.md#onhover)

## Component Functions

### Handler

▸ **Handler**<`ElementType`, `ComponentProps`\>(`__namedParameters`): ``null`` \| `ReactElement`<`ElementType`, `string` \| `JSXElementConstructor`<`any`\>\>

clone children and merge props from usePropsOnMouseHover

#### Type parameters

| Name | Type |
| :------ | :------ |
| `ElementType` | `any` |
| `ComponentProps` | `Record`<`string`, `any`\> |

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<`ComponentProps` & [`HandleEventProps`](../interfaces/Globe_Events_Types.HandleEventProps.md)<`Function`, `Record`<`string`, `any`\>\>\> |

#### Returns

``null`` \| `ReactElement`<`ElementType`, `string` \| `JSXElementConstructor`<`any`\>\>

cloned children with onHoveredProps

#### Defined in

[src/components/globe/events/OnHover.tsx:17](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/OnHover.tsx#L17)

___

### OnHover

▸ **OnHover**<`ComponentProps`\>(`__namedParameters`): `Element`

checks out for props.hover

#### Type parameters

| Name | Type |
| :------ | :------ |
| `ComponentProps` | `Record`<`string`, `any`\> |

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<`ComponentProps` & `Partial`<[`HandleEventProps`](../interfaces/Globe_Events_Types.HandleEventProps.md)<`Function`, `Record`<`string`, `any`\>\>\>\> |

#### Returns

`Element`

cloned children with onHoveredProps

#### Defined in

[src/components/globe/events/OnHover.tsx:35](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/events/OnHover.tsx#L35)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Markers Label

# Module: Globe Markers Label

## Table of contents

### Component Functions

- [default](Globe_Markers_Label.md#default)

## Component Functions

### default

▸ **default**(`props`): `Element`

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `props` | `PropsWithChildren`<[`MarkerData`](../interfaces/Globe_Markers_Types.MarkerData.md)\> | `label.text` |

#### Returns

`Element`

`SVGTextElement`

#### Defined in

[src/components/globe/markers/Label.tsx:14](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/markers/Label.tsx#L14)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Cartography Types

# Module: Globe Cartography Types

## Table of contents

### Alias Interfaces

- [Feature](../interfaces/Globe_Cartography_Types.Feature.md)
- [Topology](../interfaces/Globe_Cartography_Types.Topology.md)
- [TopologyFeatures](../interfaces/Globe_Cartography_Types.TopologyFeatures.md)

### Data Interfaces

- [PathForwardedData](../interfaces/Globe_Cartography_Types.PathForwardedData.md)

### Hook Interfaces

- [UseTopologyFeatures](../interfaces/Globe_Cartography_Types.UseTopologyFeatures.md)

### Props Interfaces

- [CartographyProps](../interfaces/Globe_Cartography_Types.CartographyProps.md)
- [PathProps](../interfaces/Globe_Cartography_Types.PathProps.md)

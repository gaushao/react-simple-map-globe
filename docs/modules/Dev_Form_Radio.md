[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev Form Radio

# Module: Dev Form Radio

type radio input element wrapper

## Table of contents

### References

- [default](Dev_Form_Radio.md#default)

### Interfaces

- [RadioGroupProps](../interfaces/Dev_Form_Radio.RadioGroupProps.md)
- [RadioOption](../interfaces/Dev_Form_Radio.RadioOption.md)
- [RadioProps](../interfaces/Dev_Form_Radio.RadioProps.md)

### Component Functions

- [Radio](Dev_Form_Radio.md#radio)
- [RadioGroup](Dev_Form_Radio.md#radiogroup)
- [RadioInput](Dev_Form_Radio.md#radioinput)

## References

### default

Renames and re-exports [Radio](Dev_Form_Radio.md#radio)

## Component Functions

### Radio

▸ **Radio**(`__namedParameters`): ``null`` \| `Element`

radio field with label

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`RadioProps`](../interfaces/Dev_Form_Radio.RadioProps.md) |

#### Returns

``null`` \| `Element`

#### Defined in

[src/components/form/Radio.tsx:28](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L28)

___

### RadioGroup

▸ **RadioGroup**(`__namedParameters`): `Element`

radio fields handler: will render one [Radio](Dev_Form_Radio.md#radio)
to each one of the `options`. allows user to
customize radio `value` with [RadioInput](Dev_Form_Radio.md#radioinput)

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<[`RadioGroupProps`](../interfaces/Dev_Form_Radio.RadioGroupProps.md)\> |

#### Returns

`Element`

#### Defined in

[src/components/form/Radio.tsx:81](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L81)

___

### RadioInput

▸ **RadioInput**(`__namedParameters`): `Element`

radio field with input as label
[RadioGroup](Dev_Form_Radio.md#radiogroup) render it if [RadioOption.name](../interfaces/Dev_Form_Radio.RadioOption.md#name) `null`
allows user to customize radio input value
changes `value` only if radio `checked`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`RadioProps`](../interfaces/Dev_Form_Radio.RadioProps.md) |

#### Returns

`Element`

#### Defined in

[src/components/form/Radio.tsx:47](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Radio.tsx#L47)

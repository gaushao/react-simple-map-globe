[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Projection

# Module: Globe Projection

D3 topojson projection

## Table of contents

### Functions

- [Projection](Globe_Projection.md#projection)

## Functions

### Projection

▸ **Projection**(`__namedParameters`): `Element`

provides D3 projected path

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `Object` |
| `__namedParameters.children?` | `ReactNode` |

#### Returns

`Element`

`wrap`\
`React.SVGProps<SVGSVGElement>`\
`children`

#### Defined in

[src/components/globe/projection/index.tsx:19](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/index.tsx#L19)

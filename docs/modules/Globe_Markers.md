[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Markers

# Module: Globe Markers

Marker mapper

## Table of contents

### Component Functions

- [Markers](Globe_Markers.md#markers)

## Component Functions

### Markers

▸ **Markers**(`props`): ``null`` \| `Element`

maps markers MarkerData into Marker component

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `props` | `MarkersProps` | mapped into Marker |

#### Returns

``null`` \| `Element`

mapped markers into Marker

#### Defined in

[src/components/globe/markers/index.tsx:27](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/markers/index.tsx#L27)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev Form Context

# Module: Dev Form Context

provides state and dispatch features to form handling

## Table of contents

### Context Variables

- [FormContext](Dev_Form_Context.md#formcontext)

### Provider Functions

- [FormProvider](Dev_Form_Context.md#formprovider)

## Context Variables

### FormContext

• `Const` **FormContext**: `Context`<[`FormContextValue`](../interfaces/Dev_Form_Types.FormContextValue.md)<`any`\>\>

#### Defined in

[src/components/form/Context.tsx:28](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Context.tsx#L28)

## Provider Functions

### FormProvider

▸ **FormProvider**<`T`\>(`props`): `Element`

**`Prop`**

form InitialProps.initial data

#### Type parameters

| Name | Description |
| :------ | :------ |
| `T` | type of `initial` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | `PropsWithChildren`<[`InitialProps`](../interfaces/Dev_Form_Types.InitialProps.md)<`T`\>\> |

#### Returns

`Element`

`wrap`\
[FormContext](Dev_Form_Context.md#formcontext)

#### Defined in

[src/components/form/Context.tsx:41](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/Context.tsx#L41)

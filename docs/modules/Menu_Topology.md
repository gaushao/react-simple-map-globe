[React Simple Globe](../README.md) / [Exports](../modules.md) / Menu Topology

# Module: Menu Topology

TopologySettings UI configuration

## Table of contents

### References

- [default](Menu_Topology.md#default)

### Component Functions

- [TopologyMenu](Menu_Topology.md#topologymenu)

## References

### default

Renames and re-exports [TopologyMenu](Menu_Topology.md#topologymenu)

## Component Functions

### TopologyMenu

▸ **TopologyMenu**(`__namedParameters`): ``null``

WIP handle Topology settings

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`MenuProps`](../interfaces/Globe_Menu_Types.MenuProps.md) |

#### Returns

``null``

`wrap`\
RadioGroup
- RadioGroupProps.options: TopologyData.keys
- RadioGroupProps.onSelect: TopologyController.setSelected>

#### Defined in

[src/components/menu/TopologyMenu.tsx:19](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/TopologyMenu.tsx#L19)

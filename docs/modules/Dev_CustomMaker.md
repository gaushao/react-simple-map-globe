[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev CustomMaker

# Module: Dev CustomMaker

testing custom svg marker

## Table of contents

### Component Functions

- [default](Dev_CustomMaker.md#default)

## Component Functions

### default

▸ **default**(`props`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | `any` |

#### Returns

`Element`

SVG

#### Defined in

[src/CustomMaker.tsx:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/CustomMaker.tsx#L12)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Menu Camera

# Module: Menu Camera

Markers UI configuration

## Table of contents

### References

- [default](Menu_Camera-1.md#default)

### Component Functions

- [MarkersMenu](Menu_Camera-1.md#markersmenu)

## References

### default

Renames and re-exports [MarkersMenu](Menu_Camera-1.md#markersmenu)

## Component Functions

### MarkersMenu

▸ **MarkersMenu**(`__namedParameters`): ``null`` \| `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`MenuProps`](../interfaces/Globe_Menu_Types.MenuProps.md) |

#### Returns

``null`` \| `Element`

#### Defined in

[src/components/menu/markers/index.tsx:13](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/markers/index.tsx#L13)

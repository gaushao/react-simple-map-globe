[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Projection Context

# Module: Globe Projection Context

## Table of contents

### Value Classes

- [ProjectionContextValue](../classes/Globe_Projection_Context.ProjectionContextValue.md)

### Props Interfaces

- [D3GeoProjection](../interfaces/Globe_Projection_Context.D3GeoProjection.md)

### Component Variables

- [ProjectionContext](Globe_Projection_Context.md#projectioncontext)

### Empty Variables

- [EMPTY\_PROJECTION\_CONTEXT\_VALUE](Globe_Projection_Context.md#empty_projection_context_value)

## Component Variables

### ProjectionContext

• `Const` **ProjectionContext**: `Context`<[`ProjectionContextValue`](../classes/Globe_Projection_Context.ProjectionContextValue.md)\>

#### Defined in

[src/components/globe/projection/Context.ts:35](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/Context.ts#L35)

___

## Empty Variables

### EMPTY\_PROJECTION\_CONTEXT\_VALUE

• `Const` **EMPTY\_PROJECTION\_CONTEXT\_VALUE**: [`ProjectionContextValue`](../classes/Globe_Projection_Context.ProjectionContextValue.md)

#### Defined in

[src/components/globe/projection/Context.ts:30](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/Context.ts#L30)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Camera

# Module: Globe Camera

## Table of contents

### Functions

- [Camera](Globe_Camera.md#camera)

## Functions

### Camera

▸ **Camera**(`props`): `Element`

provides CameraContextValue

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `props` | `PropsWithChildren`<[`CameraProps`](../classes/Globe_Camera_Classes.CameraProps.md)\> | CameraProps |

#### Returns

`Element`

>[Camera Context](Globe.md)
>>[Camera Screen](Globe.md)
>>>`children`

#### Defined in

[src/components/globe/camera/index.tsx:19](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/index.tsx#L19)

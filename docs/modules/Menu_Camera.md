[React Simple Globe](../README.md) / [Exports](../modules.md) / Menu Camera

# Module: Menu Camera

CameraSettings UI configuration

## Table of contents

### References

- [default](Menu_Camera.md#default)

### Component Functions

- [CameraMenu](Menu_Camera.md#cameramenu)

## References

### default

Renames and re-exports [CameraMenu](Menu_Camera.md#cameramenu)

## Component Functions

### CameraMenu

▸ **CameraMenu**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`MenuProps`](../interfaces/Globe_Menu_Types.MenuProps.md) |

#### Returns

`Element`

#### Defined in

[src/components/menu/CameraMenu.tsx:16](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/CameraMenu.tsx#L16)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Menu

# Module: Menu

module debugger and controller

## Table of contents

### Component Functions

- [Menu](Menu.md#menu)

## Component Functions

### Menu

▸ **Menu**(`__namedParameters`): `JSX.Element`

Links to:\
[Gitlab](https://gitlab.com/gaushao/react-simple-map-globe)\
[Docs](https://gaushao.gitlab.io/react-simple-map-globe/docs)\
open UI:\
CameraSettings\
CartographySettings

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `PropsWithChildren`<[`MenuProps`](../interfaces/Globe_Menu_Types.MenuProps.md)\> |

#### Returns

`JSX.Element`

`wrap`\
CartographyMenu\
CameraMenu

#### Defined in

[src/components/menu/index.tsx:50](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/index.tsx#L50)

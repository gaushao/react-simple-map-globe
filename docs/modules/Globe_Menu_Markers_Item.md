[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Menu Markers Item

# Module: Globe Menu Markers Item

## Table of contents

### Component Functions

- [default](Globe_Menu_Markers_Item.md#default)

## Component Functions

### default

▸ **default**(`__namedParameters`): `Element`

each marker renders an item

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | `ItemProps` |

#### Returns

`Element`

#### Defined in

[src/components/menu/markers/Item.tsx:28](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/markers/Item.tsx#L28)

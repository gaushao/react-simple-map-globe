[React Simple Globe](../README.md) / [Exports](../modules.md) / Menu Cartography

# Module: Menu Cartography

CartographySettings UI configuration

## Table of contents

### Component Functions

- [CartographyMenu](Menu_Cartography.md#cartographymenu)

### Other Functions

- [default](Menu_Cartography.md#default)

## Component Functions

### CartographyMenu

▸ **CartographyMenu**(`__namedParameters`): `Element`

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`MenuProps`](../interfaces/Globe_Menu_Types.MenuProps.md) |

#### Returns

`Element`

#### Defined in

[src/components/menu/CartographyMenu.tsx:17](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/menu/CartographyMenu.tsx#L17)

___

## Other Functions

### default

▸ **default**(`props`): ``null`` \| `ReactElement`<`any`, `string` \| `JSXElementConstructor`<`any`\>\>

**NOTE**: Exotic components are not callable.

#### Parameters

| Name | Type |
| :------ | :------ |
| `props` | [`MenuProps`](../interfaces/Globe_Menu_Types.MenuProps.md) |

#### Returns

``null`` \| `ReactElement`<`any`, `string` \| `JSXElementConstructor`<`any`\>\>

#### Defined in

node_modules/@types/react/index.d.ts:360

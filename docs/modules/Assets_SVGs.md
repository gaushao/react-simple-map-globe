[React Simple Globe](../README.md) / [Exports](../modules.md) / Assets SVGs

# Module: Assets SVGs

## Table of contents

### Assets Variables

- [SVG](Assets_SVGs.md#svg)

## Assets Variables

### SVG

• `Const` **SVG**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `Book` | `FunctionComponent`<`SVGProps`<`SVGSVGElement`\> & { `title?`: `string`  }\> |
| `Gitlab` | `FunctionComponent`<`SVGProps`<`SVGSVGElement`\> & { `title?`: `string`  }\> |
| `Globe` | `FunctionComponent`<`SVGProps`<`SVGSVGElement`\> & { `title?`: `string`  }\> |
| `Location` | `FunctionComponent`<`SVGProps`<`SVGSVGElement`\> & { `title?`: `string`  }\> |
| `Settings` | `FunctionComponent`<`SVGProps`<`SVGSVGElement`\> & { `title?`: `string`  }\> |

#### Defined in

[src/assets/svg/index.ts:15](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/assets/svg/index.ts#L15)

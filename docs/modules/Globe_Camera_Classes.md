[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Camera Classes

# Module: Globe Camera Classes

## Table of contents

### Data Classes

- [CameraData](../classes/Globe_Camera_Classes.CameraData.md)

### Props Classes

- [CameraProps](../classes/Globe_Camera_Classes.CameraProps.md)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev Form Hooks

# Module: Dev Form Hooks

## Table of contents

### Context Functions

- [useFormContext](Dev_Form_Hooks.md#useformcontext)

### Dispatch Functions

- [useFormDispatch](Dev_Form_Hooks.md#useformdispatch)

### Listener Functions

- [useFormListener](Dev_Form_Hooks.md#useformlistener)

### Selector Functions

- [useFormValue](Dev_Form_Hooks.md#useformvalue)

## Context Functions

### useFormContext

▸ **useFormContext**<`T`\>(): [`FormContextValue`](../interfaces/Dev_Form_Types.FormContextValue.md)<`T`\>

#### Type parameters

| Name |
| :------ |
| `T` |

#### Returns

[`FormContextValue`](../interfaces/Dev_Form_Types.FormContextValue.md)<`T`\>

#### Defined in

[src/components/form/hooks.ts:15](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/hooks.ts#L15)

___

## Dispatch Functions

### useFormDispatch

▸ **useFormDispatch**<`T`\>(`path`): [`DispatchState`](../interfaces/Globe_Types.DispatchState.md)<`T`\>

call form dispatch on path with value
```typescript
const dispatch = useFormDispatch('path.to.value')
dispatch(value)
```

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` |

#### Returns

[`DispatchState`](../interfaces/Globe_Types.DispatchState.md)<`T`\>

#### Defined in

[src/components/form/hooks.ts:37](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/hooks.ts#L37)

___

## Listener Functions

### useFormListener

▸ **useFormListener**<`T`\>(`path`): [`DispatchState`](../interfaces/Globe_Types.DispatchState.md)<`T`\>

`listen` to value by returned `listen callback`
and dispatches it into the form path on change\
`listen callback` will only fire on value change effect

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` |

#### Returns

[`DispatchState`](../interfaces/Globe_Types.DispatchState.md)<`T`\>

`listen callback`

#### Defined in

[src/components/form/hooks.ts:50](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/hooks.ts#L50)

___

## Selector Functions

### useFormValue

▸ **useFormValue**<`T`\>(`path`): `T`

access form value by path
use dots to select inner properties
```typescript
const value = useFormValue('path.to.value')
```

#### Type parameters

| Name |
| :------ |
| `T` |

#### Parameters

| Name | Type |
| :------ | :------ |
| `path` | `string` |

#### Returns

`T`

#### Defined in

[src/components/form/hooks.ts:26](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/form/hooks.ts#L26)

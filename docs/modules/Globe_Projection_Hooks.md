[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Projection Hooks

# Module: Globe Projection Hooks

todo description

## Table of contents

### Functions

- [useProjectionContext](Globe_Projection_Hooks.md#useprojectioncontext)

## Functions

### useProjectionContext

▸ **useProjectionContext**(): [`ProjectionContextValue`](../classes/Globe_Projection_Context.ProjectionContextValue.md)

#### Returns

[`ProjectionContextValue`](../classes/Globe_Projection_Context.ProjectionContextValue.md)

#### Defined in

[src/components/globe/projection/hooks.ts:9](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/projection/hooks.ts#L9)

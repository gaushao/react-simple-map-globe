[React Simple Globe](../README.md) / [Exports](../modules.md) / Dev Constants

# Module: Dev Constants

used to test globe module features

## Table of contents

### Constant Variables

- [LOCATIONS](Dev_Constants.md#locations)
- [PROPS](Dev_Constants.md#props)

### Other Variables

- [DEV\_VIEW\_DATA](Dev_Constants.md#dev_view_data)

## Constant Variables

### LOCATIONS

• `Const` **LOCATIONS**: `Record`<`string`, [`MarkerData`](../interfaces/Globe_Markers_Types.MarkerData.md)\>

dummy locations to Marker testing

#### Defined in

[src/constants.ts:23](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/constants.ts#L23)

___

### PROPS

• `Const` **PROPS**: [`GlobeProps`](../interfaces/Globe_Types.GlobeProps.md)

dummy props to [Globe](Globe.md) testing

#### Defined in

[src/constants.ts:66](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/constants.ts#L66)

___

## Other Variables

### DEV\_VIEW\_DATA

• `Const` **DEV\_VIEW\_DATA**: [`ViewData`](../classes/Globe_View_Classes.ViewData.md)

#### Defined in

[src/constants.ts:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/constants.ts#L12)

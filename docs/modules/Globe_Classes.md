[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Classes

# Module: Globe Classes

## Table of contents

### Value
cartesian coordinates Classes

- [Point](../classes/Globe_Classes.Point.md)

### Value
cartesian line Classes

- [Line](../classes/Globe_Classes.Line.md)
- [Rectangle](../classes/Globe_Classes.Rectangle.md)

### Value
geographic coordinates Classes

- [Coord](../classes/Globe_Classes.Coord.md)

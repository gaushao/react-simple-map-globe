[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Cartography Constants

# Module: Globe Cartography Constants

## Table of contents

### Preset Enumerations

- [CartographyUrl](../enums/Globe_Cartography_Constants.CartographyUrl.md)

### Empty Variables

- [EMPTY\_CARTOGRAPHY\_DATA](Globe_Cartography_Constants.md#empty_cartography_data)

## Empty Variables

### EMPTY\_CARTOGRAPHY\_DATA

• `Const` **EMPTY\_CARTOGRAPHY\_DATA**: [`CartographyData`](../classes/Globe_Cartography_Classes.CartographyData.md)

#### Defined in

[src/components/globe/cartography/constants.ts:19](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/cartography/constants.ts#L19)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe Camera Rotate

# Module: Globe Camera Rotate

deal with camera rotation

## Table of contents

### Handler Classes

- [CameraRotationHandler](../classes/Globe_Camera_Rotate.CameraRotationHandler.md)

### Shape Variables

- [CAMERA\_ROTATION\_HANDLER](Globe_Camera_Rotate.md#camera_rotation_handler)

### Handler Functions

- [useCameraRotationHandler](Globe_Camera_Rotate.md#usecamerarotationhandler)

## Shape Variables

### CAMERA\_ROTATION\_HANDLER

• `Const` **CAMERA\_ROTATION\_HANDLER**: [`CameraRotationHandler`](../classes/Globe_Camera_Rotate.CameraRotationHandler.md)

#### Defined in

[src/components/globe/camera/hooks/rotate.ts:42](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/rotate.ts#L42)

## Handler Functions

### useCameraRotationHandler

▸ **useCameraRotationHandler**(`settings?`): [`CameraRotationHandler`](../classes/Globe_Camera_Rotate.CameraRotationHandler.md)

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `settings` | `undefined` \| [`MutableState`](Globe_Types.md#mutablestate)<[`CameraData`](../classes/Globe_Camera_Classes.CameraData.md)\> | `EMPTY_MUTABLE_STATE` | can be loaded from outside module into CameraProps |

#### Returns

[`CameraRotationHandler`](../classes/Globe_Camera_Rotate.CameraRotationHandler.md)

#### Defined in

[src/components/globe/camera/hooks/rotate.ts:49](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/camera/hooks/rotate.ts#L49)

[React Simple Globe](../README.md) / [Exports](../modules.md) / Globe View Constants

# Module: Globe View Constants

## Table of contents

### Empty Variables

- [EMPTY\_VIEW\_DATA](Globe_View_Constants.md#empty_view_data)

### Initial Variables

- [INITIAL\_DIMENSIONS](Globe_View_Constants.md#initial_dimensions)
- [INITIAL\_HEIGHT](Globe_View_Constants.md#initial_height)
- [INITIAL\_SCALE](Globe_View_Constants.md#initial_scale)
- [INITIAL\_WIDTH](Globe_View_Constants.md#initial_width)
- [ZOOM\_SENSITIVITY](Globe_View_Constants.md#zoom_sensitivity)

## Empty Variables

### EMPTY\_VIEW\_DATA

• `Const` **EMPTY\_VIEW\_DATA**: [`ViewData`](../classes/Globe_View_Classes.ViewData.md)

#### Defined in

[src/components/globe/view/constants.ts:33](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/constants.ts#L33)

___

## Initial Variables

### INITIAL\_DIMENSIONS

• `Const` **INITIAL\_DIMENSIONS**: [`Point`](../classes/Globe_Classes.Point.md)

#### Defined in

[src/components/globe/view/constants.ts:21](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/constants.ts#L21)

___

### INITIAL\_HEIGHT

• `Const` **INITIAL\_HEIGHT**: ``600``

#### Defined in

[src/components/globe/view/constants.ts:17](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/constants.ts#L17)

___

### INITIAL\_SCALE

• `Const` **INITIAL\_SCALE**: `number`

#### Defined in

[src/components/globe/view/constants.ts:29](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/constants.ts#L29)

___

### INITIAL\_WIDTH

• `Const` **INITIAL\_WIDTH**: ``800``

#### Defined in

[src/components/globe/view/constants.ts:12](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/constants.ts#L12)

___

### ZOOM\_SENSITIVITY

• `Const` **ZOOM\_SENSITIVITY**: ``1.5``

#### Defined in

[src/components/globe/view/constants.ts:25](https://gitlab.com/gaushao/react-simple-map-globe/-/blob/f8a6da4/src/components/globe/view/constants.ts#L25)
